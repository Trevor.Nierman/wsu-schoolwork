//Trevor Nierman
//r957f692
//Program 3

/*******************************************************************************
This is a program that will create 5 books in a linked list and set their
pointers to the next book. The fifth book's pointer will be set to NULL. Then,
for all five books, each function will be tested. 

Included files, node.cpp and node.hpp, will contain the code for the class node,
which contains a book title, author, and date. They also include functions that 
allow the data to be accessed and changed. 
*******************************************************************************/
 
 #include<iostream>
 #include<string>
 #include"node.hpp"
 using namespace std;

int main(void)
{
	//Create pointers, nodes
	Node* book1 = new Node();
	Node* book2 = new Node("To Kill a Mockingbird", "Harper Lee", "05.20.2016");
	Node* book3 = new Node("Fahrenheit 451", "Ray Bradbury", "01.28.1996");
	Node* book4 = new Node("Moby-Dick", "Herman Melville", "02.04.1998");
	Node* book5 = new Node("A Book", "An Author", "A date");
	
	//Set pointers to next in list
	book1->set_next(book2);
	book2->set_next(book3);
	book3->set_next(book4);
	book4->set_next(book5);
	
	//Test accessor functions
	cout << book1->get_title() << "\t" << book1->get_author() << "\n";
	cout << book2->get_title() << "\t" << book2->get_author() << "\n";
	cout << book3->get_title() << "\t" << book3->get_author() << "\n";
	cout << book4->get_title() << "\t" << book4->get_author() << "\n";
	cout << book5->get_title() << "\t" << book5->get_author() << "\n";
	cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";
	
	//Test mutator functions
	book1->set_title("Title1");
	book2->set_title("Title2");
	book3->set_title("Title3");
	book4->set_title("Title4");
	book5->set_title("Title5");
	
	book1->set_author("Author1");
	book2->set_author("Author2");
	book3->set_author("Author3");
	book4->set_author("Author4");
	book5->set_author("Author5");
	
	book1->set_date("Date1");
	book2->set_date("Date2");
	book3->set_date("Date3");
	book4->set_date("Date4");
	book5->set_date("Date5");
	
	//Test book proccessing
	book1->process_data();
	book2->process_data();
	book3->process_data();
	book4->process_data();
	book5->process_data();
		
	cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";
	
	//Test data comparison
	if(book1->compare_data("Title1"))
	{
		cout << "This works for 1\n";
	}
	else
	{
		cout << "This isn't working for 1\n";
	}
	
	if(book2->compare_data("Title2"))
	{
		cout << "This works for 2\n";
	}
	else
	{
		cout << "This isn't working for 2\n";
	}
	
	if(book3->compare_data("Title3"))
	{
		cout << "This works for 3\n";
	}
	else
	{
		cout << "This isn't working for 3\n";
	}
	
	if(book4->compare_data("Title4"))
	{
		cout << "This works for 4\n";
	}
	else
	{
		cout << "This isn't working for 4\n";
	}
	
	if(book5->compare_data("Title5"))
	{
		cout << "This works for 5\n";
	}
	else
	{
		cout << "This isn't working for 5\n";
	}
	
	return (0);
}
