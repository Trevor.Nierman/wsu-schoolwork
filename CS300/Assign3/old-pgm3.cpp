//Trevor Nierman
//r957f692
//Program 3

/*******************************************************************************
A program that will create a linked list of favorite books and print their
info on-screen. Utilizes class "Node" (found in node.hpp and node.cpp files),
which contains data on the title and author of the book, as well as the
date read. The class also has a built-in pointer that points to the next
list element. The program will also utilize the class "Link-list", created
below, which will track the first element, number of elements, and contain
functions to add, delete and print elements. 


	Class: Linked_list
		Node *head - used to track first element in the list
		Node *tail - used to track last element in the list
		int count - determines number of elements in list
		
		Accessor Functions:
		Node *get_header - returns the pointer to first element
		int get_count - returns current count of elements
		
		Mutator Functions:
		add_count - adds one to count
		sub_count - subtracts one from count
		add_node - adds another node to list
		delete_node - deletes node from list
			


	


*******************************************************************************/
 
 #include<iostream>
 #include<string>
 #include"node.hpp"
 using namespace std;
 
 class Linked_list
 {
	 //Pointers to the head element, tail element, and a temporary pointer
	 //(ptr) used for various purposes
	 Node *head, *tail, *ptr;
	 int count;
	 
	 public:
		//Constructor
		Linked_list(void);
		
		//Mutator functions
		void add_node(string t, string a, string d);
		bool delete_node(string t);
		void add_count(void);
		void sub_count(void);
		void set_ptr(Node *p);
		
		//Accessor functions
		void print_nodes(void);
		
		Node *get_ptr(void)
		{
			return ptr;
		}
		
		int get_count(void)
		{
			return count;
		}
		
		Node *get_head(void)
		{
			return head;
		}
		
		Node *get_tail(void)
		{
			return tail;
		}
				
		//Searches for nodes
		Node *find_node(string t);
		
 };
 //Constructor
 Linked_list::Linked_list()
 {
	 head = tail = ptr = NULL;
	 count = 0;
 }
 
 //Searches for nodes
 Node Linked_list::*find_node(string t)
 {
	 //Create temporary pointers to find node
	 Node *curr, *p;
	 
	 curr->get_head();
	 while(curr != NULL && !(curr->compare_data(t)))
	 {
		 p = curr;
		 curr = curr->get_next(); 
	 }
	 
	 //No matches found
	 if(curr == NULL)
	 {
		 return NULL;
	 }
	 //Returns matched pointer, sets "ptr" to previous
	 else
	 {
		 set_ptr(p);
		 return curr;
	 }
 }
 
 
 //Mutator functions
 void Linked_list::set_ptr(Node *p)
 {
	 ptr = p;
 }
 
 void Linked_list::add_count(void)
 {
	 count++;
 }
 
 void Linked_list::sub_count(void)
 {
	 count--;
 }
 
 void Linked_list::add_node(string t, string a, string d)
 {
	 //Create temporary pointer to new node
	 Node *temp = new Node(t,a,d);
	 
	 //New pointer points to previous head of list
	 temp->set_next(head); 
	 
	 //If list was empty, makes tail point to new element
	 if(count == 0)
	 {
		 tail = temp;
	 }
	 
	 //New pointer becomes head of list
	 head = temp;
	 
	 //Add to count
	 add_count();
 }
 
 bool Linked_list::delete_node(string t)
 {
	 Node *curr, *temp;
	 
	 //Search for node to be deleted
	 curr = find_node(t);
	 
	 //Search failed
	 if(curr == NULL) 
	 {
		 return 0;
	 }
	 
	 else
	 {
		 //Node to be deleted is head, changes head to next node, then deletes
		 if(curr == head)
		 {
			 head = head->get_next();
			 delete curr;
			 
			 return 1;
		 }
		 else
		 {
			 //Returns previous pointer
			 temp = get_ptr();
			 //Sets to next pointer
			 temp->set_next(temp->get_next());
			 //Deletes current pointer
			 delete curr;
			 
			 return 1;
		 }
	 }
	 
 }
 
 //Accessor function
 void Linked_list::print_nodes(void)
 {
	 Node *temp = head;
	 
	 //Prints book data onscreen to end of list
	 while(temp != NULL)
	 {
		 temp->process_data();
		 temp = temp->get_next();
	 }
 }
 
 
 int main(void)
 {
	 Linked_list book_list;
	 book_list.add_node("To Kill a Mockingbird", "Harper Lee", "05.20.2016");
	 book_list.add_node("Fahrenheit 451", "Ray Bradbury", "01.28.1996");
	 book_list.add_node("Moby-Dick", "Herman Melville", "02.04.1998");
	 book_list.add_node("A Book", "An Author", "A date");
	 book_list.add_node("Another Book", "Another Author", "Another Date");
	 book_list.print_nodes();
	 cout << "Deleting book 'Another Book'\n";
	 book_list.delete_node("Another Book");
	 book_list.print_nodes();
	 
	 
 
 return(0);
 }
