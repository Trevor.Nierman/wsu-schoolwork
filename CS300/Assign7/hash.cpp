//Trevor Nierman
//R957F692
//Assignment 7

/*******************************************************************************
This file describes the functions for class "hash" used in assignment 7.
*******************************************************************************/
#include<iostream>
#include "hash.hpp"
using namespace std;

//CONSTRUCTOR
Hash::Hash()
{
	num = -1;
	hashed_num = 0;
	probe_num = 0;
}

Hash::Hash(int n)
{
	num = n;
	calc_hashed_num();
	probe_num = 0;
}

//Accessor functions
int Hash::get_num(void)
{
	return num;
}
	
int Hash::get_hashed_num(void)
{
	return hashed_num;
}
	
int Hash::get_probe_num(void)
{
	return probe_num;
}
	
//Mutator functions
void Hash::set_num(int n)
{
	num = n;
}
	
void Hash::add_probe_num(void)
{
	probe_num++;
}
	
void Hash::calc_hashed_num(void)
{
	hashed_num = num % 100003;
}
