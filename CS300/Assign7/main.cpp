//Trevor Nierman
//R957F692
//Assignment 7

/*******************************************************************************
This program uses data from 'file.dat' to hash 100,000 integers and input them 
into a table. It then searches for the other 100,000 integers from 'file.dat' in 
the table and displays which ones were found.

If there is a collision, the resolution method will be quadratic probe, where the 
collision number is squared and added to the hashed address to create a new hashed 
address. This is only to be done 10 times before the integer is ultimately not 
stored in the table. The output file tracks the number of times the quadratic 
probe was needed to find the data, as well as the data that was not stored.

Functions:
		

			
		--check vacancy--
			-Determines if the array address is empty
			
			bool is_empty(int array[], int index)
			
				if(array is empty at index)
					return true
					
				else
					return false
					
			inputs - array[index] - a specific address in the array that is 
					determined to be empty or not.
					
			outputs - true/false - returns true if array address is empty, 
					  false otherwise.
					  
		--Collision resolution-- 
			-A function that resolves any collision that may occur due to the 
			hashing proccess. Uses array not_stored to store any value that 
			fails to find an empty address after 10 probes. 
			
			bool resolve_coll(int array[], int coll_val, int num, int not_stored[],
				int& ns_val)
			{
				initialize probe_num to 1
				
				coll_val = coll_val + probe_num
				
				loop(probe_num <= 10)
					if(is_empty(array, coll_val))
						array[coll_val] = num
						
						return true
						
					else
						probe_num++
						
						coll_val = coll_value + probe_num * probe_num
						
				end loop
				
				not_stored[ns_val] = num
				ns_val++
				
				return false
			}
			
			inputs - array - Array of 100,000 hashed integers.
					 
					 coll_val - "collision value" the current hashed address
					 for the integer. 
					 
					 not_stored - Array of values not stored in 'array'.
					 
					 int num - The actual integer being stored (not hashed).
					 
					 int& ns_val - "not stored value" - the current index for 
					 the array 'not_stored'.
					 
			output - true/false - Returns true if collision was successfully
					 resolved, false otherwise.
			
		--Populate array--
			-A simple 'for' loop to populate an array of 125,000 elements with 
			100,000 hashed values. Resolves collisions if necessary.
			
			void pop_array(int array[])
			{
				open file.dat
				
				for(100,000 integers)
					hash_val = div_hash(file.dat output)
					
					if(is_empty(array[div_hash(file.dat output)])
						
						array[hash_val] = file.dat output
					
					else 
					{
						if(resolve_coll(array, hash_val, file.dat output, 
										not_stored, ns_val))
							
						else
							Display that the number could not be stored
					}
			}
			

*******************************************************************************/
#include<iostream>
#include<fstream>
#include "hash.hpp"
using namespace std;

bool is_empty(Hash array[], int index)
{
	if(array[index].get_num() == -1)
	{
		return true;
	}
		
	else
	{
		return false;
	}
}

bool res_coll(Hash array[], Hash number, Hash not_stored[], int& ns_val)
{
	int coll_addr = number.get_hashed_num();
	
	//Sets probe number to 1
	number.add_probe_num();
	
	coll_addr = coll_addr + number.get_probe_num() * number.get_probe_num();
	
	while(number.get_probe_num() <= 10)
	{
		
		//If collision address moves beyond the range of the array, resets address
		//to the beginning of the array (mathematically this should never actually happen)
		
		if(coll_addr >= 125000)
		{
			coll_addr = coll_addr - 125000;
		}
		
		if(is_empty(array, coll_addr))
		{
			array[coll_addr] = number;
			
			return true;
		}
		
		else
		{
			number.add_probe_num();
			
			coll_addr = coll_addr + number.get_probe_num() * number.get_probe_num();
		}
	}
	
	not_stored[ns_val] = number;
	ns_val++;
	
	return false;
}

void pop_array(Hash array[], Hash search_list[], Hash not_stored[], int& ns_val)
{
	int i, num;
	Hash temp;
	ifstream fin;
	
	fin.open("file.dat");
	
	if(fin.fail())
	{
		cout << "The file could not be opened. Array not populated." << endl;
	}
	
	else
	{
		//Populates array with 100,000 values
		for(i = 0; i < 100000; i++)
		{
			fin >> num;
			
			
			temp = Hash(num);
			
			
			//No collision when hashed
			if(is_empty(array, temp.get_hashed_num()))
			{
				array[temp.get_hashed_num()] = temp;
				

			}
			
			//Collision resolution needed
			else
			{
				res_coll(array, temp, not_stored, ns_val);
				
			}

		//End for
		}
	
	//End else	
	}
	
	//Populate search array
	for(i = 0; i < 100000; i++)
	{
		fin >> num;
		
		search_list[i] = Hash(num);
	}
		
	fin.close();
	
//End function	
}

void search_array(Hash array[], Hash search_list[], Hash not_found[], int& nf_val, Hash found[], int& f_val)
{
	//Search indeces
	int i;
	
	//key to search for
	int hash_key;
	
	for(i = 0; i <100000; i++)
	{
		hash_key = search_list[i].get_hashed_num();
		
		//Number not found; probe until found or 'probe_num' exceeds max
		while(search_list[i].get_num() != array[hash_key].get_num() && search_list[i].get_probe_num() <= 10)
		{

			//Begin probing
			search_list[i].add_probe_num();			
			
			hash_key = hash_key + (search_list[i].get_probe_num()*search_list[i].get_probe_num());
			
			//If 'hash_key' moves beyond the range of 'array', reset to beginning
			if(hash_key >= 125000)
			{
				hash_key = hash_key - 125000;
			}
		}
		
		//If found, stored in 'found' array
		if(search_list[i].get_num() == array[hash_key].get_num())
		{
			found[f_val] = search_list[i];			
			f_val ++;
		}
		
		//Otherwise, value is stored in not_found
		else
		{
			not_found[nf_val] = search_list[i];
			nf_val++;
		}
	}
	
}
			

int main(void)
{
	int i, ns_val = 0, nf_val = 0, f_val = 0;
	Hash array[125000];
	Hash search_list[100000];
	Hash not_stored[10000];
	Hash not_found[100000];
	Hash found[100000];
	
	//Populate arrays
	pop_array(array, search_list, not_stored, ns_val);
	
	
	//Begin search
	search_array(array, search_list, not_found, nf_val, found, f_val);
	
	//Output to 'results.dat'
	ofstream fout;
	
	fout.open("results.dat", ios::trunc);
	
	fout << "Numbers that were not stored: " << endl;
	
	//Print any values stored in array 'not_stored'
	for(i = 0; i < 10000; i++)
	{
		if(not_stored[i].get_num() != -1)
		{
			fout << not_stored[i].get_num() << endl;
		}	
	}


	//Print any values not found, or that required collision resolution
	fout << "Values that could not be found: " << endl;

	for(i = 0; i < nf_val; i++)
	{
		fout << not_found[i].get_num() << endl;
	}
	
	fout << "Values that were found after probing: " << endl;

	for(i = 0; i < f_val; i++)
	{
		if(found[i].get_probe_num() > 0)
		{
			fout << found[i].get_num() << " Probes: ";
			fout << found[i].get_probe_num() << endl;
		}
	}
	
	fout.close();

	return 0;
}
