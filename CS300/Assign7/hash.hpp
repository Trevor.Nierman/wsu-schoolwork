//Trevor Nierman
//R957F692
//Assignment 7

/*******************************************************************************
This file describes a class used in assignment 7 that stores the hashed value
as well as the actual number. 


--Data--
	int num - the actual number pulled from 'file.dat'.
	
	int hashed_num - the hashed number, serves as address in array
	
	int probe_num - number of probes needed to successfully store in array
	
	int const divisor - the number used by modulo divison to create hashed_num.

--Function-- 
		Accessor Functions:
			int get_num(void) - returns num. 
			
			int get_hash(void) - returns hashed_num.
			
			int get_probe_num(void) - returns probe_num.
			
			int get_divisor(void) - returns the divisor. 
			
		Mutator Functions:
			void set_num(int n) - sets num to n.
			
			void set_probe_num(int p) - sets probe_num to p.
			
		Class functions:
		--Hashing-- 
			-Uses modulo-division to hash a number. The constant the numbers are 
			divided is declared as a global constant at the top of the program 
			for ease of access. The number should be prime (likely 100,003, as
			suggested by the assignment).
			
			void calc_hashed_num(int num)
			{
				return num % CONSTANT;
			}
			
			inputs: num - the integer to be hashed
			
			outputs: num % CONSTANT  - the resulting hashed number is returned
			
*******************************************************************************/
#ifndef __HASH__
#define __HASH__
#include<iostream>
using namespace std;

class Hash
{
	int num;
	int hashed_num;
	int probe_num;

	public: 
	Hash();
	Hash(int n);
	
	//Accessor functions
	int get_num(void);
	int get_hashed_num(void);
	int get_probe_num(void);
	
	//Mutator functions
	void set_num(int n);
	void add_probe_num(void);
	void calc_hashed_num(void);

};

#endif
