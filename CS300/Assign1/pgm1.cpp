//Trevor Nierman
//ID: r957f692
//Program 1

/*A program used for inventorying purposes such as adding parts, 
recording parts in stock and reviewing all stock. */

#include <iostream>
#include <string>
#include <fstream>
#include <iomanip>
using namespace std;

const int MAX_PARTS = 100;	//Max number of parts 

//Part structure
struct part
{
	int part_num;	//Part number
	string item;	//Part name
	int quantity;	//Part quantity
	float price;	//Part price
	float total; 	//Value of all parts of this type, equals quantity * price
};

//Menu for inventory
int menu(void)
{
	int selection;
	
	cout << "What would you like to do?\n";
	cout << "\t1) Print parts\n";
	cout << "\t2) Enter a new part\n";
	cout << "\t3) Modify a part\n";
	cout << "\t4) Print inventory\n";
	cout << "\t5) Exit\n";
	cout << "Selection: ";
		 
	cin >> selection;
	
	return (selection);
}

//Makes new part
void make_part(part list[], int &count)
{
	// Array index of new part (one less than total count)
	int i = count;
	
	//Update current inventory count
	count = count + 1;

	//Create new part number
	list[i].part_num = count;
	
	//Create new item name
	cout << "Enter item name: ";
	cin.get();
	getline(cin, list[i].item);
	
	
	//Create new quantity
	cout << "Enter item quantity: ";
	cin  >> list[i].quantity;
	
	//Create new price
	cout << "Enter item price (dollars): ";
	cin >> list[i].price;
	
	//Create new total
	list[i].total = list[i].quantity * list[i].price;
}

//Menu for mod_part function. Determines 
int mod_menu(void)
{
	int choice; 
	
	cout << "Select what to modify: \n";
	cout << "   1) Item name\n";
	cout << "   2) Item quantity\n";
	cout << "   3) Item price\n";
	cout << "   4) Quit\n"; 
	
	cin >> choice;
	
	return(choice);
}

//Modifies current parts
void mod_part(part list[])
{
	//Array index, part number of part to be modified, users menu selection
	int i, num, choice;
	
	//Determine part to modify
	cout << "Enter part number of part to modify: ";
	cin >> num;
	
	//Index value is one less than the part number
	i = num - 1;
	
	cout << list[i].item << "\t";
	cout << list[i].quantity << "\t";
	cout << "$" << list[i].price << "\t";
	cout << "$" << list[i].total << "\n";
	
	do
	{
		choice = mod_menu();
		
		//Modifies item name
		if(choice == 1)
		{
			cout << "Enter new name: ";
			cin >> list[i].item;
		}
		
		//Modifies item quantity and total
		else if(choice == 2)
		{
			cout << "Enter new quantity: ";
			cin >> list[i].quantity;
			
			list[i].total = list[i].quantity * list[i].price;
		}
		
		//Modifies item price
		else if(choice == 3)
		{
			cout << "Enter new price: ";
			cin >> list[i].price;
			
			list[i].total = list[i].quantity * list[i].price;
		}
		
		else if(choice == 4)
		{
			cout << "Done modifying...\n";
		}
		
		//Invalid entry
		else
		{
			cout << "Please enter a valid selection: ";
		}
		
	}while(choice != 4);
}

//MAIN PROGRAM
int main(void)
{
	//Current inventory
	part list[MAX_PARTS];
	
	
	//Array index, inventory count
	int i = 0, count = 0;
		
	//File input variable
	ifstream fin;
	
	fin.open("inventory.txt");
	
	//Creates file if "inventory.txt" could not be found, or reads to array "list"
	if(fin.fail())
	{
		fin.open("inventory.txt", ios:: app);
	}
	
	else
	{
		//Count is first value in "inventory.txt"
		fin >> count;
		
		for(i = 0; i < 100; i ++)
		{
			
			fin >> list[i].part_num;
			fin >> list[i].quantity;
			fin >> list[i].price;
			fin >> list[i].total;
			getline(fin ,list[i].item);
		}
	}
	
	fin.close();
	
	//User's menu selection
	int choice;
	
	cout << "Hello!\n";
	
	do
	{
		choice = menu();
		
		//Prints part list
		if(choice == 1)
		{
			for(i = 0; i < count; i++)
			{
				cout << setw(15) << "Part No. " << list[i].part_num << "\t";
				cout << setw(15) << list[i].item << "\t";
				cout << setw(15) << list[i].quantity << "\t";
				cout << setw(15) << "$" << list[i].price << "\t";
				cout << setw(15) << "$" << list[i].total << "\n";
			} 
		}
		
		//Enter new part
		else if(choice == 2)
		{
			make_part(list, count);
		}
		
		//Modify parts
		else if(choice == 3)
		{
			mod_part(list);
		}
		
		//Print inventory total
		else if(choice == 4)
		{
			float sum = 0;
			
			for(i = 0; i < count; i++)
			{
				sum = sum + list[i].total;
			}
			
			cout << "The total price of inventory is $" << sum << "\n";
		}
		else if(choice == 5)
		{
			cout << "Exiting...\n";
		}
		
		else
		{
			cout << "Please make a valid selection.\n";  
		}
		
	}while(choice != 5);
	
	//Print data to "inventory.txt"
	ofstream fout;
	
	fout.open("inventory.txt");
	
	fout << count << "\n";
	
	for(i = 0; i < count; i++)
	{
		fout << list[i].part_num << " ";
		fout << list[i].quantity << " ";
		fout << list[i].price << " ";
		fout << list[i].total << " ";
		fout << list[i].item << "\n";
	}
	
	fout.close();
}
