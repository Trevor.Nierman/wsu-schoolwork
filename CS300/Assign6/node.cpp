//Trevor Nierman
//R957F692
//Assignment 6

/*******************************************************************************
This file defines the functions for the class "Node" used in a binary search 
tree for assignment 6.
*******************************************************************************/
#include<iostream>
#include<string>
#include "node.hpp"
using namespace std;

//CONSTRUCTOR
Node::Node(string n, int p, string a, string e)
{
	data.name = n;
	data.phone = p;
	data.address = a;
	data.email = e;
	left = NULL;
	right = NULL;
}

//Accessor Functions
node_data Node::get_data(void)
{
	return data;
}

string Node::get_name(void)
{
	return data.name;
}

int Node::get_phone(void)
{
	return data.phone;
}

string Node::get_address(void)
{
	return data.address;
}

string Node::get_email(void)
{
	return data.email;
}

Node* Node::get_left(void)
{
	return left;
}

Node* Node::get_right(void)
{
	return right;
}

//Mutator Functions
void Node::set_data(node_data d)
{
	data = d;
}

void Node::set_name(string n)
{
	data.name = n;
}

void Node::set_phone(int p)
{
	data.phone = p;
}

void Node::set_address(string a)
{
	data.address = a;
}

void Node::set_email(string e)
{
	data.email = e;
}

void Node::set_left(Node* l)
{
	left = l;
}

void Node::set_right(Node* r)
{
	right = r;
}

//Class Functions
void Node::print_node(void)
{
	cout << data.name << "\t";
	cout << data.phone << "\t";
	cout << data.address << "\t";
	cout << data.email << endl;
	cout << endl;
}
