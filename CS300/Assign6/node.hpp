//Trevor Nierman
//R957F692
//Assignment 6

/*******************************************************************************
This file defines the class node, which contains the data for the binary tree in
assignment 6. The data includes the name of the individual, their phone number,
address, and email, as well as pointers to the left and right subtrees. All data,
except the pointers, are stored in a structure. 

			Data -	string name - Individual's name.
			   
					int phone - Individual's phone number, used when searching 
					for	a specific node in the tree.
				
					string address - Individual's address.
				
					string email - Individual's email.
					
					Node *left - points to left subtree. NULL by default.
					
					Node *right - points to right subtree. NULL by default.
					
					Node *parent - points to the immediate ancestor.
				
			Functions - 	Accessor Functions
					string get_name() - returns the customer's name.
					
							Inputs: none
							Outputs: name (a string)
				
					int get_phone() - returns the customer's phone number.
					
							inputs: none
							outputs: phone (an integer)

					string get_address() - returns the customer's address.
					
							inputs: none
							outputs: address (a string)
				
					string get_email() - returns the customer's email.
					
							inputs: none
							outputs: email (a string)
					
					Node* get_left() - returns the left subtree.
					
							inputs: none
							outputs: left (a node pointer)
					
					Node* get_right() - returns the right subtree.
							
							inputs: none
							outputs: right (a node pointer)
							
					Node* get_parent() - returns the parent node.
							inputs: none
							outputs: parent (a node pointer)
							
							Mutator Functions
					void set_name(string n) - sets the customer's name to n. 
					Returns nothing.
							
							inputs: n (a string)
							outputs: none
							
					void set_phone(int p) - sets the customer's phone to p. 
					Returns	nothing.
							
							inputs: p (an integer)
							outputs: none
							
					void set_address(string a) - sets the customer's address 
					to a. Returns nothing.
							
							inputs: a (a string)
							outputs: none
							
					void set_email(string e) - sets the customer's email to e. 
					Returns nothing.
							
							inputs: e (a string)
							outputs: none
							
					void set_right(Node* r) - sets the customer's right 
					subtree to r. Returns nothing.
							
							inputs: r (a node pointer)
							outputs: none
							
					void set_left(Node* l) - sets the customer's left 
					subtree to l. Returns nothing. 
							
							inputs: l (a node pointer)
							outputs: none
							
					void set_parent(Node* p) - sets the customer's parent node
					to p. Returns nothing.
							
							inputs: p (a node pointer)
							outputs: none
							
*******************************************************************************/
#ifndef __NODE__
#define __NODE__
#include<iostream>
#include<string>
using namespace std;

struct node_data
{
	string name;
	int phone;
	string address;
	string email;
};

class Node
{
	node_data data;
	Node* left;
	Node* right;
	
	public:
	//CONSTRUCTOR
	Node(string n, int p, string a, string e);
	
	//Accessor Functions
	node_data get_data(void);
	string get_name(void);
	int get_phone(void);
	string get_address(void);
	string get_email(void);
	Node* get_left(void);
	Node* get_right(void);
	
	//Mutator Functions
	void set_data(node_data d);
	void set_name(string n);
	void set_phone(int p);
	void set_address(string a);
	void set_email(string e);
	void set_left(Node *l);
	void set_right(Node *r);
	
	//Class functions
	void print_node(void);
	
};

#endif
				
