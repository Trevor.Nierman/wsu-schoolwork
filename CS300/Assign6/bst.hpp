//Trevor Nierman
//R957F692
//Assignment 6

/*******************************************************************************
This file defines the class Binary_Tree which will be used in main.cpp for 
assignment 6. Binary_Tree contains a pointer to the root, and an integer, count, 
which tracks the total number of nodes contained in the tree. When searching for 
an individual, the class utilizes the individual's phone number to locate their
node. 
		
		Data -	Node *root - points to the root of the subtree
				
				int count - tracks the number of nodes in the binary search tree.
				
		Functions -	Accessor Functions
				Node* get_root() - returns root.
						Inputs: None
						Outputs: Root (as a Node pointer)
				
				int get_count() - returns count.
						Inputs: None
						Outputs: count (as an integer)
				
					Mutator Functions
				void set_root(Node* r) - sets root to r. Returns nothing.
						Inputs: r (a node pointer)
						Outputs: None
						
				void set_count(int c) - sets count to c. Returns nothing.
						Inputs: c (as an integer)
						Outputs: None
						
					Class Functions
				bool is_empty(void) - returns true if tree is empty.
						
						if(root is NULL)
							return true
						else 
							return false
							
						Inputs: None
						Outputs: T/F (a bool value)
						
				void insert_node(Node *root,Node *node) - inserts a node into
				the binary tree. Returns nothing. 
				
						if (root is empty) 
							root = node
						else if (root > node)
							insert(left subtree, node)
						else
							insert(right subtree, node)		
				
						Inputs: root (a Node pointer), node (also a node 
						pointer)
						Outputs: None
						 
				Node* search(root, key) - searches for a node in the
				binary search tree based on phone number. Returns the node when 
				found.
					
						if(root is empty)
							not found (return NULL)
						else if(root > key)
							search(left subtree, key)
						else if(root < key)
							search(right subtree, key)
						else
							found (return root)
						
						Inputs: root (a node pointer), key (an integer)
						Outputs: Node pointer
				
				Node find_smallest(root) - Traverses to the left-most subtree of
				root to find the smallest descendant of the root.
				
						if(root != NULL)
							return find_smallest(left_subtree)
						
						inputs: root (a node pointer)
						outputs: a node pointer
				
				Node find_largest(root) - Traverses to the right-most subtree of 
				the root to find the largest descendant of the root.
				
						if(root != NULL)
							return find_largest(right subtree)
						
						Inputs: root (a Node pointer)
						outputs: a Node pointer
				
				bool add_node(string n, int p, string a, string e, Node* root)
					- Adds a leaf to the tree in dynamic memory using passed in
					data. Returns true if successfully completed.
					
						create new Node
						insert_node(root, new node)
				
						inputs: Node data: {n (a string), p (an integer), 
						a (a string), e (a string)}, root (a node pointer)
						outputs: T/F (a boolean value)
				
				bool delete_node(root, key) - Deletes a node by first searching
				for it using the phone number and the search function, then 
				removing the node from dynamic memory. The tree then needs to be
				rearranged to preserve its stability.
						
						if(tree is empty)
							return false;
						else if(root > key)
							return delete_node(left subtree, key)
						else if(root < key)
							return delete_node(right subtree, key)
						else
							if(no left subtree)
								make right subtree root
								return true
							else if(no right subtree)
								make left subtree root
								return true
							else
								save root as d-node
								find_largest(left subtree)
								set largest of left subtree to d-node's location
								delete d-node
						
						Inputs: root (a node pointer), key (an integer)
						outputs: T/F (a boolean value)		
					
				void print_preorder(root) - traverses the tree in pre-order 
				(root first) and prints out all nodes. 
				
						if(root is not NULL)
							print root
							print_preorder(left subtree)
							print_preorder(right subtree)
				
						inputs: root (a node pointer)
						outputs: none
						
				void print_inorder(root) - traverses the entire tree in in-order
				(left, root, right) and prints out all nodes.
					
						if(root is not NULL)
							print_inorder(left subtree)
							print root
							print_inorder(right subtree)
							
						inputs: root (a node pointer)
						outputs: none
							
				void print_postorder(root) - traverses the entire tree in
				post-order (left, right, root) and prints out all nodes.
						
						if(root is not NULL)
							print_postorder(left subtree)
							print_postorder(right subtree)
							print root
							
						inputs: root (a node pointer)
						outputs: none	
						
*******************************************************************************/
#ifndef __BINARY_TREE__
#define __BINARY_TREE__
#include<iostream>
#include<fstream>
#include<string>
#include "node.hpp"
using namespace std;

class Binary_Tree
{
	Node *root;
	int count;
	
	public:
	//CONSTRUCTOR
	Binary_Tree();
	
	//Accessor Functions
	Node* get_root(void);
	int get_count(void);
	
	//Mutator Functions
	void set_root(Node* r);
	void set_count(int c);
	
	//Class Functions
	bool is_empty(void);
	bool insert_node(Node* curr, Node* n);
	Node* search(Node* curr, int key);
	Node* find_smallest(Node* curr);
	Node* find_largest(Node* curr);
	bool add_node(string n, int p, string a, string e);
	bool delete_root(int key);
	bool delete_node(Node* curr, int key);
	void print_pre_root(void);
	void print_preorder(Node* curr);
	void print_in_root(void);
	void print_inorder(Node* curr);
	void print_post_root(void);
	void print_postorder(Node* curr);
	bool print_root(void);
	bool print_nodes(Node* curr, ofstream &fout);
};
		
#endif
