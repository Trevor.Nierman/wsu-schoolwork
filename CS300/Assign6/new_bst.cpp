//Trevor Nierman
//R957F692
//Assignment 6

/*******************************************************************************
This file defines the functions for the class Binary_Tree used in Assignment 6. 
*******************************************************************************/
#include<iostream>
#include<string>
#include<fstream>
#include "bst.hpp"
using namespace std;

//CONSTRUCTOR
Binary_Tree::Binary_Tree()
{
	root = NULL;
	count = 0;
}

//Accessor Functions
Node* Binary_Tree::get_root(void)
{
	return root;
}

int Binary_Tree::get_count(void)
{
	return count;
}

//Mutator Functions
void Binary_Tree::set_root(Node* r)
{
	root = r;
}

void Binary_Tree::set_count(int c)
{
	count = c;
}

//Class Functions
bool Binary_Tree::is_empty(void)
{
	if(root == NULL)
	{
		return true;
	}
	
	else
	{
		return false;
	}

}

bool Binary_Tree::insert_node(Node* curr, Node* n)
{
	//If n is first node in tree, then root is set to n
	if(count == 0)
	{
		root = n;
			
		cout << "root made" << endl;
			
		return true;
	}
	
	//n's location is found
	else if(curr == NULL)
	{
		cout << "location found" << endl;
		
		curr = n;
		return true;
	}
	
	//Otherwise, if curr's phone is greater than n's, continue left
	else if(curr->get_phone() > n->get_phone())
	{
		return insert_node(curr->get_left(), n);
	}
	
	//Otherwise curr is less than n, continue right
	else
	{
		return insert_node(curr->get_right(), n);
	}
	
	return false;
	
}

Node* Binary_Tree::search(Node* curr, int key)
{
	//Node could not be found
	if(curr == NULL)
	{
		return NULL;
	}
	
	//If key is less than curr, search left
	else if(curr->get_phone() > key)
	{	
		return search(curr->get_left(), key);
	}	
	
	//If key is greater than curr, search right
	else if(curr->get_phone() < key) 
	{
		return search(curr->get_right(), key);
	}
	
	//Root found
	else
	{
		return curr;
	}
}

Node* Binary_Tree::find_smallest(Node* curr)
{
	//Traverse as far left as possible
	if(curr != NULL)
	{
		return find_smallest(curr->get_left());
	}
	
	//Returns curr once left-most node has been reached
	else
	{
		return curr;
	}
}

Node* Binary_Tree::find_largest(Node* curr)
{
	//Traverse as far right as possible
	if(curr != NULL)
	{
		return find_largest(curr->get_right());
	}
	
	//Returns curr once right-most node has been reached
	else
	{
		return curr;
	}
}

bool Binary_Tree::add_node(string n, int p, string a, string e)
{
	Node *temp = new Node(n, p, a, e);
	
	
	
	cout << "person made" << endl;
	
	
	
	if(insert_node(root, temp))
	{	
		count++;
		return true;
	}
	
	else
		return false;
}

bool Binary_Tree::delete_node(Node* curr, int key)
{
	
	//Check for empty tree
	if(is_empty())
	{
		return false;
	}
	
	//If curr's phone is greater than key, continue left
	else if(curr->get_phone() > key)
	{ 
		return delete_node(curr->get_left(), key);
	}
	
	//If curr's phone is less than key, continue right
	else if(key > curr->get_phone())
	{
		return delete_node(curr->get_right(), key);
	}
	
	//Key found
	else
	{
		Node *temp;
		//If node to be deleted only has a right subtree
		if(curr->get_left() == NULL)
		{
			//Exchange data between curr and right subtree
			curr->set_data(curr->get_right()->get_data());
			curr->set_left(curr->get_right()->get_left());
			
			//Right subtree's right pointer is temporarily saved
			temp = curr->get_right()->get_right();
			
			//Curr's right pointer is deleted
			delete curr->get_right();
			count--;
			
			//Curr's right subtree is set to temp
			curr->set_right(temp);
			
			return true;
		}
		
		//If node to be deleted only has a left subtree
		else if(curr->get_right() == NULL)
		{
			//Exchange data between curr and left subtree
			curr->set_data(curr->get_left()->get_data());
			curr->set_right(curr->get_left()->get_right());
			
			//Left subtree's left pointer is temporarily saved
			temp = curr->get_left()->get_left();
			
			//Curr's left pointer is deleted
			delete curr->get_left();
			count--;
			
			//Curr's left subtree is set to temp
			curr->set_left(temp);
			
			return true;
		}
		
		//Otherwise, curr is replaced with the smallest node in its right 
		//subtree, then is deleted.
		else
		{
			Node* new_curr = find_smallest(curr->get_right());
			
			//Exchange data between curr and new_curr
			curr->set_data(new_curr->get_data());
			
			//new_curr may have a right subtree: it is attached to new_curr's 
			//parent's left subtree
			if(new_curr->get_right() != NULL)
			{
				//Data exchanged between new_curr and its right subtree
				new_curr->set_data(new_curr->get_right()->get_data());
				new_curr->set_left(new_curr->get_right()->get_left());
				
				temp = new_curr->get_right()->get_right();
				
				delete new_curr->get_right();
				count--;
				
				new_curr->set_right(temp);
				
				return true;
			}
			
			else
			{
				delete new_curr;
				count--;
				
				return true;
			}
		}
	}
}

//Root processed first				
void Binary_Tree::print_preorder(Node* curr)
{
	if(curr != NULL)
	{
		curr->print_node();
	}
	
	print_preorder(curr->get_left());
	print_preorder(curr->get_right());
}

//Root processed second
void Binary_Tree::print_inorder(Node* curr)
{
	print_inorder(curr->get_left());
	
	if(curr != NULL)
	{
		curr->print_node();
	}
	
	print_inorder(curr->get_right());
}

//Root processed last
void Binary_Tree::print_postorder(Node* curr)
{
	print_postorder(curr->get_left());
	print_postorder(curr->get_right());
	
	if(curr != NULL)
	{
		curr->print_node();
	}
}

/*
node_data Binary_Tree::print_tree_data(Node* curr)
{
	if(curr != NULL)
	{
		
	}
	
	print_tree(curr->get_left(), fout);
	print_tree(curr->get_right(), fout);
}
	*/
