//Trevor Nierman
//R957F692
//Assignment 6

/*******************************************************************************
This assignment uses a binary search tree to organize a list of contact 
information, using the phone number as a reference. The program has the ability 
to print out the contact information in pre-,post-, and in-order, as well as
the ability to add, delete and locate nodes. The classes included, "Binary_Tree"
and "Node" are included in their respective .hpp files. The main program
utilizes a menu function detailed below.

			Function: int menu()
					Inputs: none
					Outputs: selection (an integer)
					
				- Displays a simple menu to allow the user to navigate through 
				the program
				
					display:
						"select an option: "
						{list all options}
						
					user input = selection
					return selection
					 
*******************************************************************************/
#include<iostream>
#include<string>
#include<fstream>
#include<iomanip>
#include "bst.hpp"
#include "node.hpp"
using namespace std;

int menu(void)
{
	int selection; 
	
	cout << "Select an option:" << endl;
	cout << "	1) Add an individual" << endl;
	cout << "	2) Delete an individual" << endl;
	cout << "	3) Search for an individual" << endl;
	cout << "	4) Print in pre-order" << endl;
	cout << "	5) Print in in-order" << endl;
	cout << "	6) Print in post-order" << endl;
	cout << "	7) Quit" << endl;
	
	cout << "Selection:";
	
	cin >> selection;
	
	return selection;
}

int main(void)
{
	Binary_Tree tree;
	
	//User selection, file stream index
	int selection;
	
	//Node data
	string name, address, email;
	int phone;
	
	//Search result
	Node* res;
	
	cout << "Hello!\n";
	
	selection = menu();
	
	do
	{
		
		
		
		//Add node
		if(selection == 1)
		{
			cout << "Name: ";
			cin.get();
			getline(cin, name);
			
			cout << "Phone: ";
			cin >> phone;
			
			cout << "Address: ";
			cin.get();
			getline(cin, address);
			
			cout << "Email: ";
			getline(cin, email);
			
			tree.add_node(name, phone, address, email);
		}
		
		//Delete node
			else if(selection == 2)
		{
			cout << "Phone number to delete: ";
			cin >> phone;
			
			cout << "Searching for number...";
			
			//Confirm deletion
			if(tree.delete_root(phone))
			{
				cout << "Deleted" << endl;
			}
			
			//Result not found
			else
			{
				cout << endl << "Number could not be found." << endl;
			}
						
		}
	
		//Search for a node
		else if(selection == 3)
		{
			cout << "Phone number to search: ";
			cin >> phone;
			
			res = tree.search(tree.get_root(), phone);
			
			//Results not found
			if(res == NULL) 
			{
				cout << "Number could not be found." << endl;
			}
			
			//Print results
			else
			{
				cout << setw(15) << res->get_name();
				cout << setw(15) << res->get_phone();
				cout << setw(15) << res->get_address();
				cout << setw(15) << res->get_email() << endl;
			}
		}
	
		//Print pre-order
		else if(selection == 4)
		{
			tree.print_pre_root();
		}
	
		//Print in-order
		else if(selection == 5)
		{
			tree.print_in_root();
		}
	
		//Print post-order
		else if(selection == 6)
		{
			tree.print_post_root();
		}
		
		selection = menu();
		
	//Quit
	}while(selection != 7);
	
	//Print tree to tree.dat
	cout << "Exporting data to 'tree.dat'..." << endl;
	
	tree.print_root();

	cout << "done." << endl;
	
	return 0;
}
