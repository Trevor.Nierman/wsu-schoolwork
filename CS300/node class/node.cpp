//Trevor Nierman
//r957f692
//Program3 - node.cpp

/* This file defines the class "node" that will be used to store book info
for the main function in pgm3.cpp */ 

#include<iostream>
#include<string>
#include"node.hpp"
using namespace std;

//Default Constructor
Node::Node()
{
	title = "Catch-22";
	author = "Joseph Heller";
	date = "07/04/1776";
	//('Merica)
	next = NULL;
}

//Overloaded Constructor
Node::Node(string t, string a, string d)
{
	title = t;
	author = a;
	date = d;
	next = NULL;
}

//Accessor Functions
Node* Node::get_next(void)
{
	return next;
}

string Node::get_title(void)
{
	return title;
}

string Node::get_author(void)
{
	return author;
}

//Mutator Functions
void Node::set_next(Node *n)
{
	next = n;
}

void Node::set_title(string t)
{
	title = t;
}

void Node::set_author(string a)
{
	author = a;
}

void Node::set_date(string d)
{
	date = d;
}

//Compares book titles (returns true if same, false if otherwise)
bool Node::compare_data(string t)
{
	if(t == title)
		return (true);
	else 
		return (false);
}

//Prints book data to screen
void Node::process_data(void)
{
	cout << title << "\t";
	cout << author << "\t";
	cout << date << "\n";
}
