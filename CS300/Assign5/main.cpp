//Trevor Nierman
//R957F692
//Program 5

/*******************************************************************************
Program 5 utilizes the node and stack classes to convert from a decimal (base 
10) number to a binary (base 2). Using the "Divide by 2" algorithm, the program 
converts the number by continuously dividing by 2, saving the remainder to the
stack, and dividing by two again until 0 is reached.

		Functions: 
			conv_binary(int x, stack s1) - converts an integer x from decimal to 
			binary form using a stack and nodes. The result is a print 
			out of the data contained within the stack, the function does not 
			actually return	anything. The stack is a class that contains several
			nodes, which contain the bits of x.
 
		Main: 
			Utilizes the function conv_binary to convert a decimal number to 
			binary form. The program first asks for user input, then preforms 
			the conversion using a stack and nodes while continually dividing 
			the input value. 
		
		Variables:
			x - user input. Converted to binary form. 
			s1 - a stack that stores the results of conv_binary. 
 
*******************************************************************************/
#include <iostream>
#include <cstring>
#include "node.hpp"
#include "stack.hpp"
using namespace std;

bool is_even(int x)
{
	if ((x % 2) == 0)
		return true;
	else 
		return false;
}

void conv_binary(int x, Stack &s1)
{
	do
	{
		//x is even
		if(is_even(x))
		{
			//Remainder is 0
			s1.push(0);
			
			x = x/2;
			
		}
		
		//x is odd
		else
		{
			//Remainder is 1
			s1.push(1);
			
			x = x/2;
		}
	
	}while(x != 0);
	
	//Print results
	while(!(s1.is_empty()))
	{
		s1.stack_top().print_node();
		s1.pop();
	}
}


//MAIN PROGRAM
int main(void)
{
	//Stack to be used in conv_binary function
	Stack s1;
	
	//Number to be converted
	int x;
	
	//User input
	char choice;
	
	cout << "Hello!\n";
	
	cout << "Would you like to convert a number to binary? (Y/N)\n";
	cout << "Selection: ";
	cin >> choice;
	do
	{
		//User chooses to convert
		if(choice == 'Y' || choice == 'y')
		{
			cout << "What number would you like to convert?\n";
			cout << "Selection: ";
			cin >> x;
		
			conv_binary(x, s1);
		
			//Ask user if they would like to convert another number
			cout << endl;
			cout << "Would you like to convert another number? (Y/N)\n";
			cout << "Selection: ";
			cin >> choice;
		}
		
	}while(choice == 'y' || choice == 'Y');
	
	cout << "Exiting program...\n";
	
	return 0;
}
	
