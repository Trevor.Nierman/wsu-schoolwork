//Trevor Nierman
//R957F692
//Program 5

/*******************************************************************************
This file describes the functions utilized by the node class that is utilized in
the main program for assignment 5.  
*******************************************************************************/
#include <iostream>
#include "node.hpp"
using namespace std;

//Default constructor
Node::Node()
{
	bit = 0;
}

//Overloaded constructor
Node::Node(int b)
{
	bit = b;
}

//Mutator functions
void Node::set_bit(int b)
{
	bit = b;
}

void Node::set_next(Node *n)
{
	next = n;
}

//Accessor functions
int Node::get_bit(void)
{
	return bit;
}

Node* Node::get_next(void)
{
	return next;
}

//Class functions
void Node::print_node(void)
{
	cout << bit;
}
