//Trevor Nierman
//R957F692
//Program 5

/*******************************************************************************
This file describes the "stack" classed to be used in the main program for
assignment 5. This class contains two data points: top, a Node-type pointer that
keeps track of the top element in the list, and count, an integer that tracks
the number of nodes in the stack.

				Class: Stack
	Data: 
		Node *top - points to the top of the stack at all times. Initially set
		to NULL when stack is empty. 
		
		int count - tracks number of nodes in the stack. Set to zero initially.

	Accessor functions:
		get_top - Returns pointer to top of the stack
		get_count - Returns number of nodes in stack

	Mutator functions:
		set_top - used to set the top pointer to a node. Returns nothing.
		set_count - used to manually set count to a number. Returns nothing.
		add_count - used to add 1 to count. Returns nothing.
		sub_count - used to subtract 1 from count. Returns nothing.
		
	Class functions:
		push - adds a node to the top of the stack. Returns nothing.
		pop - removes a node from the top of the stack. Returns nothing.
		Stack_top - Returns data from the top node in the stack. Does not effect
		the stack.
		Empty - Returns true if empty, false otherwise.
 
*******************************************************************************/
#ifndef __STACK__
#define __STACK__
#include <iostream>
#include "node.hpp"
using namespace std;

class Stack
{
	//Pointer to top of stack
	Node *top;
	
	//Number of nodes in stack
	int count;
	
	public:
	//Defualt constructor
	Stack();
	
	//Accessor functions
	Node *get_top(void)
	{
		return top;
	}
	
	int get_count(void)
	{
		return count;
	}
	
	//Mutator functions
	void set_top(Node *t);
	void set_count(int c);
	void add_count(void);
	void sub_count(void);
	
	//Functions
	bool push(int b);
	bool pop(void);
	Node stack_top(void);
	bool is_empty(void);
};
#endif
