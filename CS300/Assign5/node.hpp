//Trevor Nierman
//R957F692
//Program 5

/*******************************************************************************
This file describes the "node" class to be used in the main program for 
assignment 5. This class contains two data points, an integer value that is used
store a bit value that is calculated by the main function, and a pointer to the 
next node.

			Class: Node
	Data:
		int bit - either 0 or 1. Result of the "Divide by 2" algorithm for bit
		conversion.
		Node *next - points to next node in stack, if any. Otherwise set to NULL
		
	Accessor functions:
		get_bit - Returns the value of bit.
		get_next - Returns a pointer to the next node.
		
	Mutator functions:
		set_bit - sets the value of bit. Returns nothing.
		set_next - points "next" to a new location (hopefully the next node).
*******************************************************************************/
#ifndef __NODE__
#define __NODE__
#include <iostream>
using namespace std;

class Node
{
	int bit;
	Node *next;
	
	public:
	//Constructor
	Node();
	Node(int b);
	
	//Mutator Functions
	void set_bit(int b);
	void set_next(Node *n);
	
	//Accessor Functions
	int get_bit(void);
	Node *get_next(void);
	
	//Class functions
	void print_node(void);
	
};
#endif
