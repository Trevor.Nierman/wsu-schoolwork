//Trevor Nierman
//R957F692
//Program 5

/*******************************************************************************
This file contains the functions for "stack.hpp", a class to be used in
Assignment 5.
*******************************************************************************/
#include <iostream>
#include "stack.hpp"
#include "node.hpp"
using namespace std;

//Constructor
Stack::Stack()
{
	top = NULL;
	count = 0;
}

//Mutator Functions
void Stack::set_top(Node *t)
{
	top = t;
}

void Stack::set_count(int c)
{
	count = c;
}

void Stack::add_count(void)
{
	count++;
}

void Stack::sub_count(void)
{
	count--;
}

//Class Functions
bool Stack::push(int b)
{
	Node *ptr = new Node(b);
	
	//Check for empty stack
	if(is_empty())
	{
		//Set top pointer to new node
		set_top(ptr);
		
		//Update count
		add_count();
		
		return true;
	}
	
	else
	{
		//Set old "top" to new "next"
		ptr->set_next(top);
		
		//Set new node to top
		set_top(ptr);
		
		//Update count
		add_count();
		
		return true;
	}
	return false;
}

bool Stack::pop(void)
{
	Node *temp;
	//Check for empty stack
	if(is_empty())
	{
		return false;
	}
	
	else 
	{
		//Set temp to next node in stack
		temp = top->get_next();
		
		//Delete top node
		delete top;
		
		//Set new top to temp
		top = temp;
		
		return true;
	}
		
}

Node Stack::stack_top(void)
{
	if(is_empty() == false)
	{
		return top->get_bit();
	}
	else
	{
		cout << "No data available.\n";
		return false;
	} 
}

bool Stack::is_empty(void)
{
	if(top == NULL)
		return true;
	else
		return false;
}
