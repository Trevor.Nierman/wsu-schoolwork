//Trevor Nierman
//ID: r957f692
//Program 1

/*A program used for inventorying purposes such as adding parts, 
recording parts in stock and reviewing all stock. */

#include <iostream>
#include <string>
#include <fstream>
#include <iomanip>
using namespace std;

//Max number of parts 
const int MAX_PARTS = 100;	

//Part class
class Part
{
	int part_num, quantity;
	string item;
	float price, total;
	
	public:
		//Constructor
		#ifdef	CONSTRUCTOR
			Part(int num, string name, int quant, float p);
		#endif
		
		//Mutator Functions
		void set_item(string name);
		void set_quantity(int quant);
		void set_price(float p);
		void set_num(int num);
		void set_tot(float tot);

		//Accessor functions
		int get_num(void);
		string get_item(void);
		int get_quantity(void);
		float get_price(void);
		float get_total(void);
		
		//Calculates the total
		void calc_total(void);
};

#ifdef CONSTRUCTOR
Part::Part(int num, string name, int quant, float p)
{
	part_num = num;
	quantity = q;
	item = name;
	price = pr;
}
#endif

//Mutator functions
void Part::set_item(string name)
{
	item = name;
}

void Part::set_quantity(int quant)
{
	quantity = quant;
}

void Part::set_price(float p)
{
	price = p;
}

void Part::set_num(int num)
{
	part_num = num;
}

void Part::set_tot(float tot)
{
	total = tot;
}

void Part::calc_total(void)
{
	total = quantity * price;
}

//Accessor Functions
int Part:: get_num(void)
{
	return(part_num);
}

string Part::get_item(void)
{
	return(item);
}

int Part::get_quantity(void)
{
	return(quantity);
}

float Part::get_price(void)
{
	return(price);
}

float Part::get_total(void)
{
	return(total);
}

//Menu for inventory
int menu(void)
{
	int selection;
	
	cout << "What would you like to do?\n";
	cout << "\t1) Print parts\n";
	cout << "\t2) Enter a new part\n";
	cout << "\t3) Modify a part\n";
	cout << "\t4) Print inventory\n";
	cout << "\t5) Exit\n";
	cout << "Selection: ";
		 
	cin >> selection;
	
	return (selection);
}

//Makes new part
void make_part(Part list[], int &count)
{
	// Array index of new part (one less than total count)
	int i = count;
	//Quantity input
	int quant;
	//Price input
	float p;
	//Item name input
	string name;
	
	//Update current inventory count
	count = count + 1;

	//Create new part number
	list[i].set_num(count);
	
	//Create new item name
	cout << "Enter item name: ";
	cin.get();
	getline(cin, name);
	
	list[i].set_item(name);
	
	//Create new quantity
	cout << "Enter item quantity: ";
	cin >> quant;
	
	list[i].set_quantity(quant);
	
	//Create new price
	cout << "Enter item price (dollars): ";
	cin >> p;
	list[i].set_price(p);
	
	//Create new total
	list[i].calc_total();
}

//Menu for mod_part function. Determines what the user wishes to modify.
int mod_menu(void)
{
	int choice; 
	
	cout << "Select what to modify: \n";
	cout << "   1) Item name\n";
	cout << "   2) Item quantity\n";
	cout << "   3) Item price\n";
	cout << "   4) Quit\n"; 
	
	cin >> choice;
	
	return(choice);
}

//Modifies current parts
void mod_part(Part list[])
{
	//Array index, part number of part to be modified, users menu selection
	int i, num, choice;
	//Item name input
	string name;
	//Item quantity input
	int quant;
	//Item price input
	float p;
	
	//Determine part to modify
	cout << "Enter part number of part to modify: ";
	cin >> num;
	
	//Index value is one less than the part number
	i = num - 1;
	
	cout << list[i].get_item() << "\t";
	cout << list[i].get_quantity() << "\t";
	cout << "$" << list[i].get_price() << "\t";
	cout << "$" << list[i].get_total() << "\n";
	
	do
	{
		choice = mod_menu();
		
		//Modifies item name
		if(choice == 1)
		{
			cout << "Enter new name: ";
			cin >> name;
			list[i].set_item(name);
		}
		
		//Modifies item quantity and total
		else if(choice == 2)
		{
			cout << "Enter new quantity: ";
			cin >> quant;
			
			list[i].set_quantity(quant);
			list[i].calc_total();
		}
		
		//Modifies item price and total
		else if(choice == 3)
		{
			cout << "Enter new price: ";
			cin >> p;
			
			list[i].set_price(p);
			list[i].calc_total();
		}
		
		//Finished modifying
		else if(choice == 4)
		{
			cout << "Done modifying...\n";
		}
		
		//Invalid entry
		else
		{
			cout << "Please enter a valid selection: ";
		}
		
	}while(choice != 4);
}

//MAIN PROGRAM
int main(void)
{
	//Current inventory
	Part list[MAX_PARTS];
	
	
	//Array index, inventory count
	int i = 0, count = 0;
		
	//File input variable
	ifstream fin;
	
	//***File input variables***
	//Item price
	float p;
	//Item number
	int num;
	//Item quantity
	int quant;
	//Item total
	float tot;
	//Item name
	string name;
	
	
	fin.open("inventory.txt");
	
	//Creates file if "inventory.txt" could not be found, or reads to array "list"
	if(fin.fail())
	{
		fin.open("inventory.txt", ios:: app);
	}
	
	else
	{
		//Count is first value in "inventory.txt"
		fin >> count;
		
		for(i = 0; i < 100; i ++)
		{
			fin >> num;
			list[i].set_num(num);
			
			fin >> quant;
			list[i].set_quantity(quant);
			
			fin >> p;
			list[i].set_price(p);
			
			fin >> tot;
			list[i].set_tot(tot);
			
			getline(fin, name);
			list[i].set_item(name);
		}
	}
	
	fin.close();
	
	//User's menu selection
	int choice;
	
	cout << "Hello!\n";
	
	do
	{
		choice = menu();
		
		//Prints part list
		if(choice == 1)
		{
			for(i = 0; i < count; i++)
			{
				cout << setw(15) << "Part No. " << list[i].get_num() << "\t";
				cout << setw(15) << list[i].get_item() << "\t";
				cout << setw(15) << list[i].get_quantity() << "\t";
				cout << setw(15) << "$" << list[i].get_price() << "\t";
				cout << setw(15) << "$" << list[i].get_total() << "\n";
			} 
		}
		
		//Enter new part
		else if(choice == 2)
		{
			make_part(list, count);
		}
		
		//Modify parts
		else if(choice == 3)
		{
			mod_part(list);
		}
		
		//Print inventory total
		else if(choice == 4)
		{
			float sum = 0;
			
			for(i = 0; i < count; i++)
			{
				sum = sum + list[i].get_total();
			}
			
			cout << "The total price of inventory is $" << sum << "\n";
		}
		else if(choice == 5)
		{
			cout << "Exiting...\n";
		}
		
		else
		{
			cout << "Please make a valid selection.\n";  
		}
		
	}while(choice != 5);
	
	//Print data to "inventory.txt"
	ofstream fout;
	
	fout.open("inventory.txt");
	
	fout << count << "\n";
	
	for(i = 0; i < count; i++)
	{
		fout << list[i].get_num() << " ";
		fout << list[i].get_quantity() << " ";
		fout << list[i].get_price() << " ";
		fout << list[i].get_total() << " ";
		fout << list[i].get_item() << "\n";
	}
	
	fout.close();
	
return(0);
}
