/*******************************************************************************
Trevor Nierman
r957f692
Program 4

	A program that implements a double linked list to store and retrieve data
	from a list called booklist.txt. The program first checks to ensure that
	booklist.txt exists, creating it if it does not. Upon exiting, the file
	booklist.txt is updated. The program utilizes the Node and Linked_list
	classes and their functions. 

		
	Functions:
	menu - a function that acts as the user interface. Displays options, and 
	allows users to make a selection.	
*******************************************************************************/
#include<iostream>
#include<fstream>
#include<string>
#include "Link_list.hpp"
#include "node.hpp"
using namespace std;

void clear_input()
{
  while (getchar() != '\n');
}

//Menu function
int menu(void)
{
	int choice;
	
	cout << "Please choose an option from the list:\n";
	cout << "	1) Add a book\n";
	cout << " 	2) Delete a book\n";
	cout << "	3) Retrieve a book\n";
	cout << "	4) Print all books starting from beginnning of list\n";
	cout << "	5) Print all books starting from end of list\n";
	cout << "	6) Exit program\n";
	
	cout << "User selection: ";
	cin >> choice;
	
	return choice;
}
	
//MAIN PROGRAM
int main(void)
{
	//Create counter for file instream
	int i;
	
	//Create unordered double linked list
	Linked_list list;
	
	//Create file stream, open booklist.txt if it exists
	ifstream fin;
	
	fin.open("booklist.txt");
	
	//Create booklist.txt if file does not exist
	if(fin.fail())
	{
		cout << "File could not be found. Creating new file...\n";
		fin.open("booklist.txt", ios::app);
	}
	
	//Read in data if list exists
	else
	{
		//List count is first entry
		fin >> i;
		list.set_count(i);
	
		//Creates pointer "temp" which acts as a pointer to the previous node
		//Node *temp = NULL, *ptr;
		string in_t, in_a, in_d; 
	
		for(i = 0; i < list.get_count(); i ++)
		{
		
			//Add node
			fin >> in_t;
			fin >> in_a;
			fin >> in_d;		
			list.add_node(in_t, in_a, in_d);
	
		}
	}
	
	fin.close();
	
	//User menu selection
	int choice;
	
	//Inputed book data
	string title;
	string author;
	string date;
	
	//Used in options 2, 3 to search for books
	string book1;
	
	//Display menu
	do 
	{
		choice = menu();
		
		//Add book
		if(choice == 1)
		{
			cout << "Adding book...\n";
			
			cout << "Enter title: ";
			cin.get();
			getline(cin, title);
			cout << title << "\n";
			
			cout << "Enter author: ";
			getline(cin, author);
			cout << author<< "\n";
			
			cout << "Enter date read: ";
			getline(cin, date);
			cout << date << "\n";
			
			list.add_node(title, author, date);
			
			cout << "Book added.\n";
			cout << list.get_count() << "\n";
		}
		
		//Delete book
		else if(choice == 2)
		{
			cout << "Enter a title to delete: ";
			cin >> book1;
			
			list.find_node(book1);
			if(list.delete_node())
			{
				cout << "All results were successfully deleted.\n";
			}
		}
		
		//Retrieve a book
		else if(choice == 3)
		{
			cout << "Enter a title to find: ";
			cin >> book1;
			
			list.find_node(book1);
			list.print_nodes();
		}
		
		//Print from head
		else if(choice == 4)
		{
			list.traverse_forward();
		}
		
		//Print from tail
		else if(choice == 5)
		{
			list.traverse_backward();
		}
		
		//Exit
		else if(choice == 6)
		{
			cout << "Have a nice day!\n";

		}
		
		else
		{
			cout << "Invalid selection. ";
		}
		
	}while(choice != 6);
	
	cout << "Terminating program...\n";
	
	//Print data to "booklist.txt"
	
	ofstream fout;
	Node *outptr = list.get_head();
	
	fout.open("booklist.txt");
	
	//Print book count
	fout << list.get_count() << "\n";
	
	//Print book data
	for(i = 0; i < list.get_count(); i++)
	{
		fout << outptr->get_title() << " ";
		fout << outptr->get_author() << " ";
		fout << outptr->get_date() << "\n";
		
		outptr = outptr->get_next();
	}
	
	fout.close();
	
	return (0);
}
