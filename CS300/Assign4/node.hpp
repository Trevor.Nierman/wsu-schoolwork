/*******************************************************************************
Trevor Nierman
r957f692
Program 3 - node.hpp


This class is used to store book data in a list. Its members include strings for
the author, title, and date read, as well as a pointer to the next list element.
The class contains functions that return and set the pointer to the next list 
element, returns the author, date and title information, and functions that 
compares and prints class data to the screen.

	Class: Node 
	Data: string title - stores the title of the book
		  string author - stores the author of the book
		  string date - stores the date the book was read
		  Node *next - stores pointer to next node
	
	Accessor Functions:
		 Node *get_next - points to next node
		 string get_author - returns "author"
		 string title - returns "title"
	
	Mutator Functions:
		 void *set_next - sets "next" to next pointer
				Input: pointer to next element (becomes "next")
	Functions:
		 Node - overloads constructor function to set book title
				Input: title of book 
				Output: None (updates book title)
				
		 compare_book_title - compares two books' authors, returning true if same
		 and false if otherwise.
				Input: title of book to be compared against 
				Output: Returns value (-1, 0, 1) based on whether book compared 
				is alphabetically before or after other.
				
		compare_book_author - compares two books' authors, returning true if same
		and false if otherwise.
		
		
				
		 process_data - prints list data to screen
				Input: pointer to header
				Output: returns book data




*******************************************************************************/
#ifndef __NODE__
#define __NODE__
#include<iostream>
#include<string>

using namespace std;

class Node
{
	//Class data
	string title;
	string author;
	string date;
	
	//Points to next list element
	Node *next, *prev; 
	
	public:
		//Constructor
		Node();
		Node(string t, string a, string d);
		
		//Accessor functions
		Node* get_next(void);
		Node* get_prev(void);
		string get_title(void);
		string get_author(void);
		string get_date(void);
		
		//Mutator functions
		void set_next(Node *n);
		void set_prev(Node *n);
		void set_title(string t);
		void set_author(string a);
		void set_date(string d);
		
		//Compares book data
		bool compare_book_title(string t);
		bool compare_book_author(string a);
		
		//Prints book data to screen
		void process_data(void);
};
#endif
