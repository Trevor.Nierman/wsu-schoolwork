//Trevor Nierman
//R957F692
//Program 4

/* This program defines all functions used by the class Linked_list in 
in main.cpp for Assignment 4. */

#include<iostream>
#include<string> 
#include "Link_list.hpp"
#include "node.hpp"

//Constructor
Linked_list::Linked_list()
{
	head = NULL;
	tail = NULL;
	count = 0;
}

//Mutator functions
void Linked_list::set_count(int c)
{
	count = c;
}

void Linked_list::add_count(void)
{
	count++;
}

void Linked_list::sub_count(void)
{
	count--;
}

void Linked_list::set_head(Node *p)
{
	head = p;
}

void Linked_list::set_tail(Node *p)
{
	tail = p;
}

void Linked_list::set_res_list(int i, Node *p)
{
	res_list[i] = p;
}

void Linked_list::set_res(int i)
{
	res = i;
}

void Linked_list::add_res(void)
{
	res++;
}

void Linked_list::add_node(string t, string a, string d)
{
	//Create a generic pointer to point to new node
	Node *ptr = new Node(t,d,a);
	Node *prev;
	
	//If element is first added to list, set to head and tail
	if(head == NULL)
	{
		head = ptr;
		tail = ptr;
	}	
	
	//Otherwise, pointer is added to end of list, set as new tail.
	else
	{	
		prev = tail;
		tail = ptr;
		
		ptr->set_prev(prev);
		cout << "This worked\n";		
		
		prev->set_next(ptr);
		cout << "This worked too\n";
	}
	
	//Update count
	add_count();
}

//Accessor functions

Node* Linked_list::get_head(void)
{
	return head;
}
		
int Linked_list::get_count(void)
{
	return count;
}
		
Node* Linked_list::get_tail(void)
{
	return tail;
}
			
Node* Linked_list::get_res_list(int i)
{
	return (res_list[i]);
}

int Linked_list::get_res(void)
{
	return res;
}

bool Linked_list::delete_node(void)
{
	//User choice
	string choice;
	
	//Create index
	int i;
	
	//Create temporary node
	Node *temp;
	
	//Print all results
	for(i = 0; i < get_res(); i++)
	{
		temp = get_res_list(i);
		
		temp->process_data();
	}
	if(get_res() > 0)
	{
		cout << "Delete all results? (Y/N)";
		cout << "Selection: ";
		cin >> choice;
		do
		{
			//Delete all results
			if(choice == "Y" || choice == "y")
			{
				for(i = 0; i < get_res(); i++)
				{
					temp = get_res_list(i);
			
					delete temp;
					return true;
				}
			}
	
			//Terminate function
			else if(choice == "N" || choice == "n")
			{
				cout << "Results were not deleted.\n";
				return false;
			}
		
			//Invalid selection
			else
			{
				cout << "Please choose a valid selection (Y/N)";
			}
		}while(choice != "y" || choice != "Y" || choice != "n" || choice != "N");
	}
	else
	{
		return false;
	}
}

//Other Linked_list functions
bool Linked_list::find_node(string t)
{
	Node *curr;
	int i;
	
	//Set search results back to 0
	set_res(0);
	
	curr = head;
	
	//Search entire list for results
	for(i = 0; i < count; i++)
	{
		if(curr->compare_book_title(t))
		{
			set_res_list(res, curr);
			add_res();
			curr->get_next();
		}
		else
		{
			curr->get_next();
		}
	}
	
	//Grammar matters
	if(get_res() == 1)
	{
		cout << "There was " << get_res() << " match for your search.\n";
		return true;
	}
	
	else if(get_res() > 1)
	{
		cout << "There were " << get_res() << " matches for your search.\n";
		return true;
	}
	
	else
	{
		cout << "There were no results for your search.\n";
		return false;
	}
}

void Linked_list::print_nodes(void)
{
	int i;
	Node *temp;
	
	
	for(i = 0; i < get_res(); i++)
	{
		temp = get_res_list(i);
		temp->process_data();
	}
}

void Linked_list::traverse_forward(void)
{
	Node *ptr = head;
	int i;
	
	if(count == 0)
	{
		cout << "There are no results to print.\n";
	}
	
	else
	{
		for(i = 0; i < count; i++)
		{
			ptr->process_data();
		
			ptr->get_next();
		}
	}	
}

void Linked_list::traverse_backward(void)
{
	Node *ptr = tail;
	int i;
	
	if(count == 0)
	{
		cout << "There are no results to print.\n";
	}
	
	else
	{
		for(i = 0; i < count; i++)
		{
			ptr->process_data();
		
			ptr->get_prev();
		}
	}
}
