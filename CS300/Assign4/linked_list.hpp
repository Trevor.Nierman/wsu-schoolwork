/*******************************************************************************
//Trevor Nierman
//R957F692
//Program 4

Class: Linked_list
		Node *head - used to track first element in the list
		Node *tail - used to track last element in the list
		Node *res - points to array that stores search results for functions
		int count - determines number of elements in list
		
		Accessor Functions:
		Node *get_header - returns the pointer to first element
		Node *get_tail - returns the pointer to the last element
		int get_count - returns current count of elements
		
		Mutator Functions:
		add_count - adds one to count
		sub_count - subtracts one from count
		add_node - adds another node to list. If first node, sets as head and
		tail
		delete_node - deletes node from list
		
		Functions:
		find_node - searches list for a specific book title, returning
		all nodes that match to an array

*******************************************************************************/
#ifndef __LINKED_LIST__
#define __LINKED_LIST__
#include<iostream>
#include<string>
#include "node.hpp"
using namespace std;

class Linked_list
{
	//Pointers to head, tail of list
	Node *head, *tail;
	
	//A list of results from find_node function. Limited to 100 results
	Node *res_list[100];
	
	//Number of books in list, number of search results
	int count, res;
	
	public:
		//Constructors
		Linked_list();
		
		//Mutators
		void add_node(string t, string a, string d);
		void set_count(int c);
		void add_count(void);
		void sub_count(void);
		void set_head(Node *p);
		void set_tail(Node *p);
		void set_res_list(int i, Node *p);
		void set_res(int i);
		void add_res(void);
		bool delete_node(void);
		
		//Accessor
		Node *get_head(void);
		int get_count(void);
		Node *get_tail(void);		
		Node *get_res_list(int i);
		int get_res(void);
		
		//Functions
		bool find_node(string t);
		void print_nodes(void);
		void traverse_forward(void);
		void traverse_backward(void);
};
#endif
