//Trevor Nierman
//R957F692
//Project 2

/*******************************************************************************
This file defines the class node, which contains the data for the binary tree in
project 2. The data includes the name of the individual, their phone number,
address, and email, as well as pointers to the left and right subtrees. All data,
except the pointers, are stored in a structure. 

			Data -	data_struct data - structure that contains data for the node. 
					Consists of an integer, key, and a double, number.
					
					Node *left - points to left subtree. NULL by default.
					
					Node *right - points to right subtree. NULL by default.
				
			Functions - 	Accessor Functions
					data_struct get_data(void) - returns the structure "data".
					
					int get_key(void) - returns the key as an int.
					
					double get_number(void) -returns number as a double.
					
					Node* get_left() - returns the left subtree.
					
					Node* get_right() - returns the right subtree.
							
							Mutator Functions
					void set_data(int k, double num) - sets key and number 
					within structure "data" to k and num, respectively.
					Returns nothing.
					
					void set_right(Node* r) - sets the customer's right 
					subtree to r. Returns nothing.
							
					void set_left(Node* l) - sets the customer's left 
					subtree to l. Returns nothing. 

*******************************************************************************/
#ifndef __NODE__
#define __NODE__
#include<iostream>
#include<string>
#include "data_struct.hpp"
using namespace std;

class Node
{
	data_struct data;
	Node* left;
	Node* right;
	
	public:
	//CONSTRUCTOR
	Node(int k, double num);
	
	//Accessor Functions
	int get_key(void);
	double get_number(void);
	data_struct get_data(void);
	Node* get_left(void);
	Node* get_right(void);
	
	//Mutator Functions
	void set_key(int k);
	void set_number(double num);
	void set_data(data_struct dat);
	void set_left(Node* l);
	void set_right(Node* r);
	
	//Class functions
	void print_node(void);
	
};

#endif
				
