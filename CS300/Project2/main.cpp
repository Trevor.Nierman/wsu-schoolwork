//Trevor Nierman
//R957F692
//Jacob Hoggatt
//w643w733
//Project 2

/*******************************************************************************
Project 2 creates arrays of 500, 5000, and 10000 elements, and gives the user 
option to sort them using selection sort, insertion sort, bubble sort, quick 
sort, or a Binary Search Tree's inorder function. 
					  
		Functions:			
			-- Generate small array -- 
				Description: Generates an unordered array of 500 data_struct
				structures using random data. 
				
				inputs: data_struct small_list - empty list of 500 elements.
				
				outputs: data_struct small_list - fully populated list of 500
												  elements.
				
					void make_small_array(data_struct small_list)
						int i, key
						double number
						
						for(i = 0; i < 500; i++)
							list[i].key = random number
							list[i].number = random number
							
			-- Generate medium array -- 
				Description: Generates an unordered array of 5000 data_struct
				structures using random number generation.
				
				inputs: data_struct med_list - empty list of 5000 elements.
				
				outputs: data_struct med_list - fully populated list of 5000 
												elements.
												
					void make_medium_list(data_struct med_list)
						int i, key
						double number
						
						for(i = 0; i < 5000; i++)
							list[i].key = random number
							list[i].number = random number
							
			-- Generate large array --
				Description: Generates an unordered array of 10000 data_struct 
				structures using random number generation.
				
				inputs: data_struct big_list - empty list of 10000 elements.
				
				outputs: data_struct big_list - fully populated list of 10000
												elements.
												
					void make_big_list(data_struct big_list)
						int i, key
						double number
						
						for(i = 0; i < 10000; i++)
							list[i].key = random number
							list[i].number = random number
							
			-- Generate small Binary Search Tree --
				Description: Generates a binary search tree of 500 nodes in 
				order to complete the in-order sort method.
				
				inputs: data_struct small_list - fully populated list of 500 
												 elements.
												 
				outputs: small_bst - fully populated Binary Search tree with 500
									 nodes.
							
					void make_small_bst(data_struct small_list)
						int i
						
						for(i = 0; i < 500; i++)
							new node = list[i]
							insert(small_bst, new node)
							
			-- Generate medium Binary Search Tree --
				Description: Generates a binary search tree of 5000 nodes in 
				order to complete the in-order sort method.
				
				inputs: data_struct med_list - fully populated list of 5000
												 elements.
												 
				outputs: med_bst - fully populated Binary Search tree with 
									 5000 nodes.
									 
					void make_med_bst(data_struct med_list)
						int i
						
						for(i = 0; i < 5000; i++)
							new node = list[i]
							insert(med_bst, new node)
							
			-- Generate large Binary Search Tree -- 
				Description: Generates a binary search tree of 10000 nodes in 
				order to complete the in-order sort method.
				
				inputs: data_struct large_list - fully populated list of 10000
												 elements.
												 
				outputs: med_bst - fully populated binary search tree with 
									10000 nodes.
									
					void make_large_bst(data_struct large_list)
						int i
						
						for(i = 0; i < 10000; i++)
							new node = list[i]
							insert(large_bst, new node)
							
*******************************************************************************/
#include<iostream>
#include<cstdlib>
#include "bst.hpp"
#include "node.hpp"
#include "data_struct.hpp"
using namespace std;

int const small_val = 500;
int const med_val = 5000;
int const large_val = 10000;

void insert_sort(data_struct list[], int last, int &passes);
void bubble_sort(data_struct list[], int last, int &passes);
void sel_sort(data_struct list[], int last, int &passes);
int quicksort(data_struct list[], int left, int right, int& count);


void gen_small_list(data_struct small_list[])
{
	int i;
	
	//Generate a random seed based on time
	srand(time(0));
	
	for(i = 0; i < small_val; i++)
	{
		//Utilizes seed to create random numbers for each element's data
		small_list[i].key = rand();
		small_list[i].number = rand();
	}
}

void gen_med_list(data_struct med_list[])
{
	int i;
	
	//Generates random seed based on time
	srand(time(0));
	
	for(i = 0; i < med_val; i++)
	{
		//Utilizes seed to create random numbers for each element's data
		med_list[i].key = rand();
		med_list[i].number = rand();
	}
}

void gen_large_list(data_struct large_list[])
{
	int i;
	
	//Generates random seed based on time
	srand(time(0));
		
	for(i = 0; i < large_val; i++)
	{
		//Utilizes seed to create random numbers for each element's data.
		large_list[i].key = rand();
		large_list[i].number = rand();
	}
}

void gen_small_bst(data_struct small_list[], Binary_Tree small_bst)
{
	int i;
	
	
	for(i = 0; i < small_val; i++)
	{
		
		small_bst.add_node(small_list[i].key, small_list[i].number);
	}
}

void gen_med_bst(data_struct med_list[], Binary_Tree med_bst)
{
	int i;
	
	for(i = 0; i < med_val; i++)
	{
		med_bst.add_node(med_list[i].key, med_list[i].number);
	}
}

void gen_large_bst(data_struct large_list[], Binary_Tree large_bst)
{
	int i;
	
	for(i = 0; i < large_val; i++)
	{
		large_bst.add_node(large_list[i].key, large_list[i].number);
	}
}


int main(void)
{
	//Index (for various "for" loops)
	int i;
	
	//Track number of passes for each list
	int small_passes = 0;
	int med_passes = 0;
	int large_passes = 0;
	
	//Create master arrays
	data_struct small_list[small_val];
	data_struct med_list[med_val];
	data_struct large_list[large_val];
	
	//Create Binary Search Trees
	Binary_Tree small_tree;
	Binary_Tree med_tree;
	Binary_Tree large_tree;
	
	//Populate master arrays
	gen_small_list(small_list);
	gen_med_list(med_list);
	gen_large_list(large_list);
	
	//Make 4 copies of arrays - one for each sort
	//Bubble
	data_struct small_bub_list[small_val];
	for(i = 0; i < small_val; i++)
	{
		small_bub_list[i] = small_list[i];
	}
	
	data_struct med_bub_list[med_val];
	for(i = 0; i < med_val; i++)
	{
		med_bub_list[i] = med_list[i];
	}
	
	data_struct large_bub_list[large_val];
	for(i = 0; i < large_val; i++)
	{
		large_bub_list[i] = large_list[i];
	}
	
	//Insertion
	data_struct small_ins_list[small_val];
	for(i = 0; i < small_val; i++)
	{
		small_ins_list[i] = small_list[i];
	}
	
	data_struct med_ins_list[med_val];
	for(i = 0; i < med_val; i++)
	{
		med_ins_list[i] = med_list[i];
	}
	
	data_struct large_ins_list[large_val];
	for(i = 0; i < large_val; i++)
	{
		large_ins_list[i] = large_list[i];
	}
	
	//Quick
	data_struct small_quick_list[small_val];
	for(i = 0; i < small_val; i++)
	{
		small_quick_list[i] = small_list[i];
	}
	
	data_struct med_quick_list[med_val];
	for(i = 0; i < med_val; i++)
	{
		med_quick_list[i] = med_list[i];
	}
	
	data_struct large_quick_list[large_val];
	for(i = 0; i < large_val; i++)
	{
		large_quick_list[i] = large_list[i];
	}
	
	//Selection
	data_struct small_sel_list[small_val];
	for(i = 0; i < small_val; i++)
	{
		small_sel_list[i] = small_list[i];
	}
	
	data_struct med_sel_list[med_val];
	for(i = 0; i < med_val; i++)
	{
		med_sel_list[i] = med_list[i];
	}
	
	data_struct large_sel_list[large_val];
	for(i = 0; i < large_val; i++)
	{
		large_sel_list[i] = large_list[i];
	}
	
	//Populate trees
	gen_small_bst(small_list, small_tree);
	gen_med_bst(med_list, med_tree);
	gen_large_bst(large_list, large_tree);
	
	//Begin sorts
	//Run bubble sort
	bubble_sort(small_bub_list, small_val - 1, small_passes);
	
	cout << "Small bubble done." << endl;
	
	bubble_sort(med_bub_list, med_val - 1, med_passes);
	
	cout << "med bubble done." << endl;
	
	bubble_sort(large_bub_list, large_val - 1, large_passes);
	
	cout << "large bubble done." << endl;
	
	//Print bubble results
	ofstream fout;
	
	fout.open("bub500.dat");
	
	if(fout.fail())
	{
		cout << "File not found. Creating 'bub500.dat'...";
		fout.open("bub500.dat", ios::app);
		cout << "done." << endl;
	}
	
	//Print small passes
	fout << small_passes << endl;
	
	//Print list
	for(i = 0; i < small_val; i++)
	{
		fout << small_bub_list[i].key << " ";
		fout << small_bub_list[i].number << endl;
	}
	
	fout.close();
	
	
	
	fout.open("bub5000.dat");
	
	if(fout.fail())
	{
		cout << "File not found. Creating 'bub5000.dat'...";
		fout.open("bub5000.dat", ios::app);
		cout << "done." << endl;
	}
	
	//Print medium passes
	fout << med_passes << endl;
	
	//Print list
	for(i = 0; i < med_val; i++)
	{
		fout << med_bub_list[i].key << " ";
		fout << med_bub_list[i].number << endl;
	}
	
	fout.close();
	
	
	fout.open("bub10000.dat");
	
	if(fout.fail())
	{
		cout << "File not found. Creating 'bub10000.dat'...";
		fout.open("bub10000.dat", ios::app);
		cout << "done." << endl;
	}
	
	//Print large passes
	fout << large_passes << endl;

	
	//Print list
	for(i = 0; i < large_val; i++)
	{
		fout << large_bub_list[i].key << " ";
		fout << large_bub_list[i].number << endl;
	}
	
	fout.close();
		
		
	//Run insertion sort
	small_passes = 0;
	med_passes = 0;
	large_passes = 0;
	
	insert_sort(small_ins_list, small_val - 1, small_passes);
	
	cout << "small insertion done." << endl;
	
	insert_sort(med_ins_list, med_val - 1, med_passes);
	
	cout << "med insertion done." << endl;
	
	insert_sort(large_ins_list, large_val - 1, large_passes);
	
	cout << "large insertion done." << endl;
	
	//Print insertion results
	fout.open("sins500.dat");
	
	if(fout.fail())
	{
		cout << "File not found. Creating 'sins500.dat'...";
		fout.open("sins5000.dat", ios::app);
		cout << "done." << endl;
	}
	
	//Print small passes
	fout << small_passes << endl;
	
	//Print list
	for(i = 0; i < small_val; i++)
	{
		fout << small_ins_list[i].key << " ";
		fout << small_ins_list[i].number << endl;
	}
	
	fout.close();
	
	
	fout.open("sins5000.dat");
	
	if(fout.fail())
	{
		cout << "File not found. Creating 'sins5000.dat'...";
		fout.open("sins5000.dat", ios::app);
		cout << "done." << endl;
	}
	
	//Print medium passes
	fout << med_passes << endl;
	
	//Print list
	for(i = 0; i < med_val; i++)
	{
		fout << med_ins_list[i].key << " ";
		fout << med_ins_list[i].number << endl;
	}
	fout.close();
	
	
	fout.open("sins10000.dat");
	
	if(fout.fail())
	{
		cout << "File not found. Creating 'sins10000.dat'...";
		fout.open("sins10000.dat", ios::app);
		cout << "done." << endl;
	}	
	
	fout << large_passes << endl;
	for(i = 0; i < large_val; i++)
	{
		fout << large_ins_list[i].key << " ";
		fout << large_ins_list[i].number << endl;
	}
	
	fout.close();
	
	
	//Run selection sort
	small_passes = 0;
	med_passes = 0;
	large_passes = 0;
	
	sel_sort(small_sel_list, small_val - 1, small_passes);
	
	cout << "small selection done." << endl;
	
	sel_sort(med_sel_list, med_val - 1, med_passes);
	
	cout << "med selection done." << endl;
	
	sel_sort(large_sel_list, large_val - 1, large_passes);
	
	cout << "large selection done." << endl;
	
	//Print selection results
	fout.open("sels500.dat");
	
	if(fout.fail())
	{
		cout << "File could not be found. Creating 'sels500.dat'...";
		fout.open("sels500.dat", ios::app);
		cout << "done." << endl;
	}
	
	//Print small passes
	fout << small_passes << endl;
	
	//Print list
	for(i = 0; i < small_val; i++)
	{
		fout << small_sel_list[i].key << " ";
		fout << small_sel_list[i].number << endl;
	}
	
	fout.close();
	
	
	fout.open("sels5000.dat");
	
	if(fout.fail())
	{
		cout << "File could not be found. Creating 'sels5000.dat'...";
		fout.open("sels5000.dat", ios::app);
		cout << "done." << endl;
	}
	
	//Print medium passes
	fout << med_passes << endl;
	
	//Print list
	for(i = 0; i < med_val; i++)
	{
		fout << med_sel_list[i].key << " ";
		fout << med_sel_list[i].number << endl;
	}
	
	fout.close();
	
	
	fout.open("sels10000.dat");
	
	if(fout.fail())
	{
		cout << "File could not be found. Creating 'sels10000.dat'...";
		fout.open("sels10000.dat", ios::app);
		cout << "done." << endl;
	}
	
	//Print large passes
	fout << large_passes << endl;
	
	//Print list
	for(i = 0; i < large_val; i++)
	{
		fout << large_sel_list[i].key << " ";
		fout << large_sel_list[i].number << endl;
	}
		
	fout.close();
		
		
	//Run quick sort
	small_passes = 0;
	med_passes = 0;
	large_passes = 0;
	
	quicksort(small_quick_list, 0, small_val - 1, small_passes);
	
	cout << "small quick done." << endl;
	
	quicksort(med_quick_list, 0, med_val - 1, med_passes);
	
	cout << "med quick done." << endl;
	
	quicksort(large_quick_list, 0, large_val - 1, large_passes);
	
	cout << "large quick done." << endl;
	
	
	//Print quick sort results
	fout.open("quick500.dat");
	
	if(fout.fail())
	{
		cout << "File could not be found. Creating 'quick500.dat'...";
		fout.open("quick500.dat", ios::app);
		cout << "done." << endl;
	}
	
	//Print small passes
	fout << small_passes << endl;
	
	for(i = 0; i < small_val; i++)
	{
		fout << small_quick_list[i].key << " ";
		fout << small_quick_list[i].number << endl;
	}
	
	fout.close();
	
	
	fout.open("quick5000.dat");
	
	if(fout.fail())
	{
		cout << "File could not be found. Creating 'quick5000.dat'...";
		fout.open("quick5000.dat", ios::app);
		cout << "done." << endl;
	}
	
	//Print medium passes
	fout << med_passes << endl;
	
	//Print list
	for(i = 0; i < med_val; i++)
	{
		fout << med_quick_list[i].key << " ";
		fout << med_quick_list[i].number << endl;
	}
	
	fout.close();
	
	
	fout.open("quick10000.dat");
	
	if(fout.fail())
	{
		cout << "File could not be found. Creating 'quick10000.dat'...";
		fout.open("quick10000.dat", ios::app);
		cout << "done." << endl;
	}
	
	//Print large passes
	fout << large_passes << endl;
	
	//Print list
	for(i = 0; i < large_val; i++)
	{
		fout << large_quick_list[i].key << " ";
		fout << large_quick_list[i].number << endl;
	}
		
	fout.close();
	
	
	//Print BST in-order
	small_passes = 0;
	med_passes = 0;
	large_passes = 0;
	
	small_tree.count_root(small_passes);
	
	med_tree.count_root(med_passes);
	
	large_tree.count_root(large_passes);

	small_tree.print_500_root(small_passes);
	
	med_tree.print_5000_root(med_passes);
	
	large_tree.print_10000_root(large_passes);
	
	return 0;
}

/*
-- List 500--
Big O calculated bubble sort = 250000
Big O calculated selection sort = 250000
Big O calculated insertion sort = 250000
Big O calculated quick sort = 25000

--List 5000--
Big O calculated bubble sort = 25000000 
Big O calculated selection sort = 25000000
Big O calculated insertion sort = 25000000
Big O calculated quick sort = 25000000

--List 10000--
Big O calculated bubble sort = 100000000
Big O calculated selection sort = 10000000
Big O calculated insertion sort = 10000000
Big O calculated quick sort = 100000000
*/
