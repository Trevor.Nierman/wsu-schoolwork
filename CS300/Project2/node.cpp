//Trevor Nierman
//R957F692
//Project 2

/*******************************************************************************
This file defines the functions for the class "Node" used in a binary search 
tree for project 2.
*******************************************************************************/
#include<iostream>
#include<string>
#include "node.hpp"
#include "data_struct.hpp"
using namespace std;

//CONSTRUCTOR
Node::Node(int k, double num)
{
	data.key = k;
	data.number = num;
	left = NULL;
	right = NULL;
}

//Accessor Functions
data_struct Node::get_data(void)
{
	return data;
}

int Node::get_key(void)
{
	return data.key;
}

double Node::get_number(void)
{
	return data.number;
}

Node* Node::get_left(void)
{
	return left;
}

Node* Node::get_right(void)
{
	return right;
}

//Mutator Functions
void Node::set_data(data_struct dat)
{
	data = dat;
}

void Node::set_key(int k)
{
	data.key = k;
}

void Node::set_number(double num)
{
	data.number = num;
}

void Node::set_left(Node* l)
{
	left = l;
}

void Node::set_right(Node* r)
{
	right = r;
}

//Class Functions
void Node::print_node(void)
{
	cout << data.key << " ";
	cout << data.number << endl;
}
