//Trevor Nierman
//R957F692
//Project 2

/*******************************************************************************
This file descibes the structure data struct, to be used in project 2.

		Structure
			-- data_struct --
				Description: A simple structure that holds and integer, key, and
				and double, number. 
				
				Data: key - A randomly generated number that is used to sort.
						
					  number - Another piece of data so we had to make a 
					  structure.
					  
*******************************************************************************/
#ifndef __DATA_STRUCT__
#define __DATA_STRUCT__
#include<iostream>
using namespace std;

struct data_struct
{
	int key;
	double number;
};

#endif
