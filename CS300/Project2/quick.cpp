//Jacob Hoggatt
//w643w733
/* Psuedo code
	Function: print
			Used to print the data into the data files.
			* This was removed because print is in the main function.
			
			It uses switch statements to check if right is 100, 5000, or 
				10000. The name of the data files corresponds to the size
				of right (quickxxx.dat). It then outputs the data into the
				file.
			
			switch right	
			case 100
				print the data int the array to file quick100.dat
			case 5000
				print the data int the array to file quick5000.dat
			case 10000
				print the data int the array to file quick10000.dat
			
			Input Parameters: count - the number of passes the sort uses to completely.
									sort the list.
							  right - the size of the array to be printed.
							  array - the array that the data is stored in.
			Output Parameters: none
			
	Function: partition 
			Used to prepare the list for the quicksort.
			(I ended up separating this from the quicksort function due to
				the confusion it was causing.)
				
				
			set pivot to right
			
			loop right < left
				loop left_key < pivot
					increment left
				endloop
				
				loop right_key > pivot
					decrement right
				endloop
				
				if right_key = left_key
					increment right
				elseif left < right
					exchange right_key, left_key
			endloop
			
			Input Parameters: 	array - the array the data is stored in.
								left - the minimum size of the data.
								right - the size of the array.
			Output Parameters: 	right - the value of right after the function
										completes.
	
	Function: insertion_sort
			Sorts an array using the insertion method.
			
			
			initialize curr, hold, and walker.
			set curr to 1
			loop until last element is sorted
				move curr to hold
				set walker to curr - 1
				loop walker > 0 and hold_key < walker_key
					move walker element right one element
					decrement walker
				endloop
				move hold to walker + 1 element
				increment curr
			endloop
			
			Input Parameters: array - the array the data is stored in.
							  last - the size of the array.
							  
			Output Parameters: None
	Function: quicksort
			Used to sort the data within the array using the quicksort method.
			
			if right - left > min size(I researched this and came up with 9)
				if left <= sort_right
					quicksort(array, left, right) 
				if sort_left < right
					quicksort(array, sort_left, right)
				else
					insertion_sort(array, left right)
					
			
			Input Parameters: array - the array the data is stored in.
							  left - the minimum size of the data.
							  right - the size of the array.
			Output Parameters: none
*/


#include <iostream>
#include <cstdlib>
#include <time.h> 
#include "data_struct.hpp"

using namespace std;

/*
//The print fuction
void print(int array[], int right, int count)
{
	int passes = count;
	ofstream out_file;
	if(right == 99)
	{
		out_file.open("quick100.dat");
		
		out_file << "passes: " << passes << endl;
		
		for(int i = 0; i <= right; i++)
		{
			out_file << array[i] << endl;
		}
	}
	else if(right == 4999)
	{
		out_file.open("quick100.dat");
		
		out_file << "passes: " << passes << endl;
		
		for(int i = 0; i <= right; i++)
		{
			out_file << array[i] << endl;
		}
	}
	else if(right == 9999)
	{
		out_file.open("quick100.dat");
		
		out_file << "passes: " << passes << endl;
		
		for(int i = 0; i <= right; i++)
		{
			out_file << array[i] << endl;
		}
	}	
		
}
*/

//The Insertion sort function
int insertion_sort (data_struct array[],int first, int last, int& count)
{
	
	int curr; 
	
		
	for (int i = first; i < last + 1; i++)
	{
		curr = i;
		
		while (curr > 0 && array[curr].key < array[curr-1].key)
		{
			swap(array[curr], array[curr -1]);
			curr--;
			count ++;
		}
	}
		return count;
}

// The partition function
int partition(data_struct array[], int left, int right, int& count) 
{
	
	
   int pivot, sort_left = left, sort_right;
   bool stop = true;
   pivot = array[left].key;
   sort_right = right + 1;
		
   while(stop == true)
   {
	    //Increments sort_left if it is less than pivot and less than right.
   		do
		{
			count ++;
			sort_left ++;
		} while(array[sort_left].key <= pivot && sort_left <= right);
	
		//Decrements sort_right if it is greater than the pivot.
		do
		{
			count ++;
			sort_right --;
		} while(array[sort_right].key > pivot);
		
		//When the transversal causes sort_left to pass sort_right the
		//loop is stoped.
  	 	if(sort_left >= sort_right)
  	 	{
			stop = false;
		}
		else
		{	
			swap(array[sort_left], array[sort_right]);
			
		}
 	  	
   }
   swap(array[left], array[sort_right]);
   return sort_right;
}

//The quick sort function
void quicksort(data_struct array[], int left, int right, int& count)
{
   
   int sort;
   count ++;
   
   // Use insertion sort once the gap between right and left is less than 9
   if((right - left) < 16)
   {
   		insertion_sort(array, left, right, count);
   }
   else if(left < right) 
   {
   		
   		
   	    //Partitions the list then calls quicksort recursivly.
        sort = partition(array, left, right, count);
        quicksort(array, left, sort - 1, count);
        quicksort(array, sort + 1, right, count);
   }
   
	
}



