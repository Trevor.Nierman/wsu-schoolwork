//Trevor Nierman
//R957F692
//Project 2

/*******************************************************************************
This file describes the function insert_sort function, which utilizes the 
insertion sort algorithm to sort arrays.

			Functions:
			--Exchange--
				Description: Used to swap two array elements.
			
				Inputs:	data_struct list - array of the elements that are 
						being exchanged.
				
						int first - Index of first element in array "list" to
						be swapped.
						
						int second - Index of second element in array "list"
						to be swapped.
						
						int count - Tracks the number of times the algorithm
						exchanges indices.  
						
				Outputs: data_struct list - array of the elements that have been
						 exchanged. 
						 
						 int count - Updated count of times indices have been 
						 swapped.
						 
						 
						exchange(data_struct list, int first, int second, 
								 int count)
						
							int temp
							temp = first
							first = second
							second = temp
							 
							count++
							
			--Straight Insertion Sort--
				Description: Used to sort an unordered array using only the
				array and the last element. 
				
				Inputs: data_struct list - array of unordered elements to be
						sorted.
						
						int last - index of the last element in the array. 
						
						int &passes - reference parameter of the number of 
						passes required to sort the array. Used to calculate
						efficiency.
						
				Outputs: data_struct list - sorted array of structures. 
						
						 int &passes - Returned as a reference parameter. Used 
						 to calculate the efficiency of the sorting method. 
						 
						 
						 Insert_sort(data_struct list, int last, int &passes)
						 
						 int curr = 1
						 int hold
						 
						 loop (until last element is sorted)
							hold = curr
							walker = curr - 1
							loop(walker >= 0 and hold's key < walker's key)
								exchange(walker, walker + 1)
								walker--
							end loop
							exchange(hold, walker + 1)
							curr++
						end loop

*******************************************************************************/
#ifndef __INSERTION__
#define __INSERTION__
#include<iostream> 
#include "data_struct.hpp"
using namespace std;

void insert_sort(data_struct list[], int last, int &passes)
{
	int hold, walker, curr = 1;
	
	while(curr <= last)
	{
		hold = curr;
		walker = curr - 1;
		
		while(walker >= 0 && list[hold].key < list[walker].key)
		{
			swap(list[walker], list[walker + 1]);
			walker--;
			
			passes++;
		}
		
		swap(list[hold], list[walker + 1]);
		curr++;
	}
}

#endif
