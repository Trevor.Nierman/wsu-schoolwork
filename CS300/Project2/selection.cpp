//Jacob Hoggatt
//w643w733
/* Psuedo code
	Function: print
			Used to print the data into the data files.
			
			It uses switch statements to check if right is 100, 5000, or 
				10000. The name of the data files corresponds to the size
				of right (selsxxx.dat). It then outputs the data into the
				file.
			
			switch right	
			case 100
				print the data int the array to file sels100.dat
			case 5000
				print the data int the array to file sels5000.dat
			case 10000
				print the data int the array to file sels10000.dat
			
			Input Parameters: count - the number of passes the sort uses to completely.
									sort the list.
							  max - the size of the array to be printed.
							  array - the array that the data is stored in.
			Output Parameters: none
	
	
	Function: sel_sort
			Sorts an array using the selection method.
			
			initialize smallest, walker and curr to 0
			loop until last element is sorted
				smallest = curr
				walker = curr + 1
				loop walker <= last
					if walker_key < smallest_key
						smallest = walker
						increment walker
				endloop
				exchange curr and smallest
				increment curr
			endloop
			
			Input Parameters: array - the array the data is stored in.
							  last - the size of the array.
							  
			Output Parameters: None
	
*/
#ifndef __SELECTION__
#define __SELECTION__
#include <iostream>
#include <time.h> 
#include <cstdlib>
#include <fstream>
#include "data_struct.hpp"
using namespace std;


/*
//The print fuction
void selection_print(data_struct array[], int max, int count)
{
	int passes = count;
	ofstream out_file;
	if(max == 99)
	{
		out_file.open("sels100.dat");
		
		out_file << "passes: " << passes << endl;
		
		for(int i = 0; i <= max; i++)
		{
			out_file << array[i] << endl;
		}
	}
	else if(max == 4999)
	{
		out_file.open("sels100.dat");
		
		out_file << "passes: " << passes << endl;
		
		for(int i = 0; i <= max; i++)
		{
			out_file << array[i] << endl;
		}
	}
	else if(max == 9999)
	{
		out_file.open("sels100.dat");
		
		out_file << "passes: " << passes << endl;
		
		for(int i = 0; i <= max; i++)
		{
			out_file << array[i] << endl;
		}
	}	
		
}


*/
//The selection sort fuction
void sel_sort(data_struct array[], int max, int& count)
{
	int  min, walker;
	
	for(int i = 0; i <= max; i++)
	{
		count ++;
		min = i;
		for(walker = i + 1; walker <= max; walker ++)
		{
			count ++;
			if(array[walker].key < array[min].key)
			{
				min = walker;
			}	
		}
		swap(array[i], array[min]);
	}
}
/*
int main()
{
	srand(time(0));
	int max = 99, count, array[99];
    for(int i = 0; i <= 99; i++)
    {
		array[i] = rand();
	}
	sel_sort(array, max, count);
    print(array, max, count);
    return 0;
}
*/

#endif
