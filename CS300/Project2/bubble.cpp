//Trevor Nierman
//R957F692
//Project 2

/*******************************************************************************
This file describes the algorithm used for the bubble sort method which is 
required for project 2.

			Functions: 
			--Bubble sort--
			Description: Used to sort an unordered array using only the array
			and last element.

			inputs: data_struct list - an unordered array of structures that 
					is to be sorted.
					
					int last - The index of the last element in the array
					"list",
					(should be equal to size of array - 1).
					
					int count - number of times the algorithm exchanged indices.
					
					
			outputs: data_struct list - sorted array of structures
					 
					 int &passes - tracks the number of times the algorithm
					 exchanged indices. Used to calculate efficiency.
					 Returned as a reference parameter.

					 
					 bubble_sort(data_struct list, int last)
					 //last = number of elements - 1
					 
					 curr = 0
					 sort = false
					 
					 loop(curr <= last and sorted == false)
						walker = last
						sorted = true
						loop(walker > curr)
							if(walker key < walker-1 key)
								sorted = false
								exchange(list, walker, walker-1)
							walker--
						end loop
						curr++
					 end loop
					 
			--Exchange-- 
				Description: Used to swap two values in an array
				
				Inputs:	data_struct list - array of the elements that are 
						being exchanged.
				
						int first - Index of first element in array "list" to
						be swapped.
						
						int second - Index of second element in array "list"
						to be swapped.
						
						int count - Tracks the number of times the algorithm
						exchanges indices.  
						
				Outputs: data_struct list - array of the elements that have been
						 exchanged. 
						 
						 int count - Updated count of times indices have been 
						 swapped.
						 
						 
						exchange(data_struct list, int first, int second, 
								 int count)
						
							int temp
							temp = first
							first = second
							second = temp
							 
							count++

*******************************************************************************/
#ifndef __BUBBLE_SORT__
#define __BUBBLE_SORT__
#include<iostream>
#include "data_struct.hpp"
using namespace std;

void bubble_sort(data_struct list[], int last, int &passes)
{
	//Sorting variables
	int walker, curr = 0;
	bool sorted = false;
	
	while(curr <= last && sorted == false)
	{
		walker = last;
		sorted = true;
		
		while(walker > curr)
		{
			if(list[walker].key < list[walker - 1].key)
			{
				sorted = false;
				swap(list[walker], list[walker - 1]);
			}
			
			//Decrement walker
			walker--;
			
			passes++;
		}
		
		curr++;
	}
}

#endif
