//Trevor Nierman
//R957F692
//Project 2

/*******************************************************************************
This file defines the functions for the class Binary_Tree used in Project 2.
*******************************************************************************/
#include<iostream>
#include<string>
#include<fstream>
#include<cstdlib>
#include "bst.hpp"
#include "node.hpp"
#include "data_struct.hpp"
using namespace std;

//CONSTRUCTOR
Binary_Tree::Binary_Tree()
{
	root = NULL;
	count = 0;
}

//Accessor Functions
Node* Binary_Tree::get_root(void)
{
	return root;
}

int Binary_Tree::get_count(void)
{
	return count;
}

//Mutator Functions
void Binary_Tree::set_root(Node* r)
{
	root = r;
}

void Binary_Tree::set_count(int c)
{
	count = c;
}

//Class Functions
bool Binary_Tree::is_empty(void)
{
	if(root == NULL)
	{
		return true;
	}
	
	else
	{
		return false;
	}

}

Node* Binary_Tree::search(Node* curr, int key)
{
	//Node could not be found
	if(curr == NULL)
	{
		return NULL;
	}
	
	//If key is less than curr, search left
	else if(curr->get_key() > key)
	{	
		return search(curr->get_left(), key);
	}	
	
	//If key is greater than curr, search right
	else if(curr->get_key() < key) 
	{
		return search(curr->get_right(), key);
	}
	
	//Root found
	else
	{
		return curr;
	}
}

Node* Binary_Tree::find_smallest(Node* curr)
{
	if(curr->get_left() == NULL)
	{
		return curr;
	}
	
	else
	{
		return find_smallest(curr->get_left());
	}
}

Node* Binary_Tree::find_largest(Node* curr)
{
	//Traverse as far right as possible
	if(curr != NULL)
	{
		return find_largest(curr->get_right());
	}
	
	//Returns curr once right-most node has been reached
	else
	{
		return curr;
	}
}

bool Binary_Tree::add_node(int k, double num)
{
	Node *temp = new Node(k, num);
	count++;		

	root = insert_node(root, temp);
	return true;
}

Node* Binary_Tree::insert_node(Node* curr, Node* n)
{
	//If n is first node in tree, then root is set to n
	if(curr == NULL)
	{
		return n;	
	}
	
	//Go left
	else if(n->get_key() < curr->get_key())
	{
		if(curr->get_left() == NULL)
		{
			curr->set_left(n);
			
			return curr->get_left();
		}
		
		else
		{
			return insert_node(curr->get_left(), n);
		}
	}
	
	//Go right
	else
	{
		if(curr->get_left() == NULL)
		{
			curr->set_right(n);
			
			return curr->get_right();
		}
		
		else
		{
			return insert_node(curr->get_right(), n);
		}
		
	}
}
	
bool Binary_Tree::delete_root(int key)
{
	if(is_empty())
	{
		return false;
	}
	
	else
	{
		return delete_node(root, key);
	}
}

bool Binary_Tree::delete_node(Node* curr, int key)
{
	//If curr's phone is greater than key, continue left
	if(curr->get_key() > key)
	{ 
		return delete_node(curr->get_left(), key);
	}
	
	//If curr's phone is less than key, continue right
	else if(key > curr->get_key())
	{
		return delete_node(curr->get_right(), key);
	}

	//Key found
	else
	{

		Node *temp;
		
		//If node to be deleted only has a right subtree
		if(curr->get_left() == NULL && curr->get_right() != NULL)
		{
			//Exchange data between curr and right subtree
			curr->set_data(curr->get_right()->get_data());
			curr->set_left(curr->get_right()->get_left());
			
			//Right subtree's right pointer is temporarily saved
			temp = curr->get_right()->get_right();
			
			//Curr's right pointer is deleted
			delete curr->get_right();
			count--;
			
			//Curr's right subtree is set to temp
			curr->set_right(temp);
			
			return true;
		}
		
		//If node to be deleted only has a left subtree
		else if(curr->get_right() == NULL && curr->get_left() != NULL)
		{
			//Exchange data between curr and left subtree
			curr->set_data(curr->get_left()->get_data());
			curr->set_right(curr->get_left()->get_right());
			
			//Left subtree's left pointer is temporarily saved
			temp = curr->get_left()->get_left();
			
			//Curr's left pointer is deleted
			delete curr->get_left();
			count--;
			
			//Curr's left subtree is set to temp
			curr->set_left(temp);
			
			return true;
		}
		
		else if(curr->get_right() == NULL && curr->get_left() == NULL)
		{
			delete curr;
			count--;
			
			return true;
		}
		
		
		//Otherwise, curr is replaced with the smallest node in its right 
		//subtree, then is deleted.
		else
		{
			Node* new_curr = find_smallest(curr->get_right());
			
			//Exchange data between curr and new_curr
			curr->set_data(new_curr->get_data());
			
			//new_curr may have a right subtree: it is attached to new_curr's 
			//parent's left subtree
			if(new_curr->get_right() != NULL)
			{
				//Data exchanged between new_curr and its right subtree
				new_curr->set_data(new_curr->get_right()->get_data());
				new_curr->set_left(new_curr->get_right()->get_left());
				
				temp = new_curr->get_right()->get_right();
				
				delete new_curr->get_right();
				count--;
				
				new_curr->set_right(temp);
				
				return true;
			}
			
			else
			{
				//If curr is being replaced by its right subtree, right subtree is set to NULL
				if(new_curr == curr->get_right())
				{
					curr->set_right(NULL);
				}
				
				delete new_curr;
				count--;
				
				return true;
			}
		}
	}
}

//Root processed first		
void Binary_Tree::print_pre_root(void)
{
	if(root != NULL)
	{
		print_preorder(root);
	}
}
		
void Binary_Tree::print_preorder(Node* curr)
{
	if(curr != NULL)
	{
		curr->print_node();
		
		print_preorder(curr->get_left());
		print_preorder(curr->get_right());
	}
}

//Root processed second
void Binary_Tree::print_in_root(void)
{
	if(root != NULL)
	{
		print_inorder(root);
	}
}

void Binary_Tree::print_inorder(Node* curr)
{
	if(curr != NULL)
	{
		print_inorder(curr->get_left());

		curr->print_node();

		print_inorder(curr->get_right());
	}
}

//Root processed last
void Binary_Tree::print_post_root(void)
{
	if(root != NULL)
	{
		print_postorder(root);
	}
}

void Binary_Tree::print_postorder(Node* curr)
{
	if(curr != NULL)
	{
		print_postorder(curr->get_left());
		print_postorder(curr->get_right());
		
		curr->print_node();
	}
}

//Determines count for printing functions
void Binary_Tree::count_root(int &passes)
{
	if(root != NULL)
	{
		passes++; 
		count_inorder(root->get_left(), count);
		count_inorder(root->get_right(), count);
	}
}

void Binary_Tree::count_inorder(Node* curr, int &passes)
{
	if(curr != NULL)
	{
		passes++;
		count_inorder(curr->get_left(), count);	
		
		passes++;
		count_inorder(curr->get_right(), count);
	}
}
	
bool Binary_Tree::print_500_root(int &passes)
{
	if(root != NULL)
	{
		ofstream fout;
		
		fout.open("bst500.dat", ios::out | ios::app);
		
		if(fout.fail())
		{
			cout << "\t 'bst500.dat could not be found. Creating file...";
			
			fout.open("bst500.dat", ios::app);
			
			cout << "done." << endl;
			
			return false;
		}
		
		else
		{
			fout << passes << endl;
			
			fout << root->get_key() << " ";
			fout << root->get_number() << endl;
			
			print_500(root->get_left(), fout);
			print_500(root->get_right(), fout);
			
			fout.close();
			
			return true;
		}
	}
	
	return false;
}

bool Binary_Tree::print_500(Node* curr, ofstream &fout)
{
	if(curr != NULL)
	{
		print_nodes(curr->get_left(), fout);
		print_nodes(curr->get_right(), fout);
			
		return true;
		
	}
	
	else
		return false;
}

bool Binary_Tree::print_5000_root(int &passes)
{
	if(root != NULL)
	{
		ofstream fout;
		
		fout.open("bst5000.dat", ios::out | ios::app);
		
		if(fout.fail())
		{
			cout << "\t 'bst5000.dat could not be found. Creating file...";
			
			fout.open("bst5000.dat", ios::app);
			
			cout << "done." << endl;
			
			return false;
		}
		
		else
		{
			fout << passes << endl;
			
			fout << root->get_key() << " ";
			fout << root->get_number() << endl;
			
			print_5000(root->get_left(), fout);
			print_5000(root->get_right(), fout);
			
			fout.close();
			
			return true;
		}
	}
	
	return false;
}


bool Binary_Tree::print_5000(Node* curr, ofstream &fout)
{
	if(curr != NULL)
	{
			
		fout << curr->get_key() << " ";
		fout << curr->get_number() << endl;
			
		print_5000(curr->get_left(), fout);
		print_5000(curr->get_right(), fout);
			
		return true;
		
	}
	else
		return false;
}

bool Binary_Tree::print_10000_root(int &passes)
{

	ofstream fout;
	
	if(root != NULL)
	{
		
		fout.open("bst10000.dat", ios::out | ios::app);
		
		if(fout.fail())
		{
			cout << "\t 'bst10000.dat could not be found. Creating file...";
			
			fout.open("bst10000.dat", ios::app);
			
			cout << "done." << endl;
			
			return false;
		}
		
		else
		{
			fout << passes << endl;
			
			fout << root->get_key() << " ";
			fout << root->get_number() << endl;
			
			print_10000(root->get_left(), fout);
			print_10000(root->get_right(), fout);
			
			fout.close();
			
			return true;
		}
	}
	
	fout.close();
	return false;
}


bool Binary_Tree::print_10000(Node* curr, ofstream &fout)
{
	if(curr != NULL)
	{		
		fout << curr->get_key() << " ";
		fout << curr->get_number() << endl;
			
		print_10000(curr->get_left(), fout);
		print_10000(curr->get_right(), fout);
			
		return true;
		
	}
	else
		return false;
}

bool Binary_Tree::print_nodes(Node* curr, ofstream &fout)
{
	if(curr != NULL)
	{
		fout << curr->get_key() << " ";
		fout << curr->get_number() << endl;
		
		print_nodes(curr->get_left(), fout);
		print_nodes(curr->get_right(), fout);
		
		return true;	
	}
	
	return false;
}
