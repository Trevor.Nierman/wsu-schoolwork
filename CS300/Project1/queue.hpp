//Trevor Nierman
//R957F692
//Project 1

/*******************************************************************************
This file defines the class "queue" to be used in main.cpp for project 1. This 
class will be used to store customers waiting to check out for the simulation.

Each queue will only be able to store a maximum of six customers.

When a customer is finished checking out, they will be dequeued and sent to the
the cashier to be checked out, and when a new one arrives, they will be enqueued
if there is room in the queue.


		Class: Queue
		Data:	int count - tracks number of waiting customers in each queue.
				node *front - tracks first waiting customer in queue.
				node *rear - tracks last waiting customer in queue.
				
		Functions: 
			Accessor Functions
				get_count - returns count as an int
				get_front - returns the front customer as a node pointer, but 
				does not remove the node from the queue.
				get_rear - returns the rear customer as a node pointer
				
			Mutator Functions
				set_count(int c) - sets count to c. Returns nothing.
				add_count(void) - adds one to count. Returns nothing.
				sub_count(void) - subtracts one from count. Returns nothing. 
				set_front(node *f) - sets front to f. Returns nothing.
				set_rear(node *r) - sets rear to r. Returns nothing.
			
			Class Functions
				bool enqueue(n, a, s) - Creates a new node and adds it to the 
				queue.
						Node *temp_ptr = new Node(n, a, s)
						
						if(queue is empty)
							front = temp_ptr
							rear = temp_ptr
							count ++
						else
							temp_ptr->set_next(rear)
							rear = temp_ptr
							count ++
				
				dequeue(queue q) - Removes and returns a node from the front of 
				the queue. Does NOT delete the node, but only removes it from
				the queue.
						Node *temp_ptr
						
						if(queue is empty) 
							return false
						else
							if(node is last in queue)
								temp_ptr = front
								front = rear = NULL
								count--
								return temp
							else
								temp_ptr = front
								front = temp_ptr->get_next()
								count--
								return temp_ptr
								
				bool add_node(Node *n) - Adds an EXISTING node to the rear of 
				the queue. Returns 1 if successful, 0 if not.
						if(queue is empty)
							front = rear = n
							count = 1
							return 1
						else
							n->set_next(rear)
							rear = n
							count++
							return 
				

*******************************************************************************/
#ifndef __QUEUE__
#define __QUEUE__
#include "node.hpp"
#include "customer.hpp"
#include<iostream>
#include<string>
using namespace std;

class Queue
{
	//Total number of customers in queue
	int count;
	
	//Tracks front of queue
	Node *front;
	
	//Tracks rear of queue
	Node *rear;
	
	public:
	//CONSTRUCTORS
	Queue();
	
	//Accessors
	int get_count(void);
	Node *get_front(void);
	Node *get_rear(void);
	
	//Mutators
	void set_count(int c);
	void add_count(void);
	void sub_count(void);
	void set_front(Node *f);
	void set_rear(Node *r);
	
	//Class functions
	bool is_empty(void);
	bool enqueue(int num, int arr, int ser);
	void dequeue(void);
	void add_node(Node *n);
	
};

#endif
