//Trevor Nierman
//R957F692
//Project 1

/*******************************************************************************
This file describes the functions for the "customer" class to be used in project
1. 
*******************************************************************************/
#include "customer.hpp"
#include<iostream>
#include<string>
using namespace std;

//Constructors
Customer::Customer()
{
	number = 0;
	arrival_time = 0;
	service_time = 0;
	wait_time = 0;
	mark = "NULL";
}

Customer::Customer(int n, int a, int s)
{
	number = n;
	arrival_time = a;
	service_time = s;
	wait_time = 0;
	mark = "NULL";
}

//Accessor Functions
int Customer::get_number(void)
{
	return number;
}

int Customer::get_arr_time(void)
{
	return arrival_time;
}

int Customer::get_serv_time(void)
{
	return service_time;
}

int Customer::get_wait_time(void)
{
	return wait_time;
}

string Customer::get_mark(void)
{
	return mark;
}

//Mutator Functions
void Customer::set_number(int n)
{
	number = n;
}

void Customer::set_arr_time(int a)
{
	arrival_time = a;
}

void Customer::set_sev_time(int s)
{
	service_time = s;
}

void Customer::calc_wait(int w)
{
	wait_time = w - arrival_time;
}

void Customer::set_mark(string m)
{
	mark = m;
}
