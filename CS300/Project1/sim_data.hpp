//Trevor Nierman
//R957F692
//Project 1

/*******************************************************************************
This file defines the class sim_data, which is used in project 1 to store the 
simulation data. It contains the number of served customers, the number of 
rejected customers, the average time customers waited in a queue before being 
checked out, and the total wait time of all customers throughout the day. 

		Data: int cust_served - The total number of customers served throughout
		the day.
			  int cust_rejected - The total number of customers served 
		throughout the day.
			  int total_wait - The total wait time of all customers.
			  int avg_wait - The average wait time of all customers.
			  Node *curr - The current node being processed by the simulation.
		
		Functions:
			Accessor Functions:
				int get_served - Returns the total number of customers served.
				
				int get_rejected - Returns the total number of rejected 
				customers.
				
				int get_tot_wait - Returns the total wait time of all customers.
				
				int get_avg_wait - Returns the average wait time of all 
				customers.
				
				Node *get_curr - Returns the pointer curr. 
				
			Mutator Functions:
				set_served(int s) - Sets cust_served to s. Returns nothing.
				add_served(void) - Adds one to cust_served. Returns nothing. 
				set_rejected(int r) - Sets cust_rejected to r. Returns nothing.
				add_rejected(void) - Adds one to cust_rejected. Returns nothing.
				set_tot_wait(int t) - Sets total_wait to t. Returns nothing.
				sum_tot_wait(int t) - Adds t to total_wait. Returns nothing.
				set_curr(Node *c) - Sets curr to c. Returns nothing.  
				
			Class Functions
				calc_avg_wait(void) - calculates the average wait time for a 
				customer by taking total_wait and dividing it by cust_served.
				Returns nothing. 
				
				update_data(Node *n) - Calls functions add_served,
				sum_tot_wait, and calc_avg_wait to update simulation data
				when a customer is sucessfully checked out. If customer was
				rejected, updates number of rejects. The node that is passed
				in is the node that was just checked out by a cashier.
				Returns true (1), if data is successfully updated, or false
				(0), if not.
				
				bool delete_node(void) - deletes the Node pointed to by curr. 
				
							delete curr
							curr = NULL
				
				

*******************************************************************************/
#ifndef __SIM_DATA__
#define __SIM_DATA__
#include "node.hpp"
#include<iostream>
#include<string>
using namespace std;

class Sim_data
{
	//Total number of served customers
	int cust_served;
	
	//Total number of rejected customers
	int cust_rejected;
	
	//Total time customers were waiting
	int total_wait;
	
	//Average wait time per customer
	int avg_wait;
	
	//Current node being proccessed
	Node *curr;
	
	public:
	//Constructor
	Sim_data();
	
	//Accessor Functions
	int get_served(void);
	int get_rejected(void);
	int get_tot_wait(void);
	int get_avg_wait(void);
	Node *get_curr(void);
	
	//Mutator Functions
	void set_served(int s);
	void add_served(void);
	void set_rejected(int r);
	void add_rejected(void);
	void set_tot_wait(int t);
	void sum_tot_wait(int t);
	void set_avg_wait(int a);
	void set_curr(Node *c);
	
	//Class Functions
	void calc_avg_wait(void);
	void update_data(void);
	void delete_node(void);
};

#endif
