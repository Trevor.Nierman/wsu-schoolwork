//Trevor Nierman
//R957F692	
//Project 1

/*******************************************************************************
This file defines the functions for the class "sim_data" to be used in project 
1. 
*******************************************************************************/
#include "sim_data.hpp"
#include "node.hpp"
#include "customer.hpp"
#include "cashier.hpp"
#include "queue.hpp"
#include<iostream>
#include<string>
using namespace std;

Sim_data::Sim_data()
{
	cust_served = 0;
	cust_rejected = 0;
	total_wait = 0;
	avg_wait = 0;
	curr = NULL;
}

//Accessor Functions
int Sim_data::get_served(void)
{
	return cust_served;
}

int Sim_data::get_rejected(void)
{
	return cust_rejected;
}

int Sim_data::get_tot_wait(void)
{
	return total_wait;
}

int Sim_data::get_avg_wait(void)
{
	return avg_wait;
}

Node* Sim_data::get_curr(void)
{
	return curr;
}

//Mutator Functions
void Sim_data::set_served(int s)
{
	cust_served = s;
}

void Sim_data::add_served(void)
{
	cust_served++;
}

void Sim_data::set_rejected(int r)
{
	cust_rejected = r;
}

void Sim_data::add_rejected(void)
{
	cust_rejected++;
}

void Sim_data::set_tot_wait(int t)
{
	total_wait = t;
}

void Sim_data::sum_tot_wait(int t)
{
	total_wait = total_wait + t;
}

void Sim_data::set_avg_wait(int a)
{
	avg_wait = a;
}

void Sim_data::set_curr(Node *c)
{
	curr = c;
}

//Class Functions
void Sim_data::calc_avg_wait(void)
{
	avg_wait = total_wait / cust_served;
}

void Sim_data::update_data(void)
{
	if(curr != NULL)
	{
		//If wait time is still NULL, then the customer was rejected.
		if(curr->get_cust_mark() == "NULL")
		{
			add_rejected();
		}
	
		//Otherwise, update number served, total wait time, and average wait time
		else
		{
			//Update number served
			add_served();
		
			//Update total wait time
			sum_tot_wait(curr->get_cust_wait());
		
			//Update average wait time
			calc_avg_wait();
		}
	}
}

void Sim_data::delete_node(void)
{
	if(curr != NULL)
	{
		cout << "Delete curr ";
		delete curr;
		curr = NULL;
	}
}
