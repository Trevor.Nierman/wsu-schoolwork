//Trevor Nierman
//R957F692
//Project 1

/*******************************************************************************
This file defines the functions for the class "node" to be used in a queue for 
project 1.
*******************************************************************************/
#include "node.hpp"
#include "customer.hpp"
#include<iostream>
#include<string>
using namespace std;

//Constructor
Node::Node(int n, int a, int s)
{
	//Create customer
	C = Customer(n, a, s);
	
	//Pointer initally set to null
	next = NULL;
}

//Accessor Functions
int Node::get_cust_num(void)
{
	return C.get_number();
}

int Node::get_cust_arr(void)
{
	return C.get_arr_time();
}

int Node::get_cust_serv(void)
{
	return C.get_serv_time();
}

Node* Node::get_next(void)
{
	return next;
}

int Node::get_cust_wait(void)
{
	return C.get_wait_time();
}

string Node::get_cust_mark(void)
{
	return C.get_mark();
}

//Mutator Functions
void Node::set_cust_num(int n)
{
	C.set_number(n);
}

void Node::set_cust_arr(int a)
{
	C.set_arr_time(a);
}

void Node::set_cust_serv(int s)
{
	C.set_sev_time(s);
}

void Node::set_next(Node *n)
{
	next = n;
}

void Node::calc_cust_wait(int w)
{
	C.calc_wait(w);
}

void Node::set_cust_mark(string m)
{
	C.set_mark(m);
}
