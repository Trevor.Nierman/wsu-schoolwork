//Trevor Nierman
//R957F692
//Project 1

/*******************************************************************************
This file defines the "node" class to be used in main.cpp for project 1. 
Contained within is the "customer" class, which holds customer data, and a 
pointer to the next node. This class is designed to be used in a queue. 

		Class: Node
		Data: customer c - contains the customer number, arrival time, and the 
		time that it will take to service the customer. 
			  Node *next - a pointer to the next node in the queue.
			  
		Functions:
			Accessor Functions:
				int get_cust_num - returns the customer number.
				int get_cust_arr - returns the customer arrival time. 
				int get_cust_serve - returns the time it takes to serve the
				customer.
				
				Node *get_next - returns a pointer to the next node in the queue.
				
			Mutator Functions:
				set_cust_num(int n) - sets the customer number to n. Returns
				nothing. 
				
				set_cust_arr(int a) - sets the customer arrival time to a.
				Returns nothing. 
				
				set_cust_serve(int s) - sets the time to serve a customer to s. 
				Returns nothing.
				
				set_next(Node *n) - sets next to n. Returns nothing.
				
				set_cust_wait(int w) - sets wait time. Called when a customer
				begins checking out to calculate their wait time. Returns 
				nothing.
				
			Class Functions:
				print_data - prints all customer data.
				
*******************************************************************************/
#ifndef __NODE__
#define __NODE__
#include "customer.hpp"
#include<iostream>
#include<string>
using namespace std;

class Node
{
	Customer C;
	Node *next;
	
	public:
	//Constructor
	Node(int n, int a, int s);
	
	//Accessor Functions
	int get_cust_num(void);
	int get_cust_arr(void);
	int get_cust_serv(void);
	int get_cust_wait(void);
	string get_cust_mark(void);
	Node *get_next(void);
	
	//Mutator Functions
	void set_cust_num(int n);
	void set_cust_arr(int a);
	void set_cust_serv(int s);
	void calc_cust_wait(int w);
	void set_cust_mark(string m);
	void set_next(Node *n);
	

};

#endif
