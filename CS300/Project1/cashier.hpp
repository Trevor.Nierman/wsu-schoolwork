//Trevor Nierman
//R957F692
//Project 1

/*******************************************************************************
This file defines the class "cashier" to be used in project 1. This class 
recieves "customers" ("nodes") from a queue and processes their transactions. 
Each customer comes pre-equipped with a service time that dictates how long it 
takes a cashier to process the transactions. In addition to processing customers,
the cashier class tracks the number of served customers, as well as the wait 
time for each customer, which is returned when each customer is done being 
processed. 

		Data: 
			Node *curr_cust - Current customer being checked out. Null if
			available to check out a new customer.
			  
		Functions:
			Accessors:
				Node *get_curr_cust(void) - Returns a pointer to the current
				customer being checked out.
				
			Mutators:
				set_curr_cust(Node *c) - Sets "curr_cust" to c. Returns nothing.
				
			Class Functions:
				bool checkout(node *n, int time) - "checks out" a customer whose
				Node pointer is passed into the function. Time is the current
				time of day. The function first calls the customer function 
				"calc_wait_time" to calculate the customer's wait time, then uses
				this data to determine the time that the customer should be done
				checking out by adding it to the arrival time and service time.
				The function returns true when the customer is finally done 
				being processed by the cashier.
				        
				        int t
				        if(n->get_wait_time == 0)
							n->calc_wait_time
						
				        t = (n->get_arrival_time) + (n->get_wait_time) + 
				        (n->get_service_time)
						if(time < t)
							return false;
						else
							return true;
				
				This function is intended to be called multiple times for each
				customer, and the wait time should be calculated only once to 
				avoid errors. 
			  
		
*******************************************************************************/
#ifndef __CASHIER__
#define __CASHIER__
#include "node.hpp"
#include "customer.hpp"
#include "queue.hpp"
#include<iostream>
#include<string>
using namespace std;

class Cashier
{
	Node *curr_cust;
	
	public:
	Cashier();
	
	//Accessor Functions
	Node *get_curr_cust(void);
	
	
	//Mutator Functions
	void set_curr_cust(Node *c);
	
	//Class Functions
	bool checkout(int time);
};

#endif
