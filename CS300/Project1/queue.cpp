//Trevor Nierman
//R957F692
//Project 1

/*******************************************************************************
This file defines the functions used for the class "queue" to be used in 
main.cpp for project 1. 

*******************************************************************************/
#include "queue.hpp"
#include "node.hpp"
#include "customer.hpp"
#include<iostream>
#include<string>
using namespace std;

//Constructor 
Queue::Queue()
{
	front = NULL;
	rear = NULL;
	count = 0;
}

//Accessor Functions
int Queue::get_count(void)
{
	return count;
}

Node* Queue::get_front(void)
{
	return front;
}

Node* Queue::get_rear(void)
{
	return rear;
}

//Mutator Functions
void Queue::set_count(int c)
{
	count = c;
}

void Queue::add_count(void)
{
	count++;
}

void Queue::sub_count(void)
{
	count--;
}

void Queue::set_front(Node *f)
{
	front = f;
}

void Queue::set_rear(Node *r)
{
	rear = r;
}

//Class functions
bool Queue::is_empty(void)
{
	if(front == NULL)
	{
		return 1;
	}
	
	return 0;
}

bool Queue::enqueue(int num, int arr, int ser)
{
	//Creates a new node in dynamic memory
	Node *temp = new Node(num, arr, ser);
	
	//If queue is empty, both front and rear pointers point to it
	if(is_empty())
	{
		front = temp;
		rear = temp;
	}	
	
	else
	{
		//Old rear's "next" pointer is set to temp
		rear->set_next(temp);
		
		//New node becomes rear
		rear = temp;
		

	}
	
	//Update count
	count++;
	
	return 1;
}

void Queue::dequeue(void)
{		
	
	//Ensures list isn't empty
	if(!(is_empty()))
	{
		//If front is last node in list
		if(front == rear)
		{		
			//Set front and rear to NULL
			front = NULL;
			rear = NULL;
			
			//Update count
			count = 0;
		}
		
		else
		{
			//Set next node to front of queue
			front = front->get_next();
			
			//Update count
			count--;
		}
	}
}

void Queue::add_node(Node *n)
{
	//If queue is empty, both front and rear pointers point to it
	if(is_empty())
	{
		front = n;
		rear = n;
		count = 1;
	}

	else
	{
		//Old rear's "next" pointer set to n
		rear->set_next(n);
		
		//n becomes new rear
		rear = n;
		
		count++;
	}
}	
