//Trevor Nierman
//R957F692
//Project 1

/*******************************************************************************
This file defines the functions for the class "Cashier" to be used in project 1.

*******************************************************************************/
#include "queue.hpp"
#include "node.hpp"
#include "customer.hpp"
#include "cashier.hpp"
#include<iostream>
#include<cstring>
using namespace std;

//Constructor
Cashier::Cashier()
{
	curr_cust = NULL;
}

//Accessor Functions
Node* Cashier::get_curr_cust(void)
{
	return curr_cust;
}

//Mutator Functions
void Cashier::set_curr_cust(Node *c)
{
	curr_cust = c;
}

//Class Functions
bool Cashier::checkout(int time)
{
	//Calculates the wait time if it has not been calculated 
cout << "before";
	std::string mark = curr_cust->get_cust_mark();
        cout << "mark is: " << mark;
	if( mark == "NULL")
	{
		curr_cust->calc_cust_wait(time);
		curr_cust->set_cust_mark("HELPED");
	}
	
	//Calculates the point in time when the customer should be done checking out
	//Should remain constant when function is called multiple times for one 
	//customer
	int t = curr_cust->get_cust_arr() + curr_cust->get_cust_wait() + 
	curr_cust->get_cust_serv();
	
	//If it is equal to current time, customer is done checking out.
	if(time == t)
	{
		return true;
	}
	
	else 
	{
		return false;
	}
}
