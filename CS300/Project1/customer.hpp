//Trevor Nierman
//R957F692
//Project 1

/*******************************************************************************
This file defines the class "customer" to be used in main.cpp for project 1.
This class contains the customer number, the arrival time, and the time it will 
take to service the customer. It is used to store customer data for another
class; "node".  

		Data: int number - determined by "customers.txt", customer number
			  
			  int arrival_time - determined by "customers.txt", arrival time of
		      customer.
			  
			  int service_time - determined by "customers.txt", time it takes to
			  serve the customer.
		      
		      int wait_time - initially NULL for all customers, so it can be 
		      differentiated between a customer who had no wait time and a 
		      customer whose wait time was not calculated. Is equal to (time at 
			  checkout) - (time of arrival), which is calculated when customer 
			  is removed from the queue to checkout. If wait time is NULL when 
			  customer is passed to sim_data, they were rejected.
			   
		Functions:
			Accessors:
			int get_number - returns the customer number.
			
			int get_arr_time - returns the arrival time of customer.
			
			int get_serv_time - returns the service time of customer.
			
			Mutators:
			set_number(int n) - sets the customer number to n. Returns nothing.
			
			set_arr_time(int a) - sets the customer arrival time to a. Returns 
			nothing.
			
			set_serv_time(int s) - sets the customer service time to s. Returns
			nothing. 

*******************************************************************************/
#ifndef __CUSTOMER__
#define __CUSTOMER__
#include<iostream>
#include<string>
using namespace std;

class Customer
{
	//Customer number
	int number;
	
	//Arrival time
	int arrival_time;
	
	//Time it takes to check out customer
	int service_time;
	
	//Time spent waiting in line
	//Calculated at time of checkout, intially 0.
	int wait_time;
	
	//Marks unhelped customers as "NULL"
	string mark;
	
	public:
	
	//Constructor
	Customer();
	Customer(int n, int a, int s);
	
	//Accessor Functions
	int get_number(void);
	int get_arr_time(void);
	int get_serv_time(void);
	int get_wait_time(void);
	string get_mark(void);
	
	//Mutator Functions
	void set_number(int n);
	void set_arr_time(int a);
	void set_sev_time(int s);
	void calc_wait(int w);
	void set_mark(string m);
	
};

#endif	
