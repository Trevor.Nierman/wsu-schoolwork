//Trevor Nierman
//R957F692
//Project 1

/*******************************************************************************
This program simulates a grocery store with 3 cashiers. It implements the 
classes "Node", "Queue", "Customer", "Cashier", to simulate an average business
day and "Sim_data" to store the simulation data.
		
		Functions:
			queue get_shortest(line1, line2, line3) - Determines the shortest 
			line to checkout in, and returns it.
			
					if(line1 count < line2 count && line1 count < line3 count)
						return line1
					else if(line2 count < line1 count && line2 count < line1 
					count)
						return line2
					else
						return line3
						
			bool full_lines(line1, line2, line3) - Determines if all lines are 
			full. Returns true if they are, false otherwise. 
						
					if(line1 count < 7 && line2 count < 7 && line3 count < 7)
						return false
					else 
						return true
			
			void done_checking(Sim_data sim, Cashier cash, int time)
					-Hands the customer off to sim_data after they are done 
					checking out, processes them, and deletes them.
					
					if(cash.checkout(time))
					{
						sim_data customer = cashier customer
						cash customer = NULL
						sim_data.update()
						sim_data.delete()
					}
			
			
			
*******************************************************************************/
#include "sim_data.hpp"
#include "node.hpp"
#include "customer.hpp"
#include "cashier.hpp"
#include "queue.hpp"
#include<iostream>
#include<string>
#include<fstream>
using namespace std;

//Global constant TIME is the total time for simulation to run. 
int const TIME = 600;

Queue get_shortest(Queue l1, Queue l2, Queue l3)
{
	if(l1.get_count() < l2.get_count() && l1.get_count() < l3.get_count())
	{
		return l1;
	}
	
	else if(l2.get_count() < l1.get_count() && l2.get_count() < l3.get_count())
	{
		return l2;
	}
	
	else 
	{
		return l3;
	}
}
	
bool full_lines(Queue line1, Queue line2, Queue line3)
{
	if(line1.get_count() < 6 || line2.get_count() < 6 || line3.get_count() < 6)
	{
		return false;
	}
	
	else
	{
		return true;
	}
}

void done_checking(Sim_data &sim, Cashier &cash, int curr_time, Queue line)
{
	
	if(line.get_front() != NULL)
	cout << "Line1 front:" << " " << line.get_front()->get_cust_num() << " ";
	
	
	
	
	if(cash.checkout(curr_time))
	{
		//handoff customer to sim data
		sim.set_curr(cash.get_curr_cust());
		cout << "handoff ";
	
		//Update simulation data
		sim.update_data();
		
		//Delete customer
		sim.delete_node();
		cout << "Curr deleted ";	
		//Cashier grabs next customer

		if(line.get_front() != NULL) 
		{
			cout << "Set next " << line.get_count() << endl;

			cash.set_curr_cust(line.get_front());
		
			cout << "Next customer set ";
		}
		
		else
	
			cash.set_curr_cust(NULL);
	}
}


//MAIN PROGRAM
int main(void)
{
	//Create a customer pool of all customers
	Queue cust_pool;
	
	std::ios_base::sync_with_stdio( false );
	//Create temporary variables for node creation
	int temp_num, temp_arr, temp_ser, tot_cust;
	
	//Create an index to read in customers
	int i;
	
	//Read in "customers.txt"
	ifstream fin;
	
	fin.open("customers.txt");
	
	if(fin.fail())
	{
		cout << "The file could not be opened.\n";
	}
	
	else
	{
		//First value is total number of customers
		fin >> tot_cust;

		
		for(i = 0; i < tot_cust; i++)
		{
			//Reads data in order 
			fin >> temp_num;
			fin >> temp_arr;
			fin >> temp_ser;
			
			//Creates a new customer
			cust_pool.enqueue(temp_num, temp_arr, temp_ser);
		}
	}
	
	fin.close();

	cout << "file read.\n";

	//Create Sim_data to store data
	Sim_data sim;

	//Create cashiers
	Cashier cash1, cash2, cash3;
	
	//Create lines for cashiers, and a temporary line
	Queue line1, line2, line3;
	
	int curr_time;

	cout << "beginning simulation\n";

	//Begin simulation
	for(curr_time = 0; curr_time < TIME; curr_time++)
	{
		
		
		cout << endl;
		cout << "time: " << curr_time << " ";
		
		if(cash1.get_curr_cust() != NULL)
		cout << "cash1: " << cash1.get_curr_cust()->get_cust_num() << " " << "wait: " << cash1.get_curr_cust()->get_cust_wait() << " ";

		else if(cash2.get_curr_cust() != NULL)
		cout << "cash2: " << cash2.get_curr_cust()->get_cust_num() << " ";
		
		else if(cash3.get_curr_cust() != NULL)
		cout << "cash3: " << cash3.get_curr_cust()->get_cust_num() << " ";
		
		cout << "line1: " << line1.get_count() << " ";
		
		cout << "pool front: " << cust_pool.get_front()->get_cust_num() << " ";
		cout << "served: " << sim.get_served() << " ";
		
		
		
		
	        cout << "what??";
		

		//Check if cashiers are done checking out
		if(cash1.get_curr_cust() != NULL)
		{
			cout << "what??";
			done_checking(sim, cash1, curr_time, line1);

		}
	
		if(cash2.get_curr_cust() != NULL)
		{
			done_checking(sim, cash2, curr_time, line2);
		}
		
		if(cash3.get_curr_cust() != NULL)
		{
			done_checking(sim, cash3, curr_time, line3);
		}
	
	
		//A customer arrives
		if(cust_pool.get_front()->get_cust_arr() == curr_time)
		{
			//Check to ensure lines aren't maxed out
	
			//If they are, customer is rejected
			if(full_lines(line1, line2, line3))
			{
				//Customer first passed to sim to recieve data
				sim.set_curr(cust_pool.get_front());
			
				//Customer is removed from pool
				cust_pool.dequeue();
			
				//Sim data is updated, customer is deleted
				sim.update_data();
				sim.delete_node();
			}

		
			//Lines aren't maxed out
			else
			{
				//If cashier 1 is available
				if(cash1.get_curr_cust() == NULL)
				{	
					//Sets cashier 1's current customer to new customer, 
					//calculates their wait time.
					cash1.set_curr_cust(cust_pool.get_front());
					cust_pool.dequeue();
					
					cash1.get_curr_cust()->calc_cust_wait(curr_time);
					cash1.get_curr_cust()->set_cust_mark("HELPED");
					
				}
			
				else if(cash2.get_curr_cust() == NULL)
				{	
					//Sets cashier 2's current customer to new customer,
					//calculates their wait time.
					cash2.set_curr_cust(cust_pool.get_front());
					cust_pool.dequeue();
					
					cash2.get_curr_cust()->calc_cust_wait(curr_time);
					cash2.get_curr_cust()->set_cust_mark("HELPED");	
				}
				
				else if(cash3.get_curr_cust() == NULL)
				{
					//Sets cashier 3's current customer to new customer, 
					//calculates their wait time, marks them.
					cash3.set_curr_cust(cust_pool.get_front());
					cust_pool.dequeue();
					
					cash3.get_curr_cust()->calc_cust_wait(curr_time);
					cash3.get_curr_cust()->set_cust_mark("HELPED");
				}
			
				else
				{	
					//Determine the shortest line, add customer to that line
					get_shortest(line1, line2, line3).add_node(cust_pool.get_front());
				
					//Calculates wait time and marks customer before dequeueing
					cust_pool.get_front()->calc_cust_wait(curr_time);
					cust_pool.get_front()->set_cust_mark("HELPED");
			
					//Remove customer from customer pool
					cust_pool.dequeue();
				}
			}
		}
	//End simulation
	}





	cout << "simulation complete.\n";






	//Print results
	ofstream fout;
	
	fout.open("results.txt");
	
	if(fout.fail())
	{
		cout << "The file could not be found. Creating new results.txt...\n";
		
		fout.open("results.txt", ios::app);
	}
	
	fout << "Customers served: " << sim.get_served() << endl;
	fout << "Customers rejected: " << sim.get_rejected() << endl;
	fout << "Total wait: " << sim.get_tot_wait() << endl;
	fout << "Average wait: " << sim.get_tot_wait() << endl;
	
	fout.close();
	
	//End main
	return 0;
}
