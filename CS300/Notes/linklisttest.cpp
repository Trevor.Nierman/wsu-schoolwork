/*    Class Name: Node
	This class describes a node for a linked list.
    Data:       data - holds the data for a linked list
                link - the link to the next node in a linked list
                
    Mutator Functions:
                set_link - Sets the link to point to the next node
   
    Accessor Functions:
                get_link - Returns the pointer to the next node
                get_data - Returns the data from the class
                
    Function: Node
                Default Constructor that initializes the data to 0 and
                    the pointer to NULL.
                    
                Input Parms: None
                Output Parms: None
                
                data = 0
                pointer = NULL
    Function: Node 
               Overloaded Constructor that initializes the data to data passed and
                    the pointer to NULL.
                    
                Input Parms: Data - data for initialization
                Output Parms: None
                
                data = data
                pointer = NULL
    Function: compare_data
               Check the data passed in against the data in the class
               
               Input Parms: Data - data to test against
               Output Parms: 0 if equal
                             -1 if class data < data
                             1 if class data > data
                
                if (class data == data)
                  return 0
                else if (class data < data)
                  return -1
                else 
                  return 1

*/

#include <iostream>
using namespace std;

class Node
{
   double data;
   Node *link;

 public:
   Node ();
   Node (double data);
   void set_link(Node *ptr)
   {
        link = ptr;
   }
   Node *get_link()
   {
        return link;
   }
   int compare_data (double d);
   double get_data() { return data; }
};
Node::Node()
{
   data = 0;
   link = NULL;
}

Node::Node (double d)
{
   data = d;
   link = NULL;          
}

int Node::compare_data (double d)
{
   if (d == data)
      return 0;
   else if (data < d)
      return -1;
   else 
      return 1;
}

class Linked_list
{
   int count;
   Node *head, *tail;
 public:
   Linked_list();
   void add_node(double data); 
   bool delete_node (double data);
   void print_nodes();    
};

Linked_list::Linked_list()
{
   count = 0;
   head = tail = NULL;
}

void Linked_list::add_node(double data)
{
   // Create a new node with a NULL ptr.
   Node *node_ptr = new Node(data);
   Node *prev, *curr;
   
   curr = prev = head;
   // If there are no entries in the linked list,
   // add it to the head.
   if (head == NULL)
      tail = head = node_ptr;
   // If the data is greater than the data at the tail,
   // add the data at the end.
   else if (tail->compare_data(data)==-1)
   {
      tail->set_link(node_ptr);
      tail = node_ptr;
   }
   else 
   {
      // Otherwise, find where the data fits
      // Need the previous node address and the next node address
      // Doesn't work if data is equal.
      while (curr->compare_data(data)==-1)
      {
         prev = curr;
         curr = curr->get_link();
      }
      // The new node link will be set to the next position
      node_ptr->set_link(curr);
      // If the data that head points to is smaller than the inserted data,
      // then the link of head pointer is modified
      if (head->compare_data (data)==-1)
         head->set_link(node_ptr);
      else
         // Otherwise, the head pointer should point to the new node 
         head = node_ptr;
   }
}
bool Linked_list::delete_node (double data)
{
   Node *curr, *prev;
   
   // Start at the head pointer.
   curr = prev = head;
   
   // Go until the end of the list or the data is found
   while (curr != NULL && curr->compare_data (data) != 1)
   {
      prev = curr;
      curr = curr->get_link();
   }
   // If the node is not found, then return out of the routine
   // letting the calling function know the node was not found.
   if (curr == NULL)
      return 0;     // Failed to find node
   else
   {
      // If the data is in the one pointed to by head,
      // then delete it and modify the head pointer.
      if (curr == head)
      {  
         delete head;
         head = curr->get_link();
      }
      // Otherwise set the link of the next one
      else 
      {
         prev->set_link(curr->get_link());
         delete curr;
      }
   }   
}

void Linked_list::print_nodes()
{
   Node *temp;
   temp = head;
   while (temp != NULL)
   {
      cout << temp->get_data() << endl;
      temp = temp->get_link();
   }     
}
int main (void)
{
    Linked_list list;
    list.add_node(23.3);
    list.add_node (10.1);
    list.add_node (25);
    list.add_node (17);
    list.add_node (27);
    list.print_nodes(); 
    cout << "\n Delete nodes 25 and 10.1 \n";
    list.delete_node (25);
    list.delete_node (10.1);
    list.print_nodes();
    system("Pause");
    
}
