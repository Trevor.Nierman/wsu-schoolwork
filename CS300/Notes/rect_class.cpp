#include <iostream>
using namespace std;

//#define CONSTRUCTOR 

class Rectangle
{
      int len, width;
      float area, perimeter;
      
 public:
        // Constructor
#ifdef CONSTRUCTOR
        Rectangle (int l, int w);
#endif 
        void Set_len (int ln);
        void Set_width (int wid);
        void Calc (void);
        float Get_area (void);
        float Get_perimeter (void);
};


#ifdef CONSTRUCTOR
Rectangle::Rectangle (int l, int w)
{
     len = l;
     width = w;
}
#endif


void Rectangle::Set_len (int ln)
{
     len = ln;
}

void Rectangle::Set_width (int wid)
{
     width = wid;
}

void Rectangle::Calc (void)
{
     area = float (len) * width;
     perimeter = len * 2 + width * 2;
}

float Rectangle::Get_area (void)
{
     return area;
}

float Rectangle::Get_perimeter(void)
{
      return perimeter;
}

int main (void)
{
#ifndef CONSTRUCTOR    
    Rectangle r, r1, r2;         // Three instances of Rectangle r, r1, r2.  Also called objects.
#else
    Rectangle r(5, 7);
#endif    
    
    // Very burdensome
#ifndef CONSTRUCTOR
    r.Set_len (10);
    r.Set_width (25);
    r.Calc();
#endif
    cout << "\nArea is " << r.Get_area();
    cout << "\nPerimeter is " << r.Get_perimeter() << endl;
 
#ifndef CONSTRUCTOR
    cout << "\nArea is " << r1.Get_area();
    cout << "\nPerimeter is " << r1.Get_perimeter() << endl;
    
    cout << "\nArea is " << r2.Get_area();
    cout << "\nPerimeter is " << r2.Get_perimeter() << endl;
#endif    
    system ("PAUSE");
}
