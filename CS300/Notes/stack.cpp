#include <iostream>
using namespace std;

struct stack_data
{
   double num;
   string name;
};

class Node
{
   stack_data data;
   Node *link;

 public:
   Node ();
   Node (float, string);
   void set_link(Node *ptr)
   {
        link = ptr;
   }
   Node *get_link()
   {
        return link;
   }
   int compare_data (float d);
   stack_data get_data() { return data; }
   void process_data(void);
};

// Default constructor
Node::Node()
{
   data.num = 0;
   data.name = "Surprise";
   link = NULL;
}

// Initialize the Node with the given data.
Node::Node (float num, string name)
{
   data.num  = num;
   data.name = name;
   link = NULL;          
}

// Check the class data against the data passed to the routine.
// Return 0 if data is equal
// Return -1 if the data passed is less than the class data
// Return 1 if the data passes is greater than the class data.
int Node::compare_data (float d)
{
   if (data.num == d)
      return 0;
   else if (data.num < d)
      return 1;
   else 
      return -1;
}

// The process_data in this case simply prints it to the screen.
// It could do anything.  It could modify the data if desired.
void Node::process_data(void)
{
   cout << "\n Data: " << data.num << endl;
   cout << "Name: " << data.name;
}

class Stack
{
   Node *top;
   int count;
 public:
   Stack();
   bool push (float, string);
   bool pop ();
   stack_data stack_top ();
   bool empty();
   
};

Stack::Stack()
{
   top = NULL;
   count = 0;
}

bool Stack::push (float data, string name)
{
   bool success = false;
   Node *ptr = new Node(data, name);
   // Check to see if successfully allocated
   if (ptr != NULL)
   {
      // If allocated, check for top
      if (top != NULL)
      {
         // If top isn't NULL, put the pointer into the current Node
         ptr->set_link (top);
      }
      // Set the top equal to the new Node.
      top = ptr;
      count ++;
   
      success = true;
   }
   return success;
}


bool Stack::pop ()
{
    bool success = false;
    Node *temp;
    // Check to make sure there are items on the stack
    if (top != NULL) // if (top)   works also
    {
      // If there is a top, set success to true
      success = true;
      count --;
      // Save the top pointer so it can be deleted.
      temp = top;
      // Set the top pointer to the next data
      top = top->get_link();
      // Delete the former top
      delete temp;
    }

   return success;
}

stack_data Stack::stack_top()
{
   float data;
   // Check to make stack isn't empty
   if (top != NULL)  // if (top)   works also
      // Return the data
      return top->get_data();
  
   // Doesn't handle error case.    
}

bool Stack::empty()
{
   if (top != NULL)
      return false;
   else
      return true;
}

int main(void)
{
   Stack mystack;
   mystack.push(1,"What fun");
   mystack.push(2,"Great");
   mystack.push(3, "I love data structures");
   mystack.push(4, "Programming is fun");
   mystack.push(5, "You love this class");
   
   stack_data data;
   data = mystack.stack_top();
   cout << data.num << " " << data.name << endl;
   mystack.pop();
   data = mystack.stack_top();
   cout << data.num << " " << data.name << endl;

   cout << "\nStarting loop" << endl;
   while (!mystack.empty())
   {
      data = mystack.stack_top();
      mystack.pop();
      cout << data.num << " " << data.name << endl;
      
   }
   
   
   system ("pause");
   return 0;
}
