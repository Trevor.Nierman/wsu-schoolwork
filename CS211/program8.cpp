//program8.cpp
//Author: Trevor Nierman
//ID: R957F692
//Assignment: 8
//Description: A program in which the user enters up to ten words and they are then sorted alphabetically.
//Updated: 03/30/2016

#include <iostream>
#include <string>
using namespace std;

int const NUM_WORDS = 10;

void sort(string words[], int num){
  string temp;
  int i, j, min_index;
  
  for (i = 0; i < num - 1; i++){
    min_index = i;
    for (j = i + 1; j < num; j++)
      if (words[j] < words[min_index]){
	min_index = j;
      }
      
      if (i != min_index){
	temp = words[i];
	words[i] = words[min_index];
	words[min_index] = temp;
      }
  }
}

int main(void)
{
  string words[NUM_WORDS];
  string temp_word;	//Temporary holder to check for zero. 
  int i = 0, j = 0; 	//Word counters
  
  cout << "Hello!\n";
  cout << "Please enter up to ten words. Entering the number zero will terminate the program. \n";
  
  do{
    cout << "Remaining words: " << NUM_WORDS - i << endl;
    cout << "Please enter a word: ";
    cin >> temp_word;
    
    if(temp_word == "0"){
      break;
    }
    words[i] = temp_word;
    i++;
    
  }while(i < NUM_WORDS);
  
  sort(words, i);
  
  while(j <= i){
    
    cout << words[j] << " ";
    j++;
  }
  
  cout << endl;
  
  return 0;
}