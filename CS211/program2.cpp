//File Name: program2.cpp
//Author: Trevor Nierman
//ID: R957F692
//Assignment: 2
//Description: A program that determines the min, max and count for both positive and negative numbers
//Last updated: 02/08/2016

#include <iostream>
using namespace std;

int main(void)
{
  //Initialize variables and set counts to 0
  int pcount = 0, ncount = 0;
  double pmax, nmax, pmin, nmin, input;
  
  //Initial value
  cout << "Entering zero will terminate program.\nEnter a number: \n";
  cin >> input;

  while (input) {		//Loop ends when input is 0
    
    if (input > 0){
      
      if (pcount == 0) {	//Initially assigns pmax and pmin to input
	pmin = input;
	pmax = input;
      }
      
      else if (input > pmax) {
	pmax = input;		//Reassigns positive max to input if input is greater
      }
      
      else if (input < pmin) {	//Reassigns positive min to input if input is less
	  pmin = input;		
      }
      
      pcount++;			//Adds one to positive count
      
    }
   
    else if (input < 0){
      
      if (ncount == 0) {	//Initially assigns nmax and nmin to input
	nmax = input;
	nmin = input;
      }
      
      else if (input > nmax){	//Reassigns negative max to input if input is greater
	nmax = input;
      }
      
      else if (input < nmin){	//Reassigns negative min to input if input is less
	nmin = input;
      }
      
      ncount++;			//Adds one to negative count
    }
    
    //Restart Loop
    cout << "Enter a number: \n";	
    cin >> input;
  }

    //Print positive results
    if (pcount == 0){
      cout << "There were no positive values entered.\n";
    }
    else {
      cout << "There were " << pcount << " positive values entered, with a minimum of " << pmin << ", and a maximum of " << pmax << "\n";
    }

    //Print negative results
    if (ncount ==0){
      cout << "There were no negative values entered.\n";
    }
    else {
      cout << "There were " << ncount << " negative values entered, with a minimum of " << nmin << ", and a maximum of " << nmax << "\n";
    }
  return 0;
}