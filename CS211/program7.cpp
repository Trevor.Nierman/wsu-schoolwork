//File: program7.cpp
//Author: Trevor Nierman
//ID: R957F692
//Assignment: 7
//Description: 
//Updated: 03/21/2016

#include <iostream>
#include <cmath>
#include <iomanip>

using namespace std;

int const SIZE = 20;

//Creates menu
int menu(void){
  int choice;
  
  cout << "Please enter your selection.\n";
  cout << "\t1. y = a sin(x)\n";
  cout << "\t2. y = sin(x) + b\n";
  cout << "\t3. y = a sin(x) +b\n";
  cout << "\t4. Quit\n";
  cout << "Selection: ";
  cin >> choice;
  
  return(choice);
}

void fill_array(double array[]){
  int i;
  
  for(i = 0; i < SIZE; i++){
    array[i] = sin(i);
  }
}

int main(void)
{
  double sine_values[SIZE];
  double a, b, product;
  int choice, i;
  
  cout << "Hello!\n";
  
  fill_array(sine_values);		//Fills array
  
  do{
    choice = menu();			//Displays menu
    
    //y = a sin(x)
    if(choice == 1){
      cout << "Please enter the value of a: ";
      cin >> a;
      
      //Dispay results
      cout << setw(2) << "x\t" << setw(5) << "y\n";
      cout << "--\t-----\n";
      
      for(i = 0; i < SIZE; i++){
	product = a * sine_values[i];

	cout << setw(2) << i << "\t" << setw(5) << product << "\n";
      }
    }
    //y = sin(x) + b
    else if(choice == 2){
      cout << "Please enter the value of b: ";
      cin >> b;
      
      //Display results
      cout << setw(2) << "x\t" << setw(5) << "y\n";
      cout << "--\t-----\n";
      
      for(i = 0; i < SIZE; i++){
	product = sine_values[i] + b;
	
	cout << setw(2) << i << "\t" << setw(5) << product << "\n";
      }
    }
    //y = a sin(x) + b
    else if(choice == 3){
      cout << "Please enter the value of a: ";
      cin >> a;
      cout << "Please enter the value of b: ";
      cin >> b;
      
      //Display results
      cout << setw(2) << "x\t" << setw(5) << "y\n";
      cout << "--\t-----\n";
      
      for(i = 0; i < SIZE; i++){
	product = a * sine_values[i] + b;
	
	cout << setw(2) << i << "\t" << setw(5) << product << "\n";
      }
    }
    //User quits
    else if(choice == 4){
      cout << "Have a nice day!\n";
    }
    else{
      cout << "Invalid selection.\n";
    }
  }while(choice != 4);
  
  return 0;
}