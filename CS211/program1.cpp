/*
Filename: Program1.cpp
Author: Trevor Nierman	
Student ID: r957f692
Assignment: 1
Description: Prompts the user to enter two numbers and preforms an operation of the user's choice on those numbers
Last modified: 02/01/2016
*/
#include<iostream>
using namespace std;

int main(void)
{
//Introduce variables
  int op, a, b;
  
//Determine operation  
  cout << "Hello! Would you like to 1) Add 2) Subtract 3) Multiply 4) Divide?\n";
  cin >> op;	// Sets variable op to determine operation

//Determine inputs
  cout << "What is the first number you are using?\n";
  cin >> a;

  cout << "What is the second number you are using?\n";
  cin >> b;
  
  //Preform operation
  //Adds
  if (op == 1)
  {
    cout << a << "+" << b << "=" << a+b << "\n";
  }
  //Subtracts
  else if (op == 2) 
  {
    cout << a << "-" << b << "=" << a-b << "\n";
  }
  //Multiplies
  else if (op == 3)
  {  
    cout << a << "*" << b << "=" << a*b << "\n";
  }
  //Division
  else if (op == 4)
  {
    
    //Cannot divide by 0
    if (b == 0)
    {
      cout << "Cannot divide by zero. Terminating program.\n";
    }
  
    //Otherwise, divide normally
    else  
    {
    cout << a << "/" << b << "=" << a/b << "\n";
    }
    
  }
  //Invalid operation
  else
  {
    cout << op << " is an invalid operation. Terminating program.\n";
  }
    
  return 0;
}