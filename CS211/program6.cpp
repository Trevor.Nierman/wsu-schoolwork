//program6.cpp
//Author: Trevor Nierman
//ID: R957F692
//Assignment 6
//Description: A program that converts between feet/inches and meters
//Last Updated:03/02/2016

#include<iostream>
using namespace std;

int menu(void)		//Determines user intent
{
  int choice;		//User input 
  
  cout << "What would you like to do?\n";
  cout << "1)	Convert meters to feet and inches.\n";
  cout << "2)	Convert feet and inches to meters.\n";
  cout << "3)	Quit\n";
  
  cout << "Selection: ";
  cin >> choice;
  
  return(choice);
}

void meters_to_feet_and_inches(double meters, int& feet, double& inches)
{
  inches = meters / 0.0254;		//Converts meters to inches
  feet = inches / 12;			//Divides inches by 12 to obtain integer value for feet
  inches = inches - (feet * 12);		//Determines remaining inches after converting to feet
}

double feet_and_inches_to_meters(int feet, double inches)
{
  double meters;
  
  inches = inches + (feet * 12);	//Converts feet to inches and adds to existing inches
  meters = inches * 0.0254;		//Converts inches to meters
  
  return(meters);
}

//MAIN
int main(void)
{
  //Initialize Variables
  int choice;		//User menu selection
  int feet;
  double meters, inches;
  
  cout << "Hello!\n";
  
  do
  {
    choice = menu();
    
    if(choice == 1)		//Meters to feet and inches
    {
      //Determine meters
      cout << "Enter meters: ";
      cin >> meters;
      
      //Check to ensure value is positive or zero
      while(meters < 0)
      {
	cout << "Please enter a non-negative value: ";
	cin >> meters;
      }
      
      //Converts
      meters_to_feet_and_inches(meters, feet, inches);
      
      //Displays results
      cout << meters << " meters = " << feet << " feet and " << inches << " inches.\n";
    }
    
    else if(choice == 2)	//Feet and inches to meters
    {
      //Determins feet and inches
      cout << "Enter feet: ";
      cin >> feet;
      //Ensures "feet" is positive (or zero)
      while(feet < 0){
	  cout << "Please enter a non-negative value for feet: ";
	  cin >> feet;
	}
      cout << "Enter inches: ";
      cin >> inches;
      
      //Ensures "inches" is less than 12 and positive (or zero)
      while(inches < 0 || inches >= 12)
      {
	if(inches < 0)
	{
	  cout << "Please enter a non-negative value for inches: ";
	  cin >> inches;
	}
	else if(inches >= 12){
	  cout << "Please enter a value that is less than 12: ";
	  cin >> inches;
	}
      }
      
      //Converts
      meters = feet_and_inches_to_meters(feet, inches);
      
      //Displays results
      if(feet == 1){	      //Because grammar is important
	cout << feet << " foot and " << inches << " inches = " << meters << " meters\n";
      }
      else{
	cout << feet << " feet and " << inches << " inches = " << meters << " meters\n";
      }
    }
    
    else if(choice == 3)	//User quits
    {
      cout << "Have a nice day!\n";
    }
    
    else			//User enters an invalid entry
    {
      cout << "Please enter a valid option.\n";
    }
  }while(choice != 3);
  
  return (0);
}