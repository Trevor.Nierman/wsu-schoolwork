//program9.cpp
//Trevor Nierman
//ID: R957F692
//Assignment: 9
//
//Updated: 04.06.2016

#include <iostream>
#include <iomanip>
#include <fstream>
using namespace std;

const int WEATHER_STATIONS = 20;

int menu(void){
  int selection;
  
  cout << "Your options are:\n";
  cout << "1. Weather stations with an annual rainfall MORE than a cutoff\n";
  cout << "2. Weather stations with an annual rainfall LESS than a cutoff\n";
  cout << "3. Weather stations with monthly rainfall MORE than a cutoff\n";
  cout << "4. Weather stations with monthly rainfall LESS than a cutoff\n";
  cout << "5. Quit\n";
  
  cin >> selection;
  
  return(selection);
}

void display_years_more(double rainfall[][12], int stations, double cutoff){
  int i,j;
  double sum = 0;
  
  cout << "Weather stations that recorded more than " << cutoff << " inches for the year:\n";
  
  for(i = 0; i < stations; i++){
    sum = 0;
    for(j = 0; j < 12; j++){
      sum = sum + rainfall[i][j];
    }
    if(sum > cutoff){
      cout << setw(5) << "WS " << fixed << setprecision(2) << i + 1 << ":";
      for(j = 0; j < 12; j++){
	cout << setw(5) << rainfall[i][j];
      }
      cout << "\n";
    }
  }
}

void display_years_less(double rainfall[][12], int stations, double cutoff){
  int i, j;
  double sum;
  
  cout << "Weather stations that recorded less than " << cutoff << " inches for the year:\n";
  
  for(i = 0; i < stations; i++){
    sum = 0;
    for(j = 0; j < 12; j ++){
      sum = sum + rainfall[i][j];
    }
    if(sum < cutoff){
      cout << setw(5) << "WS " << fixed << setprecision(2)<< i + 1 << ":";
      for(j = 0; j < 12; j++){
	cout << setw(5) << rainfall[i][j];
      }
      cout << "\n";
    }
  }
}

void display_months_more(double rainfall[][12], int stations, double cutoff){
  int i, j;
  
  cout << "Weather stations that recorded more than " << cutoff << " inches for the month:\n";
  
  for(i = 0; i < stations; i++){
    for(j = 0; j < 12; j++){
      if(rainfall[i][j] > cutoff){ 
	cout << "Weather Station " << fixed << setprecision(2) << i + 1 << ", month " << j << ": " << setw(5) << rainfall[i][j] << " inches\n";
      }
    }
  }
}

void display_months_less(double rainfall[][12], int stations, double cutoff){
  int i, j;
  
  cout << "Weather stations that recorded more than " << cutoff << " inches for the month:\n";
  
  for(i = 0; i < stations; i++){
    for(j = 0; j < 12; j++){
      if(rainfall[i][j] < cutoff){ 
	cout << "Weather Station " << fixed << setprecision(2) << i + 1 << ", month " << j << ": " << setw(5) << rainfall[i][j] << " inches\n";
      }
    }
  }
}

int main(void)
{
  double rainfall[WEATHER_STATIONS][12];	//Creates main array
  double cutoff, r, k;
  int selection, i, j;				//User menu selection, fills array
  ifstream fin;					//File handle
  
  fin.open("rainfall.dat");
  
  /*fin.setf(ios::fixed);
  fin.setf(ios::showpoint);
  fin.precision(2);*/

  
  if(fin.fail()){
    cout << "Error. File could not be opened.\n";
  }
  else{
    //Read in data for array
    for(i = 0; i < WEATHER_STATIONS; i++){
      fin >> r;
      for(j = 0; j < 12; j++){ 
	fin >> k;
	rainfall[i][j] = k / 25.4;
	
	/*if(fin == i + 1){
	  fin.putback(rainfall[i][j])
	}*/
	
      }
    }
    
    cout << "Hello!\n";				//Manners matter
    cout << "What would you like to do today?\n";
  
    do{
    
      selection = menu();
    
      if(selection == 1){
	cout << "Enter minimum yearly rainfall (in inches): ";
	cin >> cutoff;
	
	display_years_more(rainfall, WEATHER_STATIONS, cutoff);
      }
      else if(selection == 2){
	cout << "Enter maximum yearly rainfall (in inches): ";
	cin >> cutoff;
	
	display_years_less(rainfall, WEATHER_STATIONS, cutoff);
      }
      else if(selection == 3){
	cout << "Enter minimum monthly rainfall (in inches): ";
	cin >> cutoff;
	
	display_months_more(rainfall, WEATHER_STATIONS, cutoff);
      }
      else if(selection == 4){
	cout << "Enter maximum monthly rainfall (in inches): ";
	cin >> cutoff;
	
	display_months_less(rainfall, WEATHER_STATIONS, cutoff);
      }
      else if(selection == 5){
	cout << "Have a nice day!\n";		//Manners still matter
      }
      else{
	cout << "Invalid option.\n";
      }
      
    }while(selection != 5);
    
  }
  
  return 0;
}