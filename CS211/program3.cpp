//Filename: program3.cpp
//Author: Trevor Nierman
//Student ID: R957F692
//Assignment: 3
//Description: A program that calculates the remaining balances for repaying a loan.
//Last Updated: 02/10/2016

#include<iostream>
#include<iomanip>
using namespace std;

int main(void)
{
  //Initialize variables
  int year, month;
  double rate, payment, interest, principal, balance, tot_int, tot_prin;
  
  //Sets double variables to two decimals
  cout.setf(ios::fixed);
  cout.setf(ios::showpoint);
  cout.precision(2);
  
  //Initialize total interest and principal to 0 to begin
  tot_int = 0;
  tot_prin = 0;
  
  //Input values
  cout << "Enter the amount of your loan: \n";
  cin >> balance;
  
  
  cout << "Enter your annual interest rate: \n";
  cin >> rate;
  rate = rate / 100; 	//makes interest rate a percentage
  rate = rate / 12; 	//makes annual interest rate monthly
  
  cout << "Enter your monthly payment: \n";
  cin >> payment;
  
  for (year = 1; year <= 3; year++) {
    //Create table heading
    cout << "YEAR " << year << "\n";
    cout << setw(5) << "Month     Interest     Principal     Balance\n";
    cout << setw(5) << "~~~~~     ~~~~~~~~     ~~~~~~~~~    ~~~~~~~~\n";
    
    for (month = 1; month <= 12; month++) {
      //Calculate interest, principal, and balance
      interest = balance * rate;
      principal = payment - interest;
      balance = balance - principal;
      
      //Create table 
      cout << setw(5) << month << setw(13) << interest << setw(14) << principal << setw(12) << balance << "\n";
      
      //Calculate totals
      tot_int = tot_int + interest;
      tot_prin = tot_prin + principal;
    }
    
    //Print totals
    cout << setw(5) << "          ~~~~~~~~     ~~~~~~~~~\n";
    cout << "Totals:"<< setw(12) << tot_int << setw(13) << tot_prin << "\n" << "\n";
  } 
    
    
  return 0;
}