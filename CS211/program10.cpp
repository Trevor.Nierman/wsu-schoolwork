//program10.cpp
//Trevor Nierman
//R957F692
//Assignment 10
//A program that maintains the inventory for a library
//04/18/2016

//Attempting extra-credit

#include <iostream>
#include <string>
#include <cstring>
#include <fstream>
using namespace std;

const int MAX_BOOKS = 100;

struct Book
{
  string author;
  string title;
  string publisher;
  string date;
  char status;
};

Book make_book(string line){
  Book book1;

  book1.author = line.substr(0, 37);
  book1.title = line.substr(38, 52);
  book1.publisher = line.substr(91, 18);
  book1.date = line.substr(110, 5);
  book1.status = line.at(116);
  
  cout << book1.author << endl;
  cout << book1.title << endl;
  cout << book1.publisher << endl;
  cout << book1.date << endl;
  cout << book1.status << endl;
  
  return book1;
}

//Menu function
int menu(void){
  int selection;
  
  cout << "\t1. Display all books\n";
  cout << "\t2. Display books in a category\n";
  cout << "\t3. Add a book\n";
  cout << "\t4. Write a list of books to a file\n";
  cout << "\t5. Quit\n";
  cout << "Selection: ";
  
  cin >> selection;
  
  return selection;
}

char cat_menu(void){
  char cat;
  cout << "Categories are:\n";
  cout << "\tN - new\n";
  cout << "\tD - damaged\n";
  cout << "\tR - reference\n";
  cout << "\tT - textbook\n";
  
  cout << "Selection: ";
  cin >> cat;
  
  return cat;
}

void clear_input()
{
  while (getchar() != '\n');
}

//MAIN
int main(void)
{
  int selection, count = 0, i;		//Menu selection, counter (number of books), counter
  Book list[MAX_BOOKS];			//Creates array "list" of books
  ifstream fin;
  ofstream fout;
  string txt;
  char category;
  
  fin.open("books.data");
  
  if(fin.fail()){
    cout << "Error. File could not be opened.\n";
  }
  else{
    //Reads in data from "data.txt" to "list"
    while (!fin.eof())
    {
      getline(fin, txt);
      if (txt.length() > 0)
      {
        list[count] = make_book(txt);
        count++;
      }
   }
   fin.close();    
  }
    
  cout << "Hello!";
  
  do{
    cout << "Please select an option below:\n";
    
    selection = menu();
    
    if(selection == 1){
      cout << "List of books in library:\n";
      cout << "~~~~~~~~~~~~~~~~~~~~~~~~~\n";
      for(i = 0;i < count;i++){
	cout << list[i].date << "\t" << list[i].author << endl;
	cout << list[i].title << "\t" << list[i].publisher << endl;
	cout << endl;
      }
    }
    else if(selection == 2){
      for(;;){
	category = cat_menu();
      
	if(category == 'N' || category == 'n'){
	  for(i = 0;i < count;i++){
	    if(list[i].status == 'N'){
	      cout << list[i].date << "\t" << list[i].author << endl;
	      cout << list[i].title << "\t" << list[i].publisher << endl;
	      cout << endl;
	    }
	  }
	  break;
	}
	else if(category == 'D' || category == 'd'){
	  for(i = 0;i < count;i++){
	    if(list[i].status == 'D'){
	      cout << list[i].date << "\t" << list[i].author << endl;
	      cout << list[i].title << "\t" << list[i].publisher << endl;
	      cout << endl;
	    }
	  }
	  break;
	}
	else if(category == 'R' || category == 'r'){
	  for(i = 0;i < count;i++){
	    if(list[i].status == 'R'){
	      cout << list[i].date << "\t" << list[i].author << endl;
	      cout << list[i].title << "\t" << list[i].publisher << endl;
	      cout << endl;
	    }
	  }
	  break;
	}
	else if(category == 'T' || category == 't'){
	  for(i = 0;i < count;i++){
	    if(list[i].status == 'T'){
	      cout << list[i].date << "\t" << list[i].author << endl;
	      cout << list[i].title << "\t" << list[i].publisher << endl;
	      cout << endl;
	    }
	  }
	  break;
	}
	else{
	  cout << "Please select a valid category.\n";
	}
      }//Ends for loop
    }
    else if (selection == 3){
      if(count == 100){
	cout << "Inventory full. A new book cannot be added today.\n";
      }
      else{ 
	cout << "Adding new book...\n";
	count++;
	
	cout << "Enter author: ";
	clear_input();
	getline(cin, list[count - 1].author);
	list[count - 1].author.resize(37);
	
	cout << "Enter title: ";
	getline(cin, list[count - 1].title);
	list[count - 1].title.resize(52);
	
	cout << "Enter publisher: ";
	getline(cin, list[count - 1].publisher);
	list[count - 1].publisher.resize(18);
	
	cout << "Enter date published: ";
	getline(cin, list[count - 1].date);
	list[count - 1].date.resize(5);
	
	cout << "Enter category (1-letter code): ";
	cin >> list[count - 1].status;
      
	cout << "New book successfully added!\n";
	cout << endl;
      }
    }
    else if(selection == 4){
      fout.open("books_new.data");
      if(fout.fail()){
	cout << "Error. File could not be created/updated.\n";
	cout << endl;
      }
      else{
	for(i = 0;i < count; i++){
	  fout << list[i].author << " ";
	  fout << list[i].title << " ";
	  fout << list[i].publisher << " ";
	  fout << list[i].date << " ";
	  fout << list[i].status << endl;
	}
	cout << "The file 'books_new.data' was successfully updated.\n";
	cout << endl;
      }
      fout.close();
    }
    else if(selection == 5){
      cout << "Have a nice day!\n";
    }
    else{
      cout << "Invalid selection.\n";
    }
  }while(selection != 5);
    
  return 0;
}