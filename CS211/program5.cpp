//Filename: program5.cpp
//Author: Trevor Nierman
//ID: R957F692
//Assignment: 5
//Description: A program that prompts the user for a minimum, maxium, and step size and displays the factorial starting at the minimum
// and continuing at each step, until less than or equal to the maximum.
//Last Updated: 02/23/2015

#include<iostream>
#include<iomanip>
using namespace std;

//Factorial function
long factorial(int x)
{
  int i = 1, count = 1;		//Variable i is the current step, count is the product
  
  if(x == 0){
    count = 1;
  }
  else if(x < 0){
    count = -1;
  }
  else{  
    while(i <= x){
      count = count * i;
      i = i + 1;
    }
  }
  
  return(count);
}

//MAIN
int main(void)
{
  int min, max, step, x, y;	//Variable "x" is the current step, Variable "y" is the factorial of x.
  
  //Determine minimum
  cout << "Hello!\n";
  cout << "What is your minimum value?\n";
  cin >> min;
  
  while(min < 0){		//Loop ensures a positive minimum
    cout << "Please enter a positive number.\n";
    cin >> min;
  }
  
  //Determine maximum
  cout <<"What is your maximum value?\n";
  cin >> max;
  
  while(max < min){		//Loop ensures max is greater than min
    if(max < 0){
      cout << "Please enter a positive number.\n";
      cin >> max;
    }
    else if(max < min){
      cout << "Please enter a maximum value greater than the minimum.\n";
      cin >> max;
    }
  }
  
  //Determine a step size
  cout << "What is your step size?\n";
  cin >> step;
  
  while(step <= 0){		//Loop ensures step size is greater than zero
    cout << "Please enter a step size greater than zero.\n";
    cin >> step;
  }
  
  //Create table
  cout << setw(10) << "x" << "\t" << setw(10) << "x-factorial\n";
  cout << "~~~~~~~~~~" << "\t" << "~~~~~~~~~~\n";
  
  //Begins calculations
  x = min;
  
  while(x <= max){
    y = factorial(x);
    cout << setw(10) << x << "\t" << setw(10) << y << "\n";
    x = x + step;
  }
}