//program10.cpp
//Trevor Nierman
//R957F692
//Assignment 10
//A program that maintains the inventory for a library
//04/18/2016

#include <iostream>
#include <string>
#include <cstring>
#include <fstream>
using namespace std;

const int MAX_BOOKS = 100;

struct Book
{
  string author;
  string title;
  string publisher;
  string date;
  char status;
};

Book make_book(string line){
  Book book1;

  book1.author = line.substr(0, 37);
  book1.title = line.substr(38, 90);
  book1.publisher = line.substr(91, 109);
  book1.date = line.substr(110, 115);
  book1.status = line.at(116);
  
  cout << book1.author << endl;
  cout << book1.title << endl;
  cout << book1.publisher << endl;
  cout << book1.date << endl;
  cout << book1.status << endl;
  
  return book1;
}

//Menu function
int menu(void){
  int selection;
  
  cout << "\t1. Display all books\n";
  cout << "\t2. Display books in a category\n";
  cout << "\t3. Write a list of books to a file\n";
  cout << "\t4. Quit\n";
  cout << "Selection: ";
  
  cin >> selection;
  
  return selection;
}

char cat_menu(void){
  char cat;
  cout << "Categories are:\n";
  cout << "\tN - new\n";
  cout << "\tD - damaged\n";
  cout << "\tR - reference\n";
  cout << "\tT - textbook\n";
  
  cout << "Selection: ";
  cin >> cat;
  
  return cat;
}

//MAIN
int main(void)
{
  int selection, count = 0, i;		//Menu selection, counter (number of books), counter
  Book list[MAX_BOOKS];			//Creates array "list" of books
  ifstream fin;
  ofstream fout;
  string txt;
  char category;
  
  fin.open("books.data");
  
  if(fin.fail()){
    cout << "Error. File could not be opened.\n";
  }
  else{
    //Reads in data from "data.txt" to "list"
    while (!fin.eof())
    {
      getline(fin, txt);
      if (txt.length() > 0)
      {
	fin.putback(txt);
        list[count] = make_book(txt);
        count++;
      }
   }
   fin.close();    
  }
    
  cout << "Hello!";
  
  do{
    cout << "Please select an option below:\n";
    
    selection = menu();
    
    if(selection == 1){
      cout << "List of books in library:\n";
      cout << "~~~~~~~~~~~~~~~~~~~~~~~~~\n";
      for(i = 0;i < count;i++){
	cout << list[i].date << "\t" << list[i].author << endl;
	cout << list[i].title << "\t" << list[i].publisher << endl;
	cout << endl;
      }
    }
    else if(selection == 2){
      for(;;){
	category = cat_menu();
      
	if(category == 'N' || category == 'n'){
	  for(i = 0;i < count;i++){
	    if(list[i].status == 'N'){
	      cout << list[i].date << "\t" << list[i].author << endl;
	      cout << list[i].title << "\t" << list[i].publisher << endl;
	      cout << endl;
	    }
	  }
	  break;
	}
	else if(category == 'D' || category == 'd'){
	  for(i = 0;i < count;i++){
	    if(list[i].status == 'D'){
	      cout << list[i].date << "\t" << list[i].author << endl;
	      cout << list[i].title << "\t" << list[i].publisher << endl;
	      cout << endl;
	    }
	  }
	  break;
	}
	else if(category == 'R' || category == 'r'){
	  for(i = 0;i < count;i++){
	    if(list[i].status == 'R'){
	      cout << list[i].date << "\t" << list[i].author << endl;
	      cout << list[i].title << "\t" << list[i].publisher << endl;
	      cout << endl;
	    }
	  }
	  break;
	}
	else if(category == 'T' || category == 't'){
	  for(i = 0;i < count;i++){
	    if(list[i].status == 'T'){
	      cout << list[i].date << "\t" << list[i].author << endl;
	      cout << list[i].title << "\t" << list[i].publisher << endl;
	      cout << endl;
	    }
	  }
	  break;
	}
	else{
	  cout << "Please select a valid category.\n";
	}
      }//Ends for loop
    }
    else if(selection == 3){
      fout.open("books_new.data");
      if(fout.fail()){
	cout << "Error. File could not be created/updated.\n";
      }
      else{
	for(i = 0;i < count; i++){
	  fout << list[i].author;
	  fout << list[i].title;
	  fout << list[i].publisher;
	  fout << list[i].date;
	  fout << list[i].status << endl;
	}
	cout << "The file 'books_new.data' was successfully updated.\n";
      }
      fout.close();
    }
    else if(selection == 4){
      cout << "Have a nice day!\n";
    }
    else{
      cout << "Invalid selection.\n";
    }
  }while(selection != 4);
    
  return 0;
}