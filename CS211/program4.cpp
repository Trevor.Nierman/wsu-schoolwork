//File Name: program4.cpp
//Author: Trevor Nierman
//Student ID: R957F692
//Assignment: 4
//A program that simulates a vending machine accepting money.
//Last Updated: 02/22/2016

#include <iostream>
#include <cctype>
using namespace std;


int menu(void)			//Function that displays the menu of the vending machine
{
  char choice;
  //int price;
  
  //Create menu
  cout << "  Available selections:\n";
  cout << "  P: Potato Chips  -  $1.25\n";
  cout << "  S: Snickers Bar  -  $1.35\n";
  cout << "  T: Pop Tart      -  $0.95\n";
  cout << "  C: Cookies       -  $1.50\n";
  cout << "  B: Brownies      -  $1.75\n";
  cout << "  N: Nuts          -  $1.40\n";
  
  cout << "Please enter your selection: \n";
  cin >> choice;
  
  return choice;
}


int accept_money(int price)	//Function that accepts payment and saves the value of the total paid.
{
  //Initializes users selection of money as a character
  char money;
  int inserted_money = 0;
  
  do
  {
    cout << "What type of coin or bill are you inserting? (please enter a letter): ";
    cin >> money;
    switch(money) 
    {
      case 'N':
      case 'n': inserted_money = inserted_money + 5;
		break;
      case 'Q':
      case 'q': inserted_money = inserted_money + 25;
		break;
      case 'd':
      case 'D': inserted_money = inserted_money + 100;
		break;      
    }
    cout << "Total inserted: " << inserted_money << " cents\n";
    cout << "Balance due: " << price - inserted_money << " cents\n";
    
  }while(price > inserted_money);

  return inserted_money; 
}

//Function that computes the change due from the price and amount paid
int compute_change(int total_paid, int total_price)
{
  int change;	//Initializes variable "change" as change due in cents
  
  change = total_paid - total_price;
  
  return change;
}

//MAIN 
int main(void)
{
  char repeat;		//User's choice to repeat program
  char choice;		//User's menu choice
  int total_price = 0;	//Price (in cents)
  int total_paid = 0;	//Money inserted by user (in cents)
  int change = 0;	//Change due (in cents)
  
  do
  {
    //Begin vending
    cout << "Hello!\n";
    cout << "What would you like to eat today?\n";
    
    choice = menu();
    choice = toupper(choice);
    
    switch(choice)
    {
      case 'P': total_price = 125;
		break;
      case 'S': total_price = 135;
		break;
      case 'T': total_price = 95;
		break;
      case 'C': total_price = 150;
		break;
      case 'B': total_price = 175;
		break;
      case 'N': total_price = 140;
		break;
      default : cout << "Please enter a valid selection.\n";
		choice = menu();
		choice = toupper(choice);
    }
  
    cout << "The price of your selection is " << total_price << " cents.\n";
    cout << "This machine accepts:\n";
    cout << "N - Nickles\n";
    cout << "Q - Quarters\n";
    cout << "D - Dollars\n";
    
    total_paid = accept_money(total_price);
  
    change = compute_change(total_paid, total_price);
  
    cout << "Change due: " << change << " cents\n";
    cout << "Would you like to make another purchase? (Y/N)\n";
    cin >> repeat;
  }while(repeat == 'Y' || repeat == 'y');
  
  cout << "Enjoy and have a nice day!\n";
  
return 0;
}