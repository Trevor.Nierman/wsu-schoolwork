#program2c.py

#Determines the square root of a number using Newton's method with a number of iterations determined by the user.

#Trevor Nierman
#R957F692

import math

def main():
	x0 = eval(input("Find the square root of: "))

	x1 = x0

	n = eval(input("From how many iterations: "))

	for i in range(n):
		x1 = (x1 + (x0 / x1)) / 2
		print(x1)

	print("After ", n, " iterations, the square root of ", x0, " is ", x1)
	
	diff = math.sqrt(x0) - x1

	print("This is a difference of ", diff, " from the actual value of ", math.sqrt(x0))

main()
