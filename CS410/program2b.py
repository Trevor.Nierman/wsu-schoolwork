#program2b.py

#A program that uses summation series to approximate pi.

#Trevor Nierman
#R957F692

def main():

	n = eval(input("How many times would you like to sum? "))
	
	sum = 0
	
	for i in range(n):

		#Denominator can be represented as 2i + 1
		denom = (2 * i) + 1

		#Sum is equal to previous sum +/- 4/denominator (the +/- can be represented by -1 to a power)
		sum = sum + (( -1 ) ** i) * (4 / denom)
	
	print("The approximation is ", sum)
	
	import math
	approx = math.pi - sum
	
	print("This is a difference of", approx, "from the actual value.")

main()
