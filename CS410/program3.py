#program3.py

#This program prompts the user for a message and a key, then encodes the message using the key and a shift cypher.

#Trevor Nierman
#R957F692

def main():

	import string
	
	#Obtain key
	key = eval(input("What is the key for this message? "))
	
	#Obtain message
	mess = input("What is the message? ")

	#Determine message length
	length = len(mess)

	new_mess = [0 for i in range(length)] 

	#Begin cypher
	for i in range(length):

		#Excludes spaces
		if(mess[i] != " "):
			
			#Shift characters
			char = ord(mess[i])


			shift_char = char + key

			#Upper-case character
			if char > 64 and char < 91:
				if shift_char > 90:
					shift_char = ((shift_char - 65) % 26) + 65

			#Lower-case character
			if char > 96 and char < 123:
				if shift_char > 122:
					shift_char = ((shift_char - 97) % 26) + 97
						
			new_mess[i] = chr(shift_char)
	
	#Combine string array into single string
	new_mess = ''.join(new_mess)
	print(new_mess)

main()
