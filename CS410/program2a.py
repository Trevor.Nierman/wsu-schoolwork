#program2a.py

#A program that computes the value of an investment over ten years with a given interest rate and compounding period.

#Trevor Nierman
#R957F692

def main():
	principal = eval(input("Intial principal: "))
	apr = eval(input("Annual interest rate as a decimal percentage: "))
	period = eval(input("How many times is interest compounded each year? "))
	
	#Interest compounded period * 10 times
	count = 10 * period
	
	for i in range(count):
		principal = principal * (1 + (apr / period))

	print("The value of your investment in ten years is ", principal)

main() 
