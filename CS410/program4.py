#program4.py

#Replicates the function "wc" from linux/unix. Gives the number of words, characters, and lines in a specified file.

#Trevor Nierman
#R957F692

def main():
	fi = input("What file would you like to evaluate? ")
	fin = open(fi, "r")
	
	data = fin.read()

	length = len(data)

	#The number of lines is equal to the number of newline characters
	lines = 0
	
	#For a word to be a word there are two cases: either a character followed by a space,
	#or a character followed by a newline.
	words = 0
	
	#Begin counting
	for i in range(length):
		if data[i] == "\n":
			lines = lines + 1
			
			#Case 2 for a word
			if data[i - 1] != " " and data[i - 1] != "\n":
				words = words + 1
			
		else:
			#Case 1 for a word
			if data[i] == " ":
				if data[i - 1] != " " and data[i - 1] != "\n":
					words = words + 1
		
	#Characters in a file is the length of the string "data"
	print("Characters: ", length)
	print("Words: ", words)
	print("Lines: ", lines)


main()
