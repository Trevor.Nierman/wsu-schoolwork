#Program5.py

#A program that calculates loan repayment and displays in a table format

#Trevor Nierman
#R957F692

def main():

	principal = eval(input("What is the principal on the loan? "))
	currPrincipal = principal

	interest = eval(input("What is the annual interest rate (as a decimal?)"))
	monthlyRate = interest / 12

	length = eval(input("How long is the loan (years)? "))
	totalMonths = length * 12

	monthlyPayment = principal * ((monthlyRate) / ( 1 - (1 + monthlyRate)**(-1 * totalMonths)))
	
	totalInterest = 0
	
	print("Payment schedule for a $", principal, " loan at ", interest * 100, "% interest over ", length, " years.")

	print(" Month \t Payment \t  Reduction \t  Interest \t  Loan Balance")
	print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")

	for i in range(totalMonths):
		#Calculate monthly interest
		monthlyInt = currPrincipal * monthlyRate
		
		totalInterest = totalInterest + monthlyInt

		#Calculate principal reduction from monthly interest
		princReduct = monthlyPayment - monthlyInt

		#Calculate new principal owed
		currPrincipal = currPrincipal - princReduct

		#Print Results
		#Month
		print(i + 1, end = "\t")	
		
		#Monthly Payment
		print("{0:10.2f}".format(monthlyPayment), end = "\t")

		#Principal Reduction
		print("{0:10.2f}".format(princReduct), end = "\t")

		#Interest paid
		print("{0:10.2f}".format(monthlyInt), end = "\t")

		#Loan Balance
		if(i is totalMonths - 1):
			print("{0:10.2f}".format(0))   	
		else:
			print("{0:10.2f}".format(currPrincipal))

	print("The total interest that was paid was {0:0.02f}".format(totalInterest))

main()
		
