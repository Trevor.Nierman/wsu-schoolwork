#program6.py

#A program that both produces barcodes from zip codes, and stores them in the file "bar_codes.txt", as well as reads in bar codes and converts them back to their zip code form.

#Trevor Nierman
#R957F692

def main():
	import os.path
	
	print("Hello!")

	#Determine function
	choice = menu()
	
	while (choice !=  3):

		#Encoding zipcodes
		if choice is 1:
		
			#Obtain input file
			file = input("Name of file with zipcodes: ")
			
			#Ensure file exists
			while(not os.path.isfile(file)):
				print("File not found, try again.")
				file = input("Name of file with zipcodes: ")
	
			#Open file
			infile = open(file, "r")
			
			#Obtain output file (creating it if it doesn't exist)
			file = input("Name of file to print barcodes: ")
			outfile = open(file, "w")
	
			print("Evaluating zipcodes")

			for line in infile:
				
				#Convert from zip to barcode
				barcode = ConvertZip(line)
				
				#Print to file
				print(barcode, file= outfile)
		
				barcode = ""
		
			#end for
			
			print("Zipcodes successfully encoded.")
		
			#close files
			infile.close()
			outfile.close()
	
		#end if
		
		#Decoding barcodes
		elif choice is 2:

			#Obtain input file
			file = input("Name of file with barcodes: ")

			#Ensure file exists
			while(not os.path.isfile(file)):
				print("File not found, try again.")
				file = input("Name of file with barcodes: ")
			
			#Open files
			infile = open(file, "r")
	
			#Obtain output file (creating it if it doesn't exist)
			file = input("Name of file to print zipcodes: ")
			outfile = open(file, "w")

			print("Evaluating barcodes")

			zipCode = ""

			for line in infile:
				
				#Decode from barcode to zipcode
				zipCode = ConvertBar(line)

				#Print to file
				print(zipCode, file= outfile)
			
			#end for
		
			print("Barcodes successfully decoded.")
	
			#Close files
			infile.close()
			outfile.close()

		#end elif

		#Quit progam
		elif choice is 3:
		
			print("Exiting...")
	
		#end elif

		#Invalid input
		else:
		
			print("Invalid option")

		#end else
	
		choice = menu()

	#end  while


def menu():
	print("What would you like to do? ")
	print("   1. Encode zipcodes to barcodes ")
	print("   2. Decode barcodes to zipcodes ")
	print("   3. Quit ")

	choice = eval(input(""))

	return choice


def ConvertZip(zipCode):
	
	#Initialize barcode
	barcode = "!"

	#Make zipCode a string
	zipCode = str(zipCode)
	
	zip_len = len(zipCode)
	
	for i in range(zip_len):

		barcode = barcode + NumToCode(zipCode[i])
	
	#end for 

	#Add check
	checkSum = 0
	
	for i in range(5):
		checkSum = checkSum + eval(zipCode[i])

	#end for

	check = 10 - (checkSum % 10)
	check = str(check)
	
	barcode = barcode + NumToCode(check) + "!"

	return barcode



def NumToCode(i):

	#Convert i to a string
	i = str(i)	

	#i is a number
	if i == "0":

		bar = "!!..."

	elif i == "1":

		bar = "...!!"

	elif i == "2":
	
		bar = "..!.!"

	elif i == "3":
	
		bar = "..!!."

	elif i == "4":
		
		bar = ".!..!"

	elif i == "5":
	
		bar = ".!.!."

	elif i == "6":

		bar = ".!!.."

	elif i == "7":

		bar = "!...!"

	elif i == "8":

		bar = "!..!."

	elif i == "9":
	
		bar = "!.!.."
	
	#zipCode[i] is -, + or unlisted
	else:
		bar = ""
	
	return bar


def ConvertBar(bar):
	
	#Initialize zip, index
	zipCode = ""
	i = 1 #skips initial ! in every barcode

	barLen = len(bar)

	for i in range(1,barLen,5):
		j = i + 5

		barChunk = bar[i : j]

		if len(zipCode) is 5:	
		
			zipCode = zipCode + "-" + CodeToNum(barChunk)

		elif len(zipCode) is 10:
	
			zipCode = zipCode + "+"
			zipCode = zipCode + CodeToNum(barChunk)

		else:
			zipCode = zipCode + CodeToNum(barChunk)

	zipLen = len(zipCode)

	#Strips off "+"/"-" and check from end of bar code
	if zipLen == 7 or zipLen == 12:     #<zipcode> + <extra sign> +  check
	
		return zipCode[0 : zipLen - 2]

	else:				    #Strips only check
	
		return zipCode[0 : zipLen - 1] 
	


def CodeToNum(bar):

	code = ""

	if bar == "!!...":
		code = "0"
	
	elif bar == "...!!":
		code = "1"

	elif bar == "..!.!":
		code = "2"

	elif bar == "..!!.":
		code = "3"

	elif bar == ".!..!":
		code = "4"

	elif bar == ".!.!.":
		code = "5"

	elif bar == ".!!..":
		code = "6"

	elif bar == "!...!":
		code = "7"

	elif bar == "!..!.":
		code = "8"

	elif bar == "!.!..":
		code = "9"
	
	return code
main()	
