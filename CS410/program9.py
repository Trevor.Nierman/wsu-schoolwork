#program9.py

"""Sorts the class 'student', which uses the hours and quality points of a student to calculate the gpa. 
"""


#Trevor Nierman
#R957F692

class Student:

	def __init__(self, name, hours, qpoints):
		self.name = name	
		self.hours = float(hours)
		self.qpoints = float(qpoints)

	def getName(self):
		return self.name

	def getHours(self):
		return self.hours

	def getQpoints(self):
		return self.qpoints

	def getGPA(self):
		return self.qpoints/self.hours

#End class


def makeStudent(infoStr):
	#infoStr is a tab separated line: name hours qpoints






#	print(infoStr)










	name, hours, qpoints = infoStr.split("\t")
	return Student(name, hours, qpoints)

#End function


def readStudents(file):
	
	infile = open(file, 'r')
	studentList = []

	for line in infile:

		student = makeStudent(line)
		tuple = (student.getGPA(), student)
		studentList.append(tuple)

	#End for

	infile.close()

	return studentList

#End function


def writeStudents(studentList, file):
	
	#open output file
	outfile = open(file, 'w')

	for s in studentList:
		
		#Eliminate tuple
		student = s[1]

		#Print results to output file
		print("{0}\t{1}\t{2}".format(student.getName(), student.getHours(), student.getQpoints()), file = outfile)

	#End for

	outfile.close()

#End function


def main():

	#Recieve input file from user
	inputFile = input("Student file: ")

	#Create student list
	studentList = readStudents(inputFile)
	
	#Sort list by GPA
	studentList = sorted(studentList, reverse = True)

	#Recieve output file from user
	outputFile = input("Ouput file: ")
	writeStudents(studentList, outputFile)

#End main


main()	
