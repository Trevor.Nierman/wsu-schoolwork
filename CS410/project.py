#Trevor Nierman
#R957F692

#Project.py - Final Project

#A program that accepts user input and converts it to/from Morse Code.

def main():

	print("Hello. With this function you can convert between the Latin Alphabet and Morse code.")
	print()	

	#Recieve user input
	choice = menu()

	while(choice != 3):

		#English to Morse
		if(choice == 1):
		
			print("Please enter your text:")
			print()

			#Receive user input
			userInput = input("Message: ")

			#Convert and print results
			print("Your text translates to")
			print(LettersToMorse(userInput))
			
		#Morse to English
		elif(choice == 2):

			print("Enter message as Morse code using period and dash. Separate letters with a space and words with a slash.")
			print()
			
			#Recieve user input
			userInput = input("Message: ")

			#Convert and print results
			print("Your Morse code translates to")
			print(MorseToLetters(userInput))
			
		#Quit
		elif(choice == 3):

			print("Exiting...")


		#Invalid Selection
		else:
			print("That's not a valid selection.")

		#End if/else

		#Ask to reiterate
		print("What would you like to do now?")
		choice = menu()	

	#End while
	
#End main function


def menu():
	
	print("Options are:")
	print()
	print("1. Convert from English to Morse Code")
	print("2. Convert from Morse Code to English")
	print("3. Quit")

	return eval(input("Selection: "))

#End menu function


def LettersToMorse(userInput):
	
	#Converts from Latin letters to combinations of -/. given the string input 'userInput'.

	output = ""
	
	#Make input a list
	inputList = list(userInput)

	#Convert letters to Morse code and add to output
	for i in inputList:
		
		i = convertLetter(i)

		output = output + i + " "

	return output

#End LettersToMorse function


def convertLetter(i):
	
	#Converts single letter 'in' to morse code, and returns it
	if(i == 'a' or i == 'A'):
		return '.-'
	elif(i == 'b' or i == 'B'):
		return '-...'
	elif(i == 'c' or i == 'C'):
		return '-.-.'
	elif(i == 'd' or i == 'D'):
		return '-..'
	elif(i == 'e' or i == 'E'):
		return'.'
	elif(i == 'f' or i == 'F'):
		return '..-.'
	elif(i == 'g' or i == 'G'):
		return '--.'
	elif(i == 'h' or i == 'H'):
		return '....'
	elif(i == 'i' or i == 'I'):
		return '..'
	elif(i == 'j' or i == 'J'):
		return '.---'
	elif(i == 'k' or i == 'K'):
		return '.-..'
	elif(i == 'm' or i == 'M'):
		return '--' 
	elif(i == 'n' or i == 'N'):
		return '-.'
	elif(i == 'o' or i == 'O'):
		return '---'
	elif(i == 'p' or i == 'P'):
		return '.--.'
	elif(i == 'q' or i == 'Q'):
		return '--.-'
	elif(i == 'r' or i == 'R'):
		return '.-.'
	elif(i == 's' or i == 'S'):
		return '...'
	elif(i == 't' or i == 'T'):
		return '-'
	elif(i ==  'u' or i == 'U'):
		return '..-'
	elif(i == 'v' or i == 'V'):
		return '...-'
	elif(i == 'w' or i == 'W'):
		return '.--'
	elif(i == 'x' or i == 'X'):
		return '-..-'
	elif(i == 'y' or i == 'Y'):
		return '-.--'
	elif(i == 'z' or i == 'Z'):
		return '--..'
	elif(i == '0'):
		return '-----' 
	elif(i == '1'):
		return '.----'
	elif(i == '2'):
		return '..---'
	elif(i == '3'):
		return '...--'
	elif(i == '4'):
		return '....-'
	elif(i == '5'):
		return '.....'
	elif(i == '6'):
		return '-....'
	elif(i == '7'):
		return '--...'
	elif(i == '8'):
		return '---..'
	elif(i == '9'):
		return '----.'
	#Unidentified character
	else:
		return '<Unidentified character>'

#End ConvertLetter function


def MorseToLetters(userInput):

#Converts Morse code input to Latin letter output

	#Output initialized as empty string
	output = ""

	#Convert userInput to list of morse code
	inputList = userInput.split()
	
	#Convert morsecode list to letters and add to output
	for i in inputList:

		i = convertMorse(i)
 		
		output = output + i

	return output

#End MorseToLetter function


def convertMorse(i):
	
	if(i == '.-'):
		return 'a'
	elif(i == '-...'):
		return 'b'
	elif(i == '-.-.'):
		return 'c'
	elif(i == '-..'):
		return 'd'
	elif(i == '.'):
		return 'e'
	elif(i == '..-.'):
		return 'f'
	elif(i == '--.'):
		return 'g'
	elif(i == '....'):
		return 'h'
	elif(i == '..'):
		return 'i'
	elif(i == '.---'):
		return 'j'
	elif(i == '-.-'):
		return 'k'
	elif(i == '.-..'):
		return 'l'
	elif(i == '--'):
		return 'm'
	elif(i == '-.'):
		return 'n'
	elif(i == '---'):
		return 'o'
	elif(i == '.--.'):
		return 'p'
	elif(i == '--.-'):
		return 'q'
	elif(i == '.-.'):
		return 'r'
	elif(i == '...'):
		return 's'
	elif(i == '-'):
		return 't'
	elif(i == '..-'):
		return 'u'
	elif(i == '...-'):
		return 'v'
	elif(i == '.--'):
		return 'w'
	elif(i == '-..-'):
		return 'x'
	elif(i == '-.--'):
		return 'y'
	elif(i == '--..'):
		return 'z'
	elif(i == '.----'):
		return '1'
	elif(i == '..---'):
		return '2'
	elif(i == '...--'):
		return '3'
	elif(i == '....-'):
		return '4'
	elif(i == '.....'):
		return '5'
	elif(i == '-....'):
		return '6'
	elif(i == '--...'):
		return '7'
	elif(i == '---..'):
		return '8'
	elif(i == '----.'):
		return '9'
	elif(i == '-----'):
		return '0'
	elif(i == '/'):
		return ' '
	#Unidentified character
	else:
		return '<unidentified character>'

#End convertMorse function


main()
