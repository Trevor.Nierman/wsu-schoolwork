#program7.py

#A program that converts math expressions from infix to postfix and calculates the results of postfix expressions

#Trevor Nierman
#R957F692

def main():
	print("This function converts infix expressions to postfix and evaluates postfix expressions.")
	print()
	print("What would you like to do?")

	choice = menu()

	while choice != 3:
		if choice == 1:
			print("Converting infix to postfix...")
			print()
			
			#Obtain input
			infix = input("Please enter the infix expression: ")
			
			#Convert infix
			InfixToPost(infix)
			
		elif choice == 2:
			print("Evaluating postfix expression...")
			print()

			#Obtain input
			postfix = input("Please enter the postfix expression with spaces between the terms: ")
	
			#Evaluate expression
			EvalPost(postfix)

		elif choice == 3:
			print("Terminating program...")

		else:
			print("Please choose a valid option.")

		#End if/else
		
		#Reset to menu
		print()
		print("What else would you like to do?")
		choice = menu()

#End main	


def EvalPost(postfix):
	
	opStack = []
	output = ""
	postfixList = postfix.split(" ")

	for i in postfixList:
			
		#If element is not +, -, /, *, **, or //
		if FindOp(i):
					
			try:

				opStack.append(int(i))

			except ValueError:
		
				print("Cannot evaluate unknown operator.")
				
				#Terminate function
				break

		#If element is operator other than unary plus/minus
		elif i != "#" or i != "~":
			
			#Ensure the correct number of operands on stack
			if len(opStack) >= 2: 

				#Pop top operators from stack
				op1 = opStack.pop()
				op2 = opStack.pop()

				#Evaluate and push back onto stack
				opStack.append(EvalOps(op1, op2, i))
	
			#Otherwise error
			else:
			
				print("Missing Operand:", i, "requires two operands.")

				#End looping
				break

			#End if/else

		#Element is unary plus/minus	
		elif i == "#" or i == "~":
		
			#Case: unary plus
			if i == "#":

				#Ensure at least one operand on stack
				if len(opStack) >= 0:
	
					op1 = math.abs(opStack.pop())
					opStack.append(op1)

				#Error if empty stack
				else:
				
					print("Missing Operand:", i, "requires an operand.")
					
					#End looping
					break
	
				#End if/else
			
			#Case: unary minus
			else:
		
				#Ensure at least one operand on stack
				if len(opStack) >= 0:

					op1 = opStack.pop() * (-1)
					opStack.append(op1)

				#Error if empty stack
				else:
			
					print("Missing operand:", i, "requires an operand.")
					
					#End looping
					break

				#End if/else

			#End if/else
			
	#Print results	
	print(postfix, "=", opStack.pop())


#End Function


def EvalOps(op1, op2, operator):

	if operator == "+":
		
		return op1 + op2

	elif operator == "-":
		
		return op1 - op2

	elif operator == "*":
		
		return op1 * op2
	
	elif operator == "/":
		
		return op1 / op2

	elif operator == "**":

		return op1 ** op2

	elif operator == "//":
	
		return op1 // op2

	else: 
		return False

#End Function


def menu():
	print(" 1. Convert infix to postfix")
	print(" 2. Evaluate postfix expression")
	print(" 3. Quit")

	choice = eval(input("Selection: "))
	return choice

#End Function


def InfixToPost(infix):

	#Initialize Stack, output
	opStack = []
	output = ""

	#Eliminate spaces from infix expression
	infixList = ElimSpaces(infix)

	#Convert adjacent numbers to double digits
	infixList = ListInfix(infixList)

	#Change unary plus to #, unary minus to ~, and expontentiation to ** (vs |*|*| as it would appear after ListInfix
	infixList = ConvertOperators(infixList)

	#Test for errors
	if ErrorCheck(infixList):

		#Iterate over "infixList"
		for i in infixList:

			#Determine if i is operand (number)
			if FindOp(i):

				#if i is operand, added to end of output
				output = output + i + " "		

			#Pushed to stack if i is left parenthesis
			elif i == "(":

				opStack.append(i)

			#If i is right parenthesis, begin popping operators from stack
			elif i == ")":
	
				token = opStack.pop()
		
				if token != "(":

					output = output + token

				#Keep popping until left parenthesis is found
				while opStack[-1] != "(":

					#Pop top of stack to output
					output = output + opStack.pop() + " "

				#end while
	
			#i is a operator
			else:

				#Stack is empty
				if len(opStack) == 0:

					#Operator is added to stack
					opStack.append(i)
	
				else:

					#While stack is not empty and priority of the stack is greater than the current 
					while (len(opStack) != 0) and CompareOps(opStack[-1], i) == True:
						
						#Pop operator off stack
						output = output + opStack.pop()
				
						output = output + " "
					
					#End while

					#Operator is pushed to stack
					opStack.append(i)

				#End if/else
									
			#End if/else
	
		#Pop remaining operators on stack to output
		while len(opStack) != 0:

			#Ensures output does not contain parenthesis
			if opStack[-1] != "(":

				output = output + opStack.pop()

				output = output + " "
			
			#Top of stack is (
			else:
			
				opStack.pop()
	
			#End if/else
		
		#Print output
		print(infix, "=", output)
		
	#End ErrorCheck if
	
#End Function


def ErrorCheck(infixList):
#Checks for errors in "infixList", returning true if none detected, or false and printing the error if one is detected

	infixLen = len(infixList)
	detector = ""

	for i in range(infixLen):

		#Case: "(", but no ")"
		if infixList[i] == "(":
		
			#Iterate through remaining list to find ")"
			j = i

			while j != infixLen:
				
				if infixList[j] == ")":

					detector = "Found"

				#End if

				j += 1

			#End while	

			if detector != "Found":
				
				print("Imbalanced parenthesis: missing right parenthesis")
				return False
	
			#End if

		#Case: ")", but no "("
		elif infixList[i] == ")":
			
			#Iterate through previous list to find "("
			j = 0
			while j < i:
				
				if infixList[j] == "(":
					
					detector = "Found"

				#End if

				j += 1

			#End while

			if detector != "Found":
				
				print("Imbalanced parenthesis: missing left parenthesis")
				return False
				
		#Case: Imbalanced binary operators 
		elif infixList[i] in ["+", "-", "*", "**", "/", "//"]:
			
			#Check i+1 and i-1 for operands

			#Binary operator is not surrounded by operands
			if not FindOp(infixList[i - 1]) and not FindOp(infixList[i + 1]):
		
				print("Imbalanced operator: ", infixList[i], "requires two operands")
				return False

			#Binary operator is first or last element
			elif i == 0 or i == infixLen - 1:
				
				print("Imbalanced operator:", infixList[i], "requires two operands")

				return False
			
			#End if/else
		
		#Case: Improper symbol
		
		






		#End if/else

	#End for

	#No errors
	return True  

#End Function


def ConvertOperators(infixList):
#Converts operators

	output = []
	i = 0
	infixLen = len(infixList)

	while (i < infixLen):

		#Case: i could be unary plus (#)
		if infixList[i] == "+":
			
			#Check i-1 -> if not operand, unary plus
			if not FindOp(infixList[i - 1]) or i == 0:
				
				output.append("#")	

			else:
				output.append("+")

			#End if/else

		#Case: i could be unary minus (~)
		elif infixList[i] == "-":

			#Check i-1 -> if not operand, unary minus
			if not FindOp(infixList[i - 1]) or i == 0:

				output.append("~")


			else:
				output.append("-")

			#End if/else

		#Case: i could be exponentiation (**)
		elif infixList[i] == "*":
			
			#Check i+1 -> if *, exponentiation
			if infixList[i + 1] == "*":
		
				output.append("**") 
				
				i += 1

			else:
				output.append("*")

			#End if/else

		#Case: i could be integer division (//)
		elif infixList[i] == "/":
		
			#Check i+1 -> if /, integer division
			if infixList[i + 1] == "/":
			
				output.append("//")

				i += 1
				
			else:	

				output.append("/")

			#End if/else

		else: 
		
			output.append(infixList[i])
		
		i += 1

		#End while

	return output

#End Function


def ElimSpaces(input):
	#A simple function that eliminates spaces in infixList

	input = list(input)
	output = []
	inputLen = len(input)

	for i in range(inputLen):
	
		#If current element is a space
		if input[i] != " ":
			
			#Current element is sliced out of list
			output.append(input[i])
			
	return output
#End Function


def ListInfix(infix):
#Converts raw input infix to list form and creates double digit numbers

	infixList = []
	infixLen = len(infix)

	for i in range(infixLen):

		#If current element is operand
		if FindOp(infix[i]):

			#If previous element is operand
			if FindOp(infix[i - 1]) and i != 0:

				last = int(infixList.pop()) * 10
				last = last + int(infix[i])		
				last = str(last)
				
				#Previous element in infixList is updated
				infixList.append(last)
			
			#Previous element is operator, current element is added to end of list
			else:

				infixList.append(infix[i])
			#End if/else
			
		#Current element is operator
		else:
			
			#Current element is added to the end of list
			infixList.append(infix[i])
	
		#End if/else
		
	return infixList


def CompareOps(op1, op2):
	#Compares operators and returns true if op1 has higher priority over op2

	#Assign values based on operator priority
	#Exponentiation
	if op1 == "**":
		val1 = 4

	#Unary plus / minus
	elif op1 == "~" or op1 == "#":
		val1 = 3
	
	#Multiplication / Division
	elif op1 == "*" or op1 == "/" or op1 == "//":
		val1 = 2

	#Addition / Subtraction
	elif op1 == "+" or op1 == "-":
		val1 = 1

	#Left parenthesis
	else:
		val1 = 0
	
	#End if/else


	#Exponentiation
	if op2 == "**":
		val2 = 4

	#Unary plus / minus
	elif op2 == "#" or op2 == "~":
		val2 = 3

	#Multiplication / Division
	elif op2 == "*" or op2 == "/" or op2 == "//":
		val2 = 2

	#Addition / Subtraction
	elif op2 == "+" or op2 == "-":
		val2 = 1

	#Left parenthesis
	else:
		val2 = 0

	#End if/else

	
	#Compare priority
	if val1 >= val2:
		return True

	else:
		return False
	
#End Function

def FindOp(operand):
#Returns true if operand, false if operator	
	
	#Ensure "operand" is a character
	operand = str(operand)

	#Determine if operantor -> if not, operand
	if operand in ["+", "-", "*", "%", "**", "~", "#", "/", "//", "(", ")"]:
		return False
	
	#input is an operand
	else:
		return True

#End Function


main()




