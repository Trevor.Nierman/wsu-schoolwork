#program8.py

"""Contains the class 'student' which uses the hours and quality points of a student to calculate the gpa. Also contains an algorithm to calculate the student with the best gpa out of a list. Results are stored in a list, which holds all students with the top gpa."""

#Trevor Nierman
#R957F692

class student:
	def __init__(self, name, hours, qpoints):
		self.name = name
		self.hours = float(hours)
		self.qpoints = float(qpoints)

	def getName(self):
		return self.name

	def getHours(self):
		return self.hours

	def getQpoints(self):
		return self.qpoints
	
	def getGPA(self):
		return self.qpoints/self.hours

def makeStudent(infoStr):
	#infoStr is a tab separated line: name	hours	qpoints

	name, hours, qpoints = infoStr.split("\t")
	return student(name, hours, qpoints)

def main():
	#Open file
	file = input("Enter the name of the grade file: ")
	infile = open(file, "r")

	best = []
	
	#First element of "best" list to first student
	first = makeStudent(infile.readline())
	best.append(first.getName())
	best.append(first.getHours())
	best.append(first.getGPA())
	
	#Read remaining lines
	for line in infile:
		#Line converted to student record
		s = makeStudent(line)

		#Compare to current best gpa
		#If new best, clear current list, begin a new one
		if s.getGPA() > best[2]:
			best = []
			best.append(s.getName())
			best.append(s.getHours())
			best.append(s.getGPA())

		#Expand list if gpas are equivolent
		elif s.getGPA() == best[2]:
			best.append(s.getName())
			best.append(s.getHours())
			best.append(s.getGPA())

	infile.close()

	#Print results
	print("The best student(s) are: ")
	print()
	for i in range(0, len(best), 3):
		print(best[i], best[i + 1], best[i + 2])


main()
