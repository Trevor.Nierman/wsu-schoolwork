#program1.py

#This program uses ten iterations of the Babylonian method to calculate square the square root of a number determined by the user.
#Trevor Nierman
#R957F692

def main():
	x0 = eval(input("Find the square root of: "))
	
	x1 = x0

	for i in range(10):
		x1 = (x1 + (x0 / x1)) / 2

		print(x1)

	print("After ten iterations, the square root of ", x0, " is ", x1)
	print(x1, " squared is ", x1**2)

main()
