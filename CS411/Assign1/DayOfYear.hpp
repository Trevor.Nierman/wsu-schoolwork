/***DayOfYear.hpp***
Trevor Nierman
R957F692
CS411
Assignment 1
*******************/

#include <iostream>
#include <cstdlib>

#ifndef DAY_OF_YEAR
#define DAY_OF_YEAR

using namespace std;

class DayOfYear{
public:
	//Constructors 
	DayOfYear(int month, int day);
	DayOfYear(int month);
	DayOfYear();

	//Accessors
	const int GetDay() { return(day); }
	const int GetMonth() { return(month); }

	//Mutators
	void SetDay(int inDay) { day = inDay; }
	void SetMonth(int inMonth) { month = inMonth; }

	//Operators
	bool operator ==(const DayOfYear& date);
	bool operator >(const DayOfYear& date);
	bool operator <(const DayOfYear& date);
	DayOfYear& operator ++(); //Pre-increment
	const DayOfYear operator ++(int); //Post-increment
	DayOfYear& operator --(); //Pre-decrement
	const DayOfYear operator --(int); //Post-decrement
	int operator [](int index);
	const DayOfYear operator -() const;

	//Friend Operator functions
	friend const DayOfYear operator +(const DayOfYear& date1, const DayOfYear& dat2);
	friend const DayOfYear operator +(const int addedDays, const DayOfYear& date);
	friend const DayOfYear operator +(const DayOfYear& date, const int addedDays);
	friend istream& operator >>(istream& in, DayOfYear& date);
	friend ostream& operator <<(ostream& out, const DayOfYear& date);

	//Functions
	const int ConvertDateToDays() const; //Converts current date to a total number of days (returns total number of days)
	const int ConvertMonthToDays(int inMonth) const; //Converts month (at day 1) to number of days (returns days)
	const int ConvertDaysToMonth(int totalDays); //Converts a total number of days to a month (returns a month)
	const int ConvertDaysToDays(int totalDays); //Converts a total number of days to days in a month (returns days)
	const int ReturnDayOfMonth(int inMonth); // Returns the number of days in a month (returns days)
	const bool CheckDayOfMonth(int inMonth, int inDay); //Returns true if inDay is a valid day of the month

private:
	//Variables
	int month;
	int day;
};

int operator -(const DayOfYear& date1, const DayOfYear& date2);
#endif
