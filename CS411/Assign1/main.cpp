//Trevor Nierman
//R957F692
//CS411
//Assignment 1

#include "DayOfYear.hpp"
#include <iostream>
using namespace std;

int main()
{
	DayOfYear a(12, 31);
	DayOfYear b(2);
	DayOfYear c;
	DayOfYear d;
	DayOfYear e;
	DayOfYear f(50, 12);
	DayOfYear g(10, 50);

	cout << "a = " << a << "\t" << a[1] << "/" << a[2] << "\t" << a[3] << "\n";
	cout << "b = " << b << "\t" << b[1] << "/" << b[2] << "\t" << b[3] << "\n";
	cout << "c = " << c << "\t" << c[1] << "/" << c[2] << "\t" << c[3] << "\n";
	cout << "d = " << d << "\t" << d[1] << "/" << d[2] << "\t" << d[3] << "\n";
	cout << "e = " << e << "\t" << e[1] << "/" << e[2] << "\t" << e[3] << "\n";
	cout << "f = " << f << "\t" << f[1] << "/" << f[2] << "\t" << f[3] << "\n";
	cout << "g = " << g << "\t" << g[1] << "/" << g[2] << "\t" << g[3] << "\n";

	cout << "Negating a, b, c and d" << endl;
	a = -a;
	b = -b;
	c = -c;
	d = -d;

	cout << "a = " << a << "\t" << a[1] << "/" << a[2] << "\t" << a[3] << "\n";
	cout << "b = " << b << "\t" << b[1] << "/" << b[2] << "\t" << b[3] << "\n";
	cout << "c = " << c << "\t" << c[1] << "/" << c[2] << "\t" << c[3] << "\n";
	cout << "d = " << d << "\t" << d[1] << "/" << d[2] << "\t" << d[3] << "\n";

	cout << "\n" << "\n";

	cout << "Resetting a, b, c, and d to initial values." << "\n";
	a = DayOfYear(12, 31);
	b = DayOfYear(2, 1);
	c = DayOfYear(1, 1);
	d = DayOfYear();

	cout << "a = " << a << "\t" << a[1] << "/" << a[2] << "\t" << a[3] << "\n";
	cout << "b = " << b << "\t" << b[1] << "/" << b[2] << "\t" << b[3] << "\n";
	cout << "c = " << c << "\t" << c[1] << "/" << c[2] << "\t" << c[3] << "\n";
	cout << "d = " << d << "\t" << d[1] << "/" << d[2] << "\t" << d[3] << "\n";

	cout << "Checking comparisons..." << "\n";
	if(c == d) cout << "c == d\n";
	else cout << "c != d\n";

	if(a == b) cout << "a == b\n";
	else cout << "a != b\n";

	if(c < d) cout << "c < d\n";
	else cout << "c >= d\n";

	if(c < a) cout << "c < a\n";
	else cout << "c >= a\n";

	if(c > d) cout << "c > d\n";
	else cout << "c <= d\n";

	if(a > c) cout << "a > c\n";
	else cout << "a <= c\n";

	cout << "\n";

	cout << "Checking increments and decrements...\n";

	cout << "b = " << b << "\n";
	cout << "b++ = " << b++ << "\n";
	cout << "b = " << b << "\n";
	cout << "++b = " << ++b << "\n";
	cout << "b = " << b << "\n";
	cout << "b-- = " << b-- << "\n";
	cout << "b = " << b << "\n";
	cout << "--b = " << --b << "\n";
	cout << "b = " << b << "\n";
	cout << "\n";

	cout << "Checking addition...\n";
	e = c + d;
	cout << "e = c + d = " << e << "\t" << e[3] << "\n";
	e = e + 366;
	cout << "e = e + 366 = " << e << "\t" << e[3] << "\n";
	e = 366 + e;
	cout << "e = 366 + e = " << e << "\t" << e[3] << "\n";
	e = a + 366;
	cout << "e = a + 1 = " << e << "\t" << e[3] << "\n";
	e = a + b;
	cout << "e = a + b = " << e << "\t" << e[3] << "\n";
	e = b + a;
	cout << "e = b + a = " << e << "\t" << e[3] << "\n";
	e = c + d;
	cout << "e = c + d = " << e << "\t" << e[3] << "\n\n";

	cout << "Checking subtraction...\n";
	cout << "c = " << c << "\t" << c[1] << "/" << c[2] << "\t" << c[3] << "\n";
	cout << "e = " << e << "\t" << e[1] << "/" << e[2] << "\t" << e[3] << "\n";

	int diff = e - c;
	cout << "e - c = " << diff << "\n\n";

	cout << "Checking subtraction with equal operands...\n";
	diff = e - e;
	cout << "e - e = " << diff << "\n\n";

	cout << "Checking subtraction when right operand is greater than left..." << "\n\n";
	diff = e - a;
	cout << "e - a = " << diff << "\n\n";

	cout << "Checking >> operator...\n";
	for(int i = 0; i < 4; i++){
		cout << "Enter a date using m/d, mm/d, m/dd, mm/dd: ";
		cin >> c;
		cout << "c = \t" << c << "\t" << c[3] << "\n";
	}

return 0;
}
