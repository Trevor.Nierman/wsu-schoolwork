/***DayOfYear.cpp***
Trevor Nierman
R957F692
CS411
Assignment 1
*********************/

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <cstring>

#include "DayOfYear.hpp"

using namespace std;


//Constructors
DayOfYear::DayOfYear(int inMonth, int inDay)
{
	if(inMonth > 12 || inMonth < 0) inMonth = 1;
	month = inMonth;

	if(inDay > ReturnDayOfMonth(month) || inDay < 0) inDay = 1;
	day = inDay;
}

DayOfYear::DayOfYear(int inMonth)
{
	//Ensure month is in range
	if(inMonth > 12 || inMonth < 0) inMonth = 1;

	month = inMonth;
	day = 1;
}

DayOfYear::DayOfYear(): month(1), day(1)
{}

//Friend Operators
const DayOfYear operator +(const DayOfYear& date1, const DayOfYear& date2)
{
	DayOfYear retDate;

	retDate.month = (date1.month + date2.month) % 12;
	retDate.day = (date1.day + date2.day) % retDate.ReturnDayOfMonth(retDate.month);

	return(retDate);
}

const DayOfYear operator +(const int addedDays, const DayOfYear& date)
{
	int numDays = date.ConvertDateToDays();
	numDays = numDays + addedDays;

	DayOfYear retDate;

	retDate.month = retDate.ConvertDaysToMonth(numDays);
	retDate.day = retDate.ConvertDaysToDays(numDays);

	return(retDate);
}

const DayOfYear operator +(const DayOfYear&date, const int addedDays)
{
	int numDays = date.ConvertDateToDays();
	numDays = numDays + addedDays;

	DayOfYear retDate;
	retDate.month = retDate.ConvertDaysToMonth(numDays);
	retDate.day = retDate.ConvertDaysToDays(numDays);

	return(retDate);
}

ostream& operator <<(ostream& out, const DayOfYear& date)
{
	switch(date.month){
	case(1):
		out << "January";
		break;
	case(2):
		out << "February";
		break;
	case(3):
		out << "March";
		break;
	case(4):
		out << "April";
		break;
	case(5):
		out << "May";
		break;
	case(6):
		out << "June";
		break;
	case(7):
		out << "July";
		break;
	case(8):
		out << "August";
		break;
	case(9):
		out << "September";
		break;
	case(10):
		out << "October";
		break;
	case(11):
		out << "November";
		break;
	case(12):
		out << "December";
		break;
	default:
		out << "Error in DayOfYear::output";
	}

	out << " " << date.day;

	return(out);
}

istream& operator >>(istream& in, DayOfYear& date)
{


	string input;
	in >> input;

	if(input.find("/") == 1 || input.find("/") == 2)
	{
		date.month = stoi(input.substr(0, input.find("/")));

		if(date.month > 12 || date.month < 0){
			cout << "Invalid month. Defaulting to January.";
			date.month = 1;
		}

		date.day = stoi(input.substr(input.find("/")  + 1));

		if(date.day > date.ReturnDayOfMonth(date.month) || date.day < 0){
			cout << "Invalid day. Defaulting to 1.";
			date.day = 1;
		}

	}else{
		cout << "Invalid input. Defaulting to January 1";
		date.month = 1;
		date.day = 1;
	}

	return(in);
}

//Overloaded Operators
bool DayOfYear::operator ==(const DayOfYear& date)
{
	if(this->month != date.month){
		return false;
	}else{
		if(this->day != date.day){
			return false;
		}else{
			return true;
		}
	}

	//Because warnings
	return(0);
}

bool DayOfYear::operator >(const DayOfYear& date)
{
	if(this->month <= date.month){
		return false;
	}else{
		if(this->day <= date.day){
			return false;
		}else{
			return true;
		}
	}

	//Because warnings
	return(0);
}

bool DayOfYear::operator <(const DayOfYear& date)
{
	if(this->month >= date.month){
		return false;
	}else{
		if(this->day >= date.day){
			return false;
		}else{
			return true;
		}
	}

	//Because warnings
	return(0);
}

int DayOfYear::operator [](int index)
{
	if(index == 1){
		return(month);
	} else if(index == 2){
		return(day);
	} else if(index == 3){
		return(this->ConvertDateToDays());
	}else{
		cout << "Index out of range.";
	}

	//Because warnings
	return(0);
}

DayOfYear& DayOfYear::operator++() //Pre-incrementing
{
	int numDays = this->ConvertDateToDays();
	++numDays;

	this->month = this->ConvertDaysToMonth(numDays);
	this->day = this->ConvertDaysToDays(numDays);

	return *this;
}

const DayOfYear DayOfYear::operator++(int) //Post-incrementing
{
	int temp = day;
	++(*this);

	return(DayOfYear(month, temp));
}

DayOfYear& DayOfYear::operator--() //Pre-decrementing
{
	int numDays = this->ConvertDateToDays();
	--numDays;

	this->month = this->ConvertDaysToMonth(numDays);
	this->day = this->ConvertDaysToDays(numDays);

	return *this;
}

const DayOfYear DayOfYear::operator--(int) //Post-decrementing
{
	int temp = day;
	--(*this);

	return(DayOfYear(month, temp));
}

//Functions

const int DayOfYear::ConvertDateToDays() const
{
	int totalDays = day;
	totalDays = totalDays + this->ConvertMonthToDays(this->month);

	return(totalDays);
}

const int DayOfYear::ConvertMonthToDays(int inMonth) const
{
	int totalDays;

	switch(inMonth){
	case(1):	//Jan
		totalDays = 0;
		break;
	case(2):	//Feb
		totalDays = 31;
		break;
	case(3):	//March
		totalDays = 59;
		break;
	case(4):	//April
		totalDays = 90;
		break;
	case(5):	//May
		totalDays = 120;
		break;
	case(6):	//June
		totalDays = 151;
		break;
	case(7):	//July
		totalDays = 181;
		break;
	case(8):	//Aug
		totalDays = 212;
		break;
	case(9):	//Sep
		totalDays = 243;
		break;
	case(10):	//Oct
		totalDays = 273;
		break;
	case(11):	//Nov
		totalDays = 304;
		break;
	case(12):	//Dec
		totalDays = 334;
		break;
	default:
		totalDays = 0;
	}

	return(totalDays);
}

const int DayOfYear::ConvertDaysToMonth(int totalDays)
{
	int retMonth;

	if(totalDays > 365) totalDays = totalDays % 365;


	if(totalDays > 334){
		retMonth = 12;
	} else if(totalDays > 304){
		retMonth = 11;
	} else if(totalDays > 273){
		retMonth = 10;
	} else if(totalDays > 243){
		retMonth = 9;
	} else if(totalDays > 212){
		retMonth = 8;
	} else if(totalDays > 181){
		retMonth = 7;
	} else if(totalDays > 151){
		retMonth = 6;
	} else if(totalDays > 120){
		retMonth = 5;
	} else if(totalDays > 90){
		retMonth = 4;
	} else if(totalDays > 59){
		retMonth = 3;
	} else if(totalDays > 31){
		retMonth = 2;
	} else{
		retMonth = 1;
	}

	return(retMonth);
}

const int DayOfYear::ConvertDaysToDays(int totalDays)
{
	int retDays;

	if(totalDays > 365) totalDays = totalDays % 365;

	if(totalDays > 334){
		retDays = totalDays % 334;
	} else if(totalDays > 304){
		retDays = totalDays % 304;
	} else if(totalDays > 273){
		retDays = totalDays % 273;
	} else if(totalDays > 243){
		retDays = totalDays % 243;
	} else if(totalDays > 212){
		retDays = totalDays % 212;
	} else if(totalDays > 181){
		retDays = totalDays % 181;
	} else if(totalDays > 151){
		retDays = totalDays % 151;
	} else if(totalDays > 120){
		retDays = totalDays % 120;
	} else if(totalDays > 90){
		retDays = totalDays % 90;
	} else if(totalDays > 59){
		retDays = totalDays % 59;
	} else if(totalDays > 31){
		retDays = totalDays % 31;
	} else {
		retDays = totalDays;
	}

	return(retDays);
}

const int DayOfYear::ReturnDayOfMonth(int inMonth)
{
	int retDays;
	switch(inMonth){
	case(1):
		retDays = 31;
		break;
	case(2):
		retDays = 28;
		break;
	case(3):
		retDays = 31;
		break;
	case(4):
		retDays = 30;
		break;
	case(5):
		retDays = 31;
		break;
	case(6):
		retDays = 30;
		break;
	case(7):
		retDays = 31;
		break;
	case(8):
		retDays = 31;
		break;
	case(9):
		retDays = 30;
		break;
	case(10):
		retDays = 31;
		break;
	case(11):
		retDays = 30;
		break;
	case(12):
		retDays = 31;
		break;
	}

	return(retDays);
}

const bool DayOfYear::CheckDayOfMonth(int inMonth, int inDay)
{
	bool retValue = false;

	switch(inMonth){
	case(1):
		if(inDay > 31 || inDay < 0) retValue = false;
		else retValue = true;
		break;
	case(2):
		if(inDay > 28 || inDay < 0) retValue = false;
		else retValue = true;
		break;
	case(3):
		if(inDay > 31 || inDay < 0) retValue = false;
		else retValue = true;
		break;
	case(4):
		if(inDay > 30 || inDay < 0) retValue = false;
		else retValue = true;
		break;
	case(5):
		if(inDay > 31 || inDay < 0) retValue = false;
		else retValue = true;
		break;
	case(6):
		if(inDay > 30 || inDay < 0) retValue = false;
		else retValue = true;
		break;
	case(7):
		if(inDay > 31 || inDay < 0) retValue = false;
		else retValue = true;
		break;
	case(8):
		if(inDay > 31 || inDay < 0) retValue = false;
		else retValue = true;
		break;
	case(9):
		if(inDay > 30 || inDay < 0) retValue = false;
		else retValue = true;
		break;
	case(10):
		if(inDay > 31 || inDay < 0) retValue = false;
		else retValue = true;
		break;
	case(11):
		if(inDay > 30 || inDay < 0) retValue = false;
		else retValue = true;
		break;
	case(12):
		if(inDay > 31 || inDay < 0) retValue = false;
		else retValue = true;
		break;
	default:
		cout << "Error: Month does not exist!";
		retValue = false;
	}

	return(retValue);
}

//NON-FRIEND / NON-MEMBER FUNCTIONS
int operator -(const DayOfYear& date1, const DayOfYear& date2)
{
	int numDays1 = date1.ConvertDateToDays();
	int numDays2 = date2.ConvertDateToDays();

	if(numDays1 > numDays2){
		int diffDays = numDays1 - numDays2;
		return(diffDays);
	}else{
		cout << "The operation is invalid.";
		return(0);
	}
}

const DayOfYear DayOfYear::operator -() const
{

	//Calculate the total number of days without using ConvertDateToDays (which uses private data).
	int numDays = this->ConvertDateToDays();

	//Error check
	if(numDays > 365) numDays = numDays % 365;

	/*
	To invert a number in a range with an odd number of elements:
		~Determine the middle element (in a 365 day calendar, its day 183)
		~Find the distance from the target element to the middle element
		~If the target element is less than the middle element, double the distance and add it to the target element
		~If the target element is greater than the middle element, double the distance and subtract it from the target element.
		~If the target is the middle element -> no change
	*/

	//Invert days
	int midDay = 183;
	int invDay;

	int dist = abs(numDays - midDay);
	dist = dist * 2;

	if(numDays > midDay){
		invDay = numDays - dist;
	}else if(numDays < midDay){
		invDay = numDays + dist;
	}else{
		invDay = numDays;
	}

	DayOfYear retDate;
	retDate.month = retDate.ConvertDaysToMonth(invDay);
	retDate.day = retDate.ConvertDaysToDays(invDay);

	return(retDate);
}

