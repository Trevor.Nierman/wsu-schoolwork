//Trevor Nierman
//R957F692
//CS 411
//Assignment 2

#ifndef PFASTRING_HPP
#define PFASTRING_HPP

#include<iostream>
#include<string>
using namespace std;


class PFAString {
public:
	//Constructors
	PFAString();
	PFAString(int cap);
	PFAString(int cap, string baseString);

	//Copy Constructor
	PFAString(const PFAString& oldVector);

	//Destructor
	~PFAString();

	int get_capacity();
	int get_size();
	void push_back(string str);
	void pop_back();
//	void resize(int newCap);
	void resize(int = 1, string = string());
	void empty_array();

	//Operator overloads
	bool operator ==(PFAString& vector);
	bool operator <(PFAString& vector);
	bool operator >(PFAString& vector);
	bool operator <=(PFAString& vector);
	bool operator >=(PFAString& vector);
	void operator =(const PFAString& vector);
	string operator [](int index) const;
	string& operator [](int index);

	//Friend overloads
	friend ostream& operator <<(ostream& out, const PFAString vector);

private:
	string *arr;
	int capacity;
	int size;

	//Functions
	bool reachedCapacity();
	void doubleCapacity();
	void setCapacity(int newCap);
	void oneCapacity();
};

#endif
