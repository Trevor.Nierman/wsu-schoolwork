//Trevor Nierman
//R957F692
//CS 411
//Assignment 2

#include <string>
#include "PFAString.hpp"
using namespace std;

//Constructors
PFAString::PFAString()
{
	capacity = 0;
	size = 0;
	arr = '\0';
}

PFAString::PFAString(int cap)
{
	size = 0;
	if(cap > 0){
		capacity = cap;
		arr = new string[capacity];
	}else{
		capacity = 0;
		arr ='\0';
	}
}

PFAString::PFAString(int cap, string baseString)
{
	if(cap > 0){
		size = cap;
		capacity = cap;
		arr = new string[capacity];

		for(auto i = 0; i < cap; i++){
			arr[i] = baseString;
		}
	}else{
		size = 0;
		capacity = 0;
		arr = '\0';
	}
}

//Copy Constructor
PFAString::PFAString(const PFAString& oldVector)
{
	capacity = oldVector.size;
	size = oldVector.size;
	arr = new string[oldVector.size];

	if(oldVector.size == 0){
		arr = new string[0];
	}else{
		arr = new string[oldVector.size];

		for(int i = 0; i < capacity; i++){
			arr[i] = oldVector.arr[i];
		}
	}
}

//Destructor
PFAString::~PFAString()
{
	delete [] arr;
}

//Functions
int PFAString::get_capacity()
{
	return capacity;
}

int PFAString::get_size()
{
	return size;
}

void PFAString::push_back(string str)
{
	size++;

	if(capacity == 0){
		oneCapacity();
	}else if(reachedCapacity()){

		size --;
		doubleCapacity();
		size++;

	}

	arr[size - 1] = str;
}

void PFAString::pop_back()
{
	if(size == 0){}
	else {size--;}
}

/*
void PFAString::resize(int newSize)
{
	if(newSize < 0){
		for(int i = 0; i < size; i++) arr[i] = '\0';
		newSize = 0;
	}else{

		if(newSize > capacity) setCapacity(newSize);

		if(size > newSize){
			for(int i = newSize - 1; i > size; i++){
				arr[i] = '\0';
			}
		}

		size = newSize;
	}
}
*/

void PFAString::resize(int newSize, string baseString)
{
	if(newSize < 0){
		for(int i = 0; i < size; i++){
			arr[i] = '\0';
		}

		newSize = 0;
	}else{
		if(newSize > capacity){
			setCapacity(newSize);
		}

		if(size > newSize){
			for(int i = newSize; i < size; i++){
				arr[i] = '\0';
			}
		}else{
			for(int i = size; i < newSize; i++){
				arr[i] = baseString;
			}
		}

		size = newSize;
	}
}

void PFAString::empty_array()
{
	size = 0;
}

bool PFAString::operator <(PFAString& vector)
{
	int smaller;

	if(this->size < vector.size) smaller = size;
	else smaller = vector.size;

	if(smaller > 0){
		for(int i = 0; i < smaller; i++){
			if(this->arr[i] >  vector.arr[i]) {return false;}
			else if(vector.arr[i] > this->arr[i]) {return true;}
		}

		if(vector.size > this->size) {return true;}
		else {return false;}

	}else{
		return false;
	}
}

bool PFAString::operator ==(PFAString& vector)
{
	return(!((*this < vector) || (vector < *this)));
}


bool PFAString::operator >(PFAString& vector)
{
	return(!((*this < vector) || (*this == vector)));
}

bool PFAString::operator <=(PFAString& vector)
{
	return((*this < vector) || (*this == vector));
}

bool PFAString::operator >=(PFAString& vector)
{
	return((*this > vector) || (*this == vector));
}

string PFAString::operator [](int index) const
{
	if(index > capacity || index < 0){
		cout<< "Attempted access at an out-of-bounds value. Exiting..." << endl;
		exit(1);
	}else{ return arr[index];}
}

string& PFAString::operator [](int index)
{
	if(index > size || index < 0) exit(1);
	else return arr[index];
}

void PFAString::operator =(const PFAString& vector)
{
	if(this == &vector){//Do nothing
	}else{
		delete [] arr;

		capacity = vector.size;
		size = vector.size;
		arr = new string[vector.capacity];

		for(int i = 0; i < size; i++){
			arr[i] = vector.arr[i];
		}
	}
}


//Friend function overloads
ostream& operator <<(ostream& out, const PFAString vector)
{
	for(int i = 0; i < vector.capacity; i++){
		out << i << ") " << vector.arr[i] << endl;
	}
	out << endl;

	return out;
}

//Private Functions
bool PFAString::reachedCapacity()
{
	if(size >= capacity) return true;
	else return false;
}

void PFAString::doubleCapacity()
{
	string *temp = new string[size];

	for(int i = 0; i < size; i++){
		temp[i] = arr[i];
	}

	delete [] arr;
	arr = new string[capacity * 2];

	for(int i = 0; i < size; i++){
		arr[i] = temp[i];
	}

	delete [] temp;
	capacity = capacity * 2;
}

void PFAString::setCapacity(int newCap)
{
	string* temp = new string[size];

	for(int i = 0; i < size; i++){
		temp[i] = arr[i];
	}

	delete [] arr;
	arr = new string[newCap];

	if(newCap > size){

		for(int i = 0; i < size; i++){
			arr[i] = temp[i];
		}
	}else{
		for(int i = 0; i < newCap; i++){
			arr[i] = temp[i];
		}
	}

	delete [] temp;
	capacity = newCap;
}

//Deletes current arr -> creates new arr of 1.
//To be used when current capacity is at 0.
void PFAString::oneCapacity()
{
	capacity = 1;
	delete [] arr;
	arr = new string[1];
}
