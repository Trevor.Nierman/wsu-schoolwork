//Trevor Nierman
//R957F692
//CS 411
//Assignment 4

//BST.hpp

#ifndef BST_HPP
#define BST_HPP

namespace Tree{

	template<class T>

	class BST {
	public:
		//Constructors
		BST();
 		BST(T key);
		BST(BST tree);	//Copy
		~BST();		//Destructor

		//Functions
		void insert(T key);
		void remove(T key);

		//Operator
		const void operator =(const BST& tree);

	private:
		//Variables
		Node *root;

		//Functions
		void destroy(Node* r);
		Node* getNode(T key, Node* currNode);



	//////////////
	//NODE CLASS//
	//////////////

		class Node {
		public:
			//Constructors
			Node();
			Node(T keyVal);
			~Node();

			//Accessor Functions
			Node* getParent(){ return parent; }
			Node* getLeft(){ return left; }
			Node* getRight(){ return right; }
			T getKey(){ return key; }


			//Mutator Functions
			void setParent(Node* newParent){ this->parent = newParent; }
			void setLeft(Node* newLeft){ this->left = newLeft; }
			void setRight(Node* newRight){ this->right = newRight; }
			void setKey(T key){ this->key = key; }

			//Operators
			//friend const bool operator < (T value);
			const bool operator < (const Node* node);
			//friend const bool operator == (T value);
			const bool operator == (const Node* node);
			//friend const bool operator != (T value);
			const bool operator != (const Node* node);
			//friend const bool operator << (T value);
			const ostream& operator << (const ostream& out, const Node* node);

		private:
			T key;
			Node *parent;
			Node *left;
			Node *right;
		};

	//////////////////
	//END NODE CLASS//
	//////////////////


	};
	#include "BST.inl"
};
#endif
