//Trevor Nierman
//R957F692
//CS411
//Assignment 4

//BST.cpp

#include "BST.hpp"

namespace Tree{

/**********************************************************************************************************************/
/////////////////////////////////////////////////BST FUNCTIONS/////////////////////////////////////////////////////////
/*********************************************************************************************************************/

	////////////////
	//CONSTRUCTORS//
	////////////////

	BST::BST()
	{
		*root = NULL;
	}

	template<class T>
	BST::BST(T key)
	{
		//Create root
		*root = new Node(key);

	}

	BST::BST(BST tree)
	{
		this->root = tree.root;
		destroy(tree.root);
	}

	BST::~BST()
	{
		delete root;
	}


	/////////////
	//FUNCTIONS//
	/////////////

	template<class T>
	void BST::insert(T key)
	{
		Node* newLoc = getNode(key, root);
		Node* newParent;

		while(newLoc != NULL){
			newParent = newLoc;
			newLoc = getNode(key, newLoc);
		}

		Node* newNode = Node(key);
		newNode->setParent(newParent);

		if(key < newParent->getKey()){ newParent->setLeft(newNode); }
		else{ newParent->setRight(newNode); }
	}

	template<class T>
	void BST::remove(T key)
	{
		if(key == root->getKey()){
			Node* oldRoot = this->root;

			if(oldRoot->getRight != NULL){

				if(oldRoot->getRight->getLeft() == NULL){ //Right becomes root if it has no left
					this->root = oldRoot->getRight();
					this->root->setParent(NULL);
					this->root->setLeft(oldRoot->getLeft());
				}

			}else if(oldRoot->getLeft != NULL){

				if(oldRoot->getLeft()->getRight() == NULL){ //Left becomes root if it has no right

					this->root = oldRoot->getLeft();
					this->root->setParent(NULL);
					this->root->setRight(oldRoot->getRight());

				}else{ //Left becomes root and **EVERYTHING** to its right gets moved to the leftmost  branch of the right

					this->root = oldRoot->getLeft();

					Node* leftOfRight = oldRoot->getRight();
					while(leftOfRight->getLeft() != NULL){
						leftOfRight = leftOfRight->getLeft();
					}

					leftOfRight->setLeft(this->root->getRight());
					this->root->setRight(oldRoot->getRight());
				}

			}else{
				//1 node tree, do nothing here and delete oldRoot normally
			}

			oldRoot->setLeft(NULL);
			oldRoot->setRight(NULL);

			delete oldRoot;

		}else{ //Proceed normally

			Node* oldLoc = getNode(key, root);
			Node* oldParent;

			while(oldLoc.getKey() != key)
				oldParent = oldLoc;
				oldLoc = getNode(key, oldLoc);
			}

			//Check the node's left child, append to parent
			if(oldLoc.getLeft() != NULL){
				oldParent.setLeft(oldLoc.getLeft());
				oldLoc.setLeft(NULL);
			}

			//Check the node's right child, append to parent
			if(oldLoc.getRight() != NULL){
				oldParent.setRight(oldLoc.getRight());
				oldLoc.setRight(NULL);
			}

			delete oldLoc;

		}
	}

	template<class T>
	Node* BST::getNode(T key, Node* currNode)
	{
		if(key >= currNode->getKey()){ return currNode->getRight(); }
		else{ return currNode->getLeft(); }
	}

	template<class T>
	BST<T>& BST::operator = (const BST& tree)
	{
		if(this != &tree){
			this->clearTree();
			Node* copyNode = new Node(tree->root->getKey());
			if(copyNode){ this->copyTree(copyNode); }

			return *this;
		}

	}

	BST<T>& BST::copyTree(Node* copyNode)
	{
		this->insert(copyNode->key);

		if(copyNode->getLeft() != NULL){ copyTree(copyNode->getLeft()); }
		if(copyNode->getRight() != NULL){ copyTree(copyNode->getRight(); }

		return *this;
	}

	void BST::destroy()
	{
		delete this->root;
		this->root = NULL;
	}



	Node* makeNodeCopy(Node* copyNode)
	{
		if(copyNode != NULL){ return Node* = new Node(copyNode->getKey()); }
		else { return NULL; }
	}

/*********************************************************************************************************************/
////////////////////////////////////////////END BST FUNCTIONS/NODE FUNCTIONS///////////////////////////////////////////
/*********************************************************************************************************************/


	/////////////////////
	//NODE CONSTRUCTORS//
	/////////////////////

	Node::Node()
	{
		parent = NULL;
		left = NULL;
		right = NULL;
	}

	template<class T>
	Node::Node(T keyVal)
	{
		key = keyVal;

		parent = NULL;
		left = NULL;
		right = NULL;
	}

	Node::~Node()
	{
		delete *left;
		delete *right;
	}


	//////////////////
	//NODE OPERATORS//
	//////////////////

	const bool Node::operator < (const Node* node)
	{
		return(this->key < node->key);
	}

	const bool Node::operator == (const Node* node)
	{
		return(!(this->key < node->key) && !(node->key < this->key));
	}

	const bool Node::operator != (const Node* node)
	{
		return(!(this->key == node->key));
	}

	const ostream& Node::operator << (const ostream& out, const Node* node)
	{
		out << node->key;
		return out;
	}


//End namespace Tree
}
