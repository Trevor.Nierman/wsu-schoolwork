//Trevor Nierman
//R957F692
//CS 411
//Assignment 3
//Grid.hpp


#ifndef GRID_H
#define GRID_H

//#define ROWS 20;
//#define COLUMNS 20;

#include <iostream>
#include <vector>

#include "GridCoordinates.hpp"
#include "Critter.hpp"
#include "Ant.hpp"
#include "Doodlebug.hpp"

//class Ant;
//class Doodlebug;
//class Critter;



namespace critter{

	class Grid
	{

		public:
		//Constructors
		Grid(GridCoordinates size);
		Grid(unsigned int rows, unsigned int columns);

		//Accessor Functions
		int getMaxRows() { return gridSize.row; }
		int getMaxColumns() { return gridSize.column; }
		GridCoordinates getSize() { return gridSize; }

		//Member Functions
		void renderGrid();
		void advanceTime();
		GridCoordinates getRandomLocation();
		bool emptyCell(GridCoordinates checkLocation);

		//Public Variables
		std::vector<Ant*> antList;
		std::vector<Doodlebug*> doodlebugList;

		//Variables
		int antsEaten = 0;
		int antsBorn = 0;
		int totalAnts = 0;

		int dBStarved = 0;
		int dBBorn = 0;
		int totalDb = 0;

		int time = 0;

		private:

		//Variables
		GridCoordinates gridSize;
	};

}

#endif
