//Trevor Nierman
//R957F692
//CS 411
//Assignment 3

#ifndef GRIDCOORDS_H
#define GRIDCOORDS_H

#include <iostream>

namespace critter{

	struct GridCoordinates
	{
		unsigned int row;
		unsigned int column;

		bool operator ==(GridCoordinates loc) { return(this->row == loc.row && this->column == loc.column); }
		//friend std::ostream& operator <<(std::ostream& out, const GridCoordinates& location);
		};


/*
		std::ostream& operator <<(std::ostream& out, const GridCoordinates& location)
		{
			out << location.row;
			out << ", ";
			out << location.column;

			return out;
		}

*/

}


#endif
