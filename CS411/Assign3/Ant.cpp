//Trevor Nierman
//R957F692
//CS 411
//Assignment 3
//Ant.cpp


#include<iostream>
#include<vector>
#include "Ant.hpp"
#include "Grid.hpp"

namespace critter{

	///////////////
	//Constructor//
	///////////////

	Ant::Ant() : Critter()
	{}

	Ant::Ant(Grid* grid) : Critter()
	{
		breedTime = 3;
		grid->totalAnts++;
	}

	Ant::Ant(Grid* grid, GridCoordinates loc) : Critter(loc)
	{
		breedTime = 3;
		grid->totalAnts++;
	}

	Ant::Ant(const Ant& ant)
	{
		this->location = ant.location;
		this->lifeTime = ant.lifeTime;
	}

	/////////////////////
	//Virtual Functions//
	/////////////////////

	/*
	Spawns a new Ant
	*/
	void Ant::spawnCritter(GridCoordinates spawnLocation, Grid* grid)
	{
		Ant* newAnt = new Ant(grid, spawnLocation);

		grid->antList.push_back(newAnt);
		grid->antsBorn++;
	}

	/*
	Removes an ant object from antList at the given location
	*/

	void Ant::die()
	{
		delete this;
	}

	//Moves *all* ants
	void Ant::moveAnts(Grid* grid)
	{
		for(unsigned i = 0; i < grid->antList.size(); i++){
			grid->antList[i]->move(grid);
		}

	}

	//Breeds *all* ants
	void Ant::breedAnts(Grid* grid)
	{
		for(unsigned i = 0; i < grid->antList.size(); i++){
			grid->antList[i]->breed(grid);
		}
	}

	//Public die function
	void Ant::getEaten(GridCoordinates sceneOfTheCrime, Grid* grid)
	{
		for(unsigned i = 0; i < grid->antList.size(); i++){
			if(grid->antList[i]->location == sceneOfTheCrime) {
				grid->antList[i]->die();
				grid->antList.erase(grid->antList.begin() + i);
				grid->antsEaten++;
				grid->totalAnts--;
			}
		}
	}

	void Ant::operator =(Ant& ant)
	{
		this->location = ant.location;
		this->lifeTime = ant.lifeTime;
	}

}

