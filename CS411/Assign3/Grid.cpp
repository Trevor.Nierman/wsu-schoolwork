//Trevor Nierman
//R957F692
//CS 411
//Assignment 3
//Grid.cpp

#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <random>
#include "Grid.hpp"

namespace critter{

	//Constructors
	Grid::Grid(GridCoordinates size)
	{
		gridSize = size;
	}

	Grid::Grid(unsigned int rows, unsigned int columns)
	{
		gridSize.row = rows;
		gridSize.column = columns;
	}

	////////////////////
	//Member Functions//
	////////////////////

	/*
	Renders a grid of any size
	*/
	void Grid::renderGrid(){
		std::string rowDivision = "-";
		std::string columnDivision = "|";

		//Create top row
		std::cout << " ";
		for(unsigned i = 0; i < gridSize.column; i++){
			std::cout <<  rowDivision << rowDivision << rowDivision << " ";
		}
		std::cout << std::endl;

		//Create intermediate rows (each square consists of 3 rows and columns -> top "border" (empty space), middle (empty space with critter), and bottom "border" which is same as top)
		for(unsigned i = 0; i < gridSize.row; i++){
			//Create top border
			for(unsigned j = 0; j < gridSize.column; j++){
				std::cout << columnDivision << " " << " " << " ";
			}
			std::cout << columnDivision << std::endl;

			//Create "center" (row which renders Critter)
			for(unsigned j = 0; j < gridSize.column; j++){
				std::cout << columnDivision << " ";

				std::string space = " ";

				for(unsigned k = 0; k < antList.size(); k++){
					if(antList[k]->getRow() == i && antList[k]->getColumn() == j){ space = "O"; }
				}
				for(unsigned k = 0; k < doodlebugList.size(); k++){
					if(doodlebugList[k]->getRow() == i && doodlebugList[k]->getColumn() == j){ space = "X"; }
				}

				std::cout << space << " ";
			}
			std::cout << columnDivision << std::endl;

			//Create bottom border
			for(unsigned j = 0; j < gridSize.column; j++){
				std::cout << columnDivision << " " << " " << " ";
			}

			std::cout << columnDivision << std::endl;

			for(unsigned j = 0;j < gridSize.column; j++){
				std::cout << " " << rowDivision << rowDivision << rowDivision;
			}

			std::cout << std::endl;
		}

		std::cout << std::endl;
	}


	/*
	Increments counters by one.
	*/
	void Grid::advanceTime(){
		for(unsigned i = 0; i < antList.size(); i++){
			antList[i]->addLife();
		}

		for(unsigned i = 0; i < doodlebugList.size(); i++){
			doodlebugList[i]->addLife();
			doodlebugList[i]->addDbHunger();
		}

		time++;
		std::cout << "Time: " << time << std::endl;

		std::cout << "Total Ants: " << totalAnts << std::endl;
		std::cout << "Ants Born: " << antsBorn << std::endl;
		std::cout << "Ants Eaten: " << antsEaten << std::endl;

		antsEaten = 0;
		antsBorn = 0;

		std::cout << "Total Doodlebugs: " << totalDb << std::endl;
		std::cout << "Doodlebugs Born: " << dBBorn << std::endl;
		std::cout << "Doodlebugs Starved: " << dBStarved << std::endl;

		dBStarved = 0;
		dBBorn = 0;

		std::string input;

		std::cout << "<ENTER>" << std::endl;

		do{
			std::getline(std::cin, input);
		}while(!input.find("\n") || !input.find("\r"));
	}

	GridCoordinates Grid::getRandomLocation()
	{
		std::random_device randNum;
		std::mt19937 generator(randNum());
		std::uniform_int_distribution<int> rowDist(0, gridSize.row - 1);
		std::uniform_int_distribution<int> colDist(0, gridSize.column - 1);

		GridCoordinates retGrid;

		retGrid.row = rowDist(generator);
		retGrid.column = rowDist(generator);

		return retGrid;
	}

	//Returns true if cell is empty
	bool Grid::emptyCell(GridCoordinates checkLocation)
	{
		//Check antList
		for(unsigned i = 0; i < antList.size(); i++){
			if(checkLocation == antList[i]->getLocation()){ return false; }
		}

		//Check doodlebugList
		for(unsigned i = 0; i < doodlebugList.size(); i++){
			if(checkLocation == doodlebugList[i]->getLocation()){ return false; }
		}

		//Check out of bounds
		if((checkLocation.row > gridSize.row - 1) || (checkLocation.row < 0)){ return false; }
		else if((checkLocation.column > gridSize.column - 1) || (checkLocation.column < 0)){ return false; }
		else { return true; }
	}

}

