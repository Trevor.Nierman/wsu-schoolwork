//Trevor Nierman
//R957F692
//CS 411
//Assignment 3
//Critter.hpp

#ifndef CRITTER_H
#define CRITTER_H

#include <iostream>

#include "GridCoordinates.hpp"
//#include "Grid.hpp"


namespace critter{

	class Grid;


	class Critter
	{

		protected:
		//Constructor
		Critter();
		Critter(GridCoordinates loc) { location = loc; }
		Critter(const Critter& critter);
		virtual ~Critter(){}

		//Virtual Functions
		virtual void move(Grid* grid);
		virtual void spawnCritter(GridCoordinates spawnLocation, Grid* grid);
//		virtual void die(GridCoordinates sceneOfTheCrime, Grid* grid) = 0;
		virtual void die() {}

		//Member Functions
		int getRandomAdjacent();

		//Variables
		int breedTime;
		int lifeTime = 0;
		GridCoordinates location;

		public:
		//Functions
		void breed(Grid* grid);
		void moveCritter(GridCoordinates newLocation);

		int getBreedTime() { return breedTime; }
		int getLifeTime() { return lifeTime; }
		void addLife() { lifeTime++; }
		unsigned int getRow() { return location.row; }
		unsigned int getColumn() { return location.column; }
		GridCoordinates getLocation() { return location; }
		GridCoordinates getCoordinatesAbove();
		GridCoordinates getCoordinatesBelow();
		GridCoordinates getCoordinatesRight();
		GridCoordinates getCoordinatesLeft();

		//Operators
		bool operator ==(const Critter& crit) { return this->location == crit.location; }
		void operator =(const Critter& critter);
	};
}

#endif
