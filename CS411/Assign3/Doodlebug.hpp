//Trevor Nierman
//R957F692
//CS 411
//Assignment 3
//Doodlebug.hpp

#ifndef DOODLEBUG_H
#define DOODLEBUG_H

#include <iostream>
#include <vector>

#include "GridCoordinates.hpp"
#include "Critter.hpp"
#include "Ant.hpp"

namespace critter{

	class Grid;

	class Doodlebug : public Critter
	{
		public:
		//Constructor
		Doodlebug(Grid* grid, GridCoordinates loc);
		Doodlebug(Grid* grid);
		Doodlebug();

		Doodlebug(const Doodlebug& doodlebug);

		//Public functions
		static void moveDoodlebugs(Grid* grid);
		static void starveDoodlebugs(Grid* grid);
		static void breedDoodlebugs(Grid* grid);

		int getDbHunger() { return dBHunger; }
		void addDbHunger() { dBHunger++; }
		void resetDbHunger() { dBHunger = 0; }

		void operator =(const Doodlebug& doodlebug);

		private:
		int dBHunger = 0;

		//Private functions
		bool findAnt(GridCoordinates targetLocation, Grid* grid);
		void eatAnt(GridCoordinates sceneOfTheCrime, Grid* grid);

		virtual void move(Grid* grid) override final;
		virtual void spawnCritter(GridCoordinates spawnLocation, Grid* grid) override final;
		virtual void die() override final;
	};
}

#endif
