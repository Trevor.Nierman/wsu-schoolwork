//Trevor Nierman
//R957F692
//CS 411
//Assignment 3
//Ant.hpp

#ifndef ANT_H
#define ANT_H

#include <iostream>
#include <vector>
#include "GridCoordinates.hpp"
#include "Critter.hpp"

namespace critter{

	class Grid;

	class Ant : public Critter
	{
		public:
		//Constructor
		Ant();
		Ant(Grid* grid);
		Ant(Grid* grid, GridCoordinates loc);

		Ant(const Ant& ant);

		//Public Functions
		static void moveAnts(Grid* grid);
		static void breedAnts(Grid* grid);
		void getEaten(GridCoordinates sceneOfTheCrime, Grid* grid);

		//Operators
		bool operator ==(Ant& ant) { return this->location == ant.location; }
		void operator =(Ant& ant);


		private:
		//Private Functions
		virtual void move(Grid* grid) override final { Critter::move(grid); }
		virtual void spawnCritter(GridCoordinates spawnLocation, Grid* grid) override final;
		virtual void die() override final;

	};
}

#endif
