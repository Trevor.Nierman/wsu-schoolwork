//Trevor Nierman
//R957F692
//CS 411
//Assignment 3
//Critter.cpp

#include <iostream>
#include <random>
#include "Critter.hpp"
#include "Grid.hpp"

namespace critter{

	///////////////
	//Constructor//
	//////////////

	Critter::Critter()
	{
		location.row = 0;
		location.column = 0;
	}

	Critter::Critter(const Critter& critter)
	{
		this->location = critter.location;
		this->lifeTime = critter.lifeTime;
	}

	void Critter::operator =(const Critter& critter)
	{
		this->location = critter.location;
		this->lifeTime = critter.lifeTime;
	}


	/////////////////////
	//Virtual Functions//
	/////////////////////

	/*
	Randomly moves the critter NSEW.

	Parameters: maxSize-> dimensions of grid in as a GridCoordinates object
	*/

	void Critter::move(Grid* grid)
	{
		int moveSpace = getRandomAdjacent();

		if(moveSpace == 1){
			if(grid->emptyCell(getCoordinatesAbove())){ moveCritter(getCoordinatesAbove()); }
		}else if(moveSpace == 2){
			if(grid->emptyCell(getCoordinatesRight())){ moveCritter(getCoordinatesRight()); }
		}else if(moveSpace == 3){
			if(grid->emptyCell(getCoordinatesBelow())){ moveCritter(getCoordinatesBelow()); }
		}else if(moveSpace== 4){
			if(grid->emptyCell(getCoordinatesLeft())){ moveCritter(getCoordinatesLeft()); }
		}else{
			//Do Nothing
		}
	}

	/*
	Calls respective constructor for Doodlebugs/Ants -> Critter version does nothing
	*/
	void Critter::spawnCritter(GridCoordinates spawnLocation, Grid* grid)
	{}


	////////////////////
	//Member Functions//
	////////////////////

	/*
	Creates new instance of the respective Critter in an adjacent square.

	Parameters: maxSize-> maximum dimensions of the grid as a GridCoordinates object
	*/

	void Critter::breed(Grid* grid)
	{
		if((lifeTime % breedTime) == 0 && lifeTime != 0){
			if(grid->emptyCell(getCoordinatesAbove())) { spawnCritter(getCoordinatesAbove(), grid); }
			else if(grid->emptyCell(getCoordinatesRight())) { spawnCritter(getCoordinatesRight(), grid); }
			else if(grid->emptyCell(getCoordinatesBelow())) { spawnCritter(getCoordinatesBelow(), grid); }
			else if(grid->emptyCell(getCoordinatesLeft())) { spawnCritter(getCoordinatesLeft(), grid); }
			else{ /* Do Nothing */ }
		}else{ /*Do Nothing*/ }
	}

	/*
	"Moves" a Critter by changing its coordinates
	*/
	void Critter::moveCritter(GridCoordinates newLocation)
	{
		this->location = newLocation;
	}

	/*
	Returns an int (1-4) that points either 1) Up, 2)Right, 3)Down, 4)Left
	*/
	int Critter::getRandomAdjacent(){
		int retVal;

		std::random_device randNum;
    		std::mt19937 generator(randNum());
    		std::uniform_int_distribution<int> uniDist(1, 4);

		retVal = uniDist(generator);

		return retVal;
	}

	/*
	Returns the coordinates of the square above calling object
	*/
	GridCoordinates Critter::getCoordinatesAbove()
	{
		GridCoordinates retCoords = this->location;
		retCoords.row--;
		return retCoords;
	}

	/*
	Returns the coordinates of the square below calling object
	*/
	GridCoordinates Critter::getCoordinatesBelow()
	{
		GridCoordinates retCoords = this->location;
		retCoords.row++;
		return retCoords;
	}

	/*
	Returns the coordinates of the square right of the calling object
	*/
	GridCoordinates Critter::getCoordinatesRight()
	{
		GridCoordinates retCoords = this->location;
		retCoords.column++;
		return retCoords;
	}

	/*
	Returns the coordinates of the square left of calling object
	*/
	GridCoordinates Critter::getCoordinatesLeft()
	{
		GridCoordinates retCoords = this->location;
		retCoords.column--;
		return retCoords;
	}

}
