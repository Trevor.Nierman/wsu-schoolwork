//Trevor Nierman
//R957F692
//CS 411
//Assignment 3
//main.cpp

#include <iostream>
#include <vector>
#include "Grid.hpp"
#include "Critter.hpp"
#include "Ant.hpp"
#include "Doodlebug.hpp"


#define GRIDROW 10
#define GRIDCOLUMN 10
#define ANTPOP 50
#define DBPOP 5


using namespace critter;

void spawnNewAnt(Grid& grid, GridCoordinates spawnLocation)
{
	Ant* newAnt = new Ant(&grid, spawnLocation);
	grid.antList.push_back(newAnt);
	grid.antsBorn++;
}

void spawnNewDoodlebug(Grid& grid, GridCoordinates spawnLocation)
{
	Doodlebug* newDb = new Doodlebug(&grid, spawnLocation);
	grid.doodlebugList.push_back(newDb);
	grid.dBBorn++;
}

int main(){
	//Created grid
	Grid grid(GRIDROW, GRIDCOLUMN);

	//Create ants
	for(int i = 0; i < ANTPOP; i++){
		GridCoordinates spawnLocation = grid.getRandomLocation();
		while(!(grid.emptyCell(spawnLocation))){ spawnLocation = grid.getRandomLocation(); }
		spawnNewAnt(grid, spawnLocation);
	}

	//Create doodlebugs
	for(int i = 0; i < DBPOP; i++){
		GridCoordinates spawnLocation = grid.getRandomLocation();
		while(!grid.emptyCell(spawnLocation)){ spawnLocation = grid.getRandomLocation(); }
		spawnNewDoodlebug(grid, spawnLocation);
	}

	while(1){

		std::cout << "Initial grid: " << std::endl;
		grid.renderGrid();

		std::cout << "After Doodlebugs starve: " << std::endl;
		Doodlebug::starveDoodlebugs(&grid);

		grid.renderGrid();

		std::cout << "After moving Doodlebugs: " << std::endl;
		Doodlebug::moveDoodlebugs(&grid);
		grid.renderGrid();

		std::cout << "After breeding Doodlebugs: " << std::endl;
		Doodlebug::breedDoodlebugs(&grid);
		grid.renderGrid();

		std::cout << "After moving Ants: " << std::endl;
		Ant::moveAnts(&grid);
		grid.renderGrid();

		std::cout << "After breeding Ants: " << std::endl;
		Ant::breedAnts(&grid);
		grid.renderGrid();

		std::cout << std::endl;
		grid.advanceTime();
		std::cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << std::endl;
	}

	return 0;
}
