//Trevor Nierman
//R957F692
//CS 411
//Assignment 3
//Doodlebug.cpp

#include<iostream>
#include<vector>
#include"Doodlebug.hpp"
#include "Grid.hpp"

namespace critter{

	///////////////
	//Constructor//
	///////////////
	Doodlebug::Doodlebug(Grid* grid, GridCoordinates loc) : Critter(loc)
	{
		breedTime = 8;
		grid->totalDb++;
	}

	Doodlebug::Doodlebug(Grid* grid) : Critter()
	{
		breedTime = 8;
		grid->totalDb++;
	}

	Doodlebug::Doodlebug() : Critter()
	{}

	Doodlebug::Doodlebug(const Doodlebug& doodlebug)
	{
		this->location = doodlebug.location;
		this->lifeTime = doodlebug.lifeTime;
	}

	void Doodlebug::operator =(const Doodlebug& doodlebug)
	{
		this->location = doodlebug.location;
		this->lifeTime = doodlebug.lifeTime;
	}


	/////////////////////
	//Virtual Functions//
	/////////////////////

	/*
	Targets ant if one is nearby, otherwise moves randomly (uses Critter::move)
	*/
	void Doodlebug::move(Grid* grid)
	{
		if(findAnt(getCoordinatesAbove(), grid)){ eatAnt(getCoordinatesAbove(), grid); }
		else if(findAnt(getCoordinatesRight(), grid)){ eatAnt(getCoordinatesRight(), grid); }
		else if(findAnt(getCoordinatesBelow(), grid)){ eatAnt(getCoordinatesBelow(), grid); }
		else if(findAnt(getCoordinatesLeft(), grid)){ eatAnt(getCoordinatesLeft(), grid); }
		else{ Critter::move(grid); }
	}

	/*
	Removes a doodlebug object from doodlebugList at a given location
	*/
	void Doodlebug::die()
	{
		delete this;
	}

	/*
	"Spawns" a new doodlebug
	*/
	void Doodlebug::spawnCritter(GridCoordinates spawnLocation, Grid* grid)
	{
		Doodlebug* newDb = new Doodlebug(grid, spawnLocation);

		grid->doodlebugList.push_back(newDb);
		grid->dBBorn++;
	}

	////////////////////
	//Member Functions//
	////////////////////

	/*
	Returns true if an ant's location on antlist matches the parameter location
	*/
	bool Doodlebug::findAnt(GridCoordinates targetLocation, Grid* grid)
	{
		for(unsigned i = 0; i < grid->antList.size(); i++){
			if(grid->antList[i]->getLocation() == targetLocation) return true;
		}
		return false;
	}

	void Doodlebug::eatAnt(GridCoordinates sceneOfTheCrime, Grid* grid)
	{
		for(unsigned i = 0; i < grid->antList.size(); i++){
			if(grid->antList[i]->getLocation() == sceneOfTheCrime){
				grid->antList[i]->getEaten(sceneOfTheCrime, grid);
				resetDbHunger();
				this->Critter::moveCritter(sceneOfTheCrime);
			}
		}
	}

	//Moves *all* doodlebugs
	void Doodlebug::moveDoodlebugs(Grid* grid)
	{
		for(unsigned i = 0; i < grid->doodlebugList.size(); i++){
			grid->doodlebugList[i]->move(grid);
		}
	}

	//Breeds *all* doodlebugs
	void Doodlebug::breedDoodlebugs(Grid* grid)
	{
		for(unsigned i = 0; i < grid->doodlebugList.size(); i++){
			grid->doodlebugList[i]->breed(grid);
		}
	}

	//Checks *all* doodlebugs to see if they've starved, killing them if they have
	void Doodlebug::starveDoodlebugs(Grid* grid)
	{
		for(unsigned i = 0; i < grid->doodlebugList.size(); i++){
			if(grid->doodlebugList[i]->getDbHunger() == 3){
					grid->doodlebugList[i]->die();
					grid->doodlebugList.erase(grid->doodlebugList.begin() + i);
					grid->dBStarved++;
					grid->totalDb--;
			}
		}
	}

}
