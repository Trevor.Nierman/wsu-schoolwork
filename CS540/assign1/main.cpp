//Trevor Nierman
//CS540
//Programming Assignment 1

//main.cpp

#include<iostream>
#include<mutex>
#include<thread>
#include<cstdlib>
#include<unistd.h>
#include<semaphore.h>
#include"buffer.hpp"
using namespace std;

buffer buff[2];
sem_t numEmpty;
sem_t numFull;
sem_t coutMutex;

sem_t* pNumFull = &numFull;
sem_t* pNumEmpty = &numEmpty;

void producer(int id);
void consumer(int id);


//const int SIZE = 5; //Buffer size
const double FACTOR = 0.000000001; //one-billionth
int main(int argc, char* argv[]){

	if(argc > 4){
		cout << "Error: Too many arguments passed. ( Expected ./prodCons [sleep Time] [number Producers] [number Consumers] )" << endl;
		exit(0);
	} else if(argc < 4){
		cout << "Error: Too few arguments passed. ( Expected prodCons [sleep Time] [number Producers] [number Consumers] )" << endl;
		exit(0);
	}

	//chrono::milliseconds sleepTime = atoi(argv[1]); //Sleep time for program
	int sleepTime = atoi(argv[1]);
	int numProducers = atoi(argv[2]); //Number of producers
	int numConsumers = atoi(argv[3]); //Number of consumers

	sem_init(&numEmpty, 0, SIZE);
	sem_init(&numFull, 0, SIZE);
	sem_init(&coutMutex, 0, 1);

	thread* prodThreads = new thread[numProducers];
	thread* conThreads = new thread[numConsumers];

	for(int i = 0; i < numProducers; i++){
		prodThreads[i] = thread(producer, i);
	}

	for(int i = 0; i < numConsumers; i++){
		conThreads[i] = thread(consumer, i);
	}

	sleep(sleepTime);

	return 0;
}

void producer(int id)
{
	int item;
	int sleepTime;

	while(true){
		sleepTime = rand()*FACTOR;
		sleep(sleepTime);

		//Generate item to insert
		item = rand() % 100;

		sem_wait(&numEmpty);

		int i = rand() % 2;
		buff[i].insertItem(item);

		sem_wait(&coutMutex);
		cout << "Producer " << id << " inserted " << item << " into a buffer." << endl;
		sem_post(&coutMutex);

		sem_post(&numEmpty);
	}
}

void consumer(int id)
{
	int item;
	int sleepTime;

	while(true){
		sleepTime = rand() * FACTOR;
		sleep(sleepTime);

		sem_wait(&numFull);

		int i = rand() % 2;
		buff[i].removeItem(item);

		sem_wait(&coutMutex);
		cout << "Consumer " << id << " removed " << item << " from a buffer." << endl;
		sem_post(&coutMutex);

		sem_post(&numFull);
	}
}
