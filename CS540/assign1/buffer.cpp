//Trevor Nierman
//CS540 Operating Systems
//Program1

//buffer.cpp

#ifndef BUFFER_H
#define BUFFER_H



#include<iostream>
//#include<threads>
#include<semaphore.h>

#include "buffer.hpp"

using namespace std;

buffer::buffer()
{
	in = 0;
	out = 0;
}

buffer::buffer(buffer& buff)
{
	this->in = buff.in;
	this->out = buff.out;

	for(int i = 0; i < SIZE; i++){
		this->itemArray[i] = buff.itemArray[i];
	}
}

buffer::~buffer()\
{}


void buffer::insertItem(int item)
{
	buffAccess.lock();
	itemArray[in] = item;
	in++;
	buffAccess.unlock();
}

void buffer::removeItem(int &item)
{
	buffAccess.lock();
	item = itemArray[out];
	itemArray[out] = -1;
	out++;
	buffAccess.unlock();
}

#endif
