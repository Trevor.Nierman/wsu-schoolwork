//Trevor Nierman
//R957F692
//tsnierman@shockers.wichita.edu

//CS540 Operating Systems
//Program1

//buffer.hpp
#include<iostream>
#include<semaphore.h>
#include<thread>
#include<mutex>
using namespace std;

const int SIZE = 5;

class buffer {

private:
	int itemArray[];
	int in, out;

public:
	//Constructors
	buffer();
	buffer(buffer& buff);
	~buffer();


	//Public data
	mutex buffAccess;

	void insertItem(int item);
	void removeItem(int &item);

};
