import socket

#Create a TCP server socket
socket1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

#Assign Port number
serverPort = 6789

#Bind the socket to server address and get server port
socket1.bind(('localhost', serverPort))

#Listen to, at most, one connection at a time
socket1.listen(1)

#Listen to incoming requests
while True:
	print 'Hello'

	#Create new connection from client
	connection, address = socket1.accept()	

	try:
		#Recieve request from client
		message = connection.recv(4096)

		#Parse request message to find path
		filename = message.split()[1]
		
		#Path is read from second character (exclude '\')
		f = open(filename[1:]) 
		output = f.read()

		#Send response to connection socket
		connectionSocket.send("OK")

		#Send the content of output to connection socket
		for i in range(0, len(output)):	socket1.send(i)

		#Close connection socket
		socket1.shutdown(SHUT_RDWR)

	except IOError: 
		#Send response for file not found
		connectionSocket.send("HEADER LINE HERE")
		connectionSocket.send("<html><head></head><body><h1>404 Not Found</h1></body></html>")
			
		#Close connection socket
		socket1.shutdown(SHUT_RDWR)		


#End while

#Close Socket
socket1.shutdown(SHUT_RDWR)
