#Trevor Nierman
#R857F692
#CS 464
#Programming Assignment 1

import socket

socket1 = socket.socket()
host = socket.gethostname()
hostip = socket.gethostbyname(host)
port = 6789

socket1.bind((hostip, port))

print "Socket bound to", hostip

socket1.listen(1)
while True:
	connection, address = socket1.accept()
	print 'Accepted connection from ', address
	
	connection.send('Trevor is amazing')

	connection.close()

connection.close()
