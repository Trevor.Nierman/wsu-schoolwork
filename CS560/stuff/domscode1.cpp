//Servando D. Gonzalez
//X923A549
//CS560
//Programming Homework 1


#include<fstream>
#include<stdlib.h>
#include<chrono>
#include<iostream>




using namespace std;
using namespace std::chrono;






const int MAX_RANGE = 10000;

int partition(int qsArray[], int minIndex, int maxIndex)
{
	int p = minIndex;
	int pivot = qsArray[minIndex];
	for(int i = minIndex + 1; i < maxIndex; i++)
	{
		if(qsArray[i] <= pivot)
		{
			p++;
			swap(qsArray[p], qsArray[i]);
		}
	}

	swap(qsArray[p], qsArray[minIndex]);
	return p + 1;
}
////////////////////////////////////////////////////////////////////////
int randomPartition(int rqsArray[], int minIndex, int maxIndex)
{
	int p = rand() % (maxIndex - minIndex);
	if(p < minIndex) p = minIndex;
	if(p > maxIndex) p = maxIndex;

	swap(rqsArray[p], rqsArray[minIndex]);
	p = minIndex - 1;
	
	int pivot = rqsArray[minIndex];

	for(int i = minIndex; i < maxIndex; i++)
	{
		if(rqsArray[i] <= pivot)
		{
			p++;
			swap(rqsArray[p], rqsArray[i]);
		}
	}
	swap(rqsArray[p], rqsArray[minIndex]);
	return p+1;
}
////////////////////////////////////////////////////////////////////////
void quicksort(int intArray[], int minIndex, int maxIndex)
{
	if(minIndex < maxIndex)
	{

		int pivot = partition(intArray, minIndex, maxIndex);
		quicksort(intArray, minIndex, pivot - 1);
		quicksort(intArray, pivot + 1, maxIndex);
	}
}
////////////////////////////////////////////////////////////////////////
void randomQuicksort(int intArray[], int minIndex, int maxIndex)
{
	if(minIndex < maxIndex)
	{

		int pivot = randomPartition(intArray, minIndex, maxIndex);
		randomQuicksort(intArray, minIndex, pivot - 1);
		randomQuicksort(intArray, pivot + 1, maxIndex);
	}
}
////////////////////////////////////////////////////////////////////////
int main()
{

	
	cout << "Enter a positive # : ";

	int numInts;

	cin >> numInts;

	while(numInts <= 0)
	{
		cout << "Enter a positive #: ";
		cin >> numInts;
	}

	cout << "Choose option 1 or 2 by typing in repective number" << endl;
	cout << "1) Generate numbers from your input" << endl;
	cout << "2) Randomly generate numbers from 0 to 10,000" << endl;

	int option;
	cin >> option;

	while(option < 1 || option > 2)
	{
		cout << "Choose Valid Input" << endl;
		cin >> option;
	}

	int intArray[numInts];

	if(option == 1)
	{
		cout << "Enter an offset number.";

		int offset;
		cin >> offset;

		while(offset < 0)
		{
			cout << "Enter a positive number: ";
			cin >> offset;
		}

		for(int i = 0; i < numInts; i++)
		{
			intArray[i] = numInts + (offset * i+1);
		}

	}
	
	else if(option == 2)
	{

		for(int i = 0; i < numInts; i++)
		{
			intArray[i] = rand() % MAX_RANGE;
		}

		
	}

	
	int qsArray[numInts];
	int rqsArray[numInts];

	cout << endl << "Original Array: " << endl;

	for(int i = 0; i < numInts; i++)
	{
		qsArray[i] = intArray[i];
		rqsArray[i] = intArray[i];


		cout << "intArray[" << i << "] = " << intArray[i] << endl;
	}

	////////////////////////////////////////////////////////////////////
	//begin runtime
    high_resolution_clock::time_point qsStart = high_resolution_clock::now();
	quicksort(qsArray, 0, numInts);
	//end runtime
	high_resolution_clock::time_point qsEnd = high_resolution_clock::now();
	auto qsDuration = duration_cast<microseconds>(qsEnd - qsStart).count();
	////////////////////////////////////////////////////////////////////

	srand(rand());
	////////////////////////////////////////////////////////////////////
	//begin runtime
	high_resolution_clock::time_point rqsStart = high_resolution_clock::now();
	randomQuicksort(rqsArray, 0, numInts);
	//end runtime
	high_resolution_clock::time_point rqsEnd = high_resolution_clock::now();
	auto rqsDuration = duration_cast<microseconds>(rqsEnd - rqsStart).count();
	////////////////////////////////////////////////////////////////////
	
	
	
	
	
	ofstream fout;
	fout.open("output.txt");
	fout << "Original Array Sorts" << endl;
	for(int i = 0; i < numInts; i++)
	{
		fout << intArray[i] << "\t\t";
		fout << qsArray[i] << "\t\t";
		fout << rqsArray[i] << endl;
	}
	fout << endl;
	fout<< "Quicksort runtime: " << qsDuration << "Microseconds" << endl;
	fout << "Randomized Quicksort runtime: " << rqsDuration << "Microseconds" << endl;
	fout.close();
	


	return 0;
}
