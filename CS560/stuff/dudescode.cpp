//Trevor Nierman
//R957F692
//CS560
//Programming Assignment 1

//main.cpp

#include<iostream>
#include<stdlib.h>
#include<chrono>
#include<fstream>
using namespace std;
using namespace std::chrono;

const int MAX_RANGE = 10000;

int partition(int qsArray[], int minIndex, int maxIndex)
{
	int p = minIndex;
	int pivot = qsArray[minIndex];

	for(int i = minIndex + 1; i < maxIndex; i++){
		if(qsArray[i] <= pivot){
			p++;
			swap(qsArray[p], qsArray[i]);
		}
	}

	swap(qsArray[p], qsArray[minIndex]);

	return p + 1;
}

int randomPartition(int rqsArray[], int minIndex, int maxIndex)
{
	int p = rand() % (maxIndex - minIndex);

	if(p < minIndex) p = minIndex;
	if(p > maxIndex) p = maxIndex;

	swap(rqsArray[p], rqsArray[minIndex]);
	p = minIndex - 1;
	int pivot = rqsArray[minIndex];

	for(int i = minIndex; i < maxIndex; i++){
		if(rqsArray[i] <= pivot){
			p++;
			swap(rqsArray[p], rqsArray[i]);
		}
	}

	swap(rqsArray[p], rqsArray[minIndex]);

	return p + 1;
}

void quicksort(int intArray[], int minIndex, int maxIndex)
{
	if(minIndex < maxIndex){

		int pivot = partition(intArray, minIndex, maxIndex);

		quicksort(intArray, minIndex, pivot - 1);
		quicksort(intArray, pivot + 1, maxIndex);
	}
}

void randomQuicksort(int intArray[], int minIndex, int maxIndex)
{
	if(minIndex < maxIndex){

		int pivot = randomPartition(intArray, minIndex, maxIndex);

		randomQuicksort(intArray, minIndex, pivot - 1);
		randomQuicksort(intArray, pivot + 1, maxIndex);
	}
}

int main(){

	cout << "Hello!" << endl;
	cout << "Please enter a positive number as the length of the array: ";

	int numInts;

	cin >> numInts;

	while(numInts <= 0){
		cout << "Please enter a positive number: ";
		cin >> numInts;
	}

	cout << "What would you like to do?" << endl;
	cout << "1) Generate numbers based on input X" << endl;
	cout << "2) Randomly generate numbers from 0 to 10,000" << endl;

	int option;
	cin >> option;

	while(option < 1 || option > 2){
		cout << "Please choose a valid option." << endl;
		cin >> option;
	}

	int intArray[numInts];

	if(option == 1){
		cout << "Enter an offset (X): ";

		int offset;
		cin >> offset;

		while(offset < 0){
			cout << "Please enter a positive number: ";
			cin >> offset;
		}

		cout << "Generating numbers...";

		for(int i = 0; i < numInts; i++){
			intArray[i] = numInts + (offset * (i+1));
		}

		cout << "done" << endl;

	}else if(option == 2){
		cout << "Randomly generating " << numInts << " numbers...";

		for(int i = 0; i < numInts; i++){
			intArray[i] = rand() % MAX_RANGE;
		}

		cout << "done" << endl;
	}

	//Create one copy of the array for each sort
	int qsArray[numInts];
	int rqsArray[numInts];

	cout << endl << "Original Array: " << endl;

	for(int i = 0; i < numInts; i++){
		qsArray[i] = intArray[i];
		rqsArray[i] = intArray[i];
	}

	cout << "Starting Quicksort...";

	//Time Quicksort
	high_resolution_clock::time_point qsStart = high_resolution_clock::now();
    	quicksort(qsArray, 0, numInts);
    	high_resolution_clock::time_point qsEnd = high_resolution_clock::now();

	auto qsDuration = duration_cast<microseconds>(qsEnd - qsStart).count();



	cout << "done" << endl;
	cout << "Starting Randomized Quicksort...";


	//Seed for randomized quicksort
	srand(rand());

	//Time randomized Quicksort
	high_resolution_clock::time_point rqsStart = high_resolution_clock::now();
    	randomQuicksort(rqsArray, 0, numInts);
    	high_resolution_clock::time_point rqsEnd = high_resolution_clock::now();

	auto rqsDuration = duration_cast<microseconds>(rqsEnd - rqsStart).count();


	cout << "done" << endl;

	//Open output.txt
	ofstream fout;
	fout.open("output.txt");

	fout << "Original Array\tQuicksort\tRandomized Quicksort" << endl;
	for(int i = 0; i < numInts; i++){
		fout << intArray[i] << "\t\t";
		fout << qsArray[i] << "\t\t";
		fout << rqsArray[i] << endl;
	}

	fout << endl;
	fout << "Quicksort runtime: " << qsDuration << " milliseconds" << endl;
	fout << "Randomized Quicksort runtime: " << rqsDuration << " milliseconds" << endl;
	fout.close();

	return 0;
}
 