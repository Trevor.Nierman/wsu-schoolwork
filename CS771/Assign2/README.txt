Trevor Nierman
CS771
Programming Assignment 2

N Queens problem

This program solves the N Queens Problem through the Min-Conflict Algorithm.

In order to compile this program, type "make". 
To run the program, type "./solve". 

For example:

	$ make
	$ ./solve

It is recommended that input be created in a seperate file, then chained in after for ease of use.
To create a valid input file:
	~The first line should be the size of the board. This program uses only n x n (square) boards.
	~The second line should be the maximum number of steps. 
	~The final section is the board itself. The program recieves the board one row at a time, so the input should look like the actual board.
	Queens should be represented by X's (capitalized) and empty spaces can be anything (except empty characters).

	For example (empty spaces are symbolized with O's):
	
	4
	10
	OXOO
	OOXO
	XOOO
	OOOX

ENSURE THAT THERE ARE NO SPACES BETWEEN CHARACTERS.
This will cause errors when generating the initial configuration of the board.

To chain in the input file when running the executable:

	$ make
	$ ./solve < inputFile

For ease of use, it is also possible to chain out the output:

	$ make
	$ ./solve < inputFile > outputFile

This will cause inputFile to be read for all necessary inputs, and all outputs (including the final solution) will be sent to the 
outputFile to be reviewed at the user's leisure.

Enjoy.
