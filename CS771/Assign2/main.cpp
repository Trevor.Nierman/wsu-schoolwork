//Trevor Nierman
//R957F692
//CS771 AI
//Programming Assignment 2


//main.cpp

#include<iostream>
#include<string>
#include<vector>
using namespace std;

bool checkBoard(vector<vector<bool>>& board, int& size)
{
	for(int i = 0; i < size; i++){
		for(int j = 0; j < size; j++){
			if(board[i][j] == true){

				//Check left
				if(j > 0){
					for(int k = 0; k < j; k++){
						if(board[i][k] == true)	return false;
					}
				}
				//Check right
				if(j < size - 1){
					for(int k = j + 1; k < size; k++){
						if(board[i][k] == true) return false;
					}
				}
				//Check above
				if(i > 0){
					for(int k = 0; k < i; k++){
						if(board[k][j] == true)	return false;
					}
				}
				//Check below
				if(i < size - 1){
					for(int k = i + 1; k < size; k++){
						if(board[k][j] == true)	return false;
					}
				}
				//Check upper left
				if(j > 0 && i > 0){
					int n = i - 1;
					int m = j - 1;

					do{
						if(board[n][m] == true) return false;

						n--;
						m--;
					}while(n >= 0 && m >= 0);
				}
				//Check lower right
				if(j < size - 1 && i < size - 1){
					int n = i + 1;
					int m = j + 1;

					do{
						if(board[n][m] == true)	return false;

						n++;
						m++;
					}while(n < size && m < size);
				}
				//Check upper right
				if(i > 0 && j < size - 1){
					int n = i - 1;
					int m = j + 1;

					do{
						if(board[n][m] == true) return false;

						n--;
						m++;
					}while(n >= 0 && m < size);
				}
				//Check lower left
				if(i < size - 1 && j > 0){
					int n = i + 1;
					int m = j - 1;

					do{
						if(board[n][m] == true) return false;

						n++;
						m--;
					}while(n < size && m >= 0);
				}
			}
		}
	}
	return true;
}

void checkConflicts(vector<vector<bool>>& board, vector<bool>& conflicts, int& size)
{
	for(int i = 0; i < size; i++){
		for(int j = 0; j < size; j++){
			if(board[i][j] == true){
				//Check left
				if(j > 0){
					for(int k = 0; k < j; k++){
						if(board[i][k] == true)	conflicts[j] = true;
					}
				}
				//Check right
				if(j < size - 1){
					for(int k = j + 1; k < size; k++){
						if(board[i][k] == true)	conflicts[j] = true;
					}
				}
				//Check above
				if(i > 0){
					for(int k = 0; k < i; k++){
						if(board[k][j] == true)	cout << "C; i = " << i << endl;
					}
				}
				//Check below
				if(i < size - 1){
					for(int k = i + 1; k < size; k++){
						if(board[k][j] == true)	conflicts[j] = true;
					}
				}
				//Check upper left
				if(j > 0 && i > 0){
					int n = i - 1;
					int m = j - 1;

					do{
						if(board[n][m] == true)	conflicts[j] = true;

						n--;
						m--;
					}while(n >= 0 && m >= 0);
				}
				//Check lower right
				if(j < size - 1 && i < size - 1){
					int n = i + 1;
					int m = j + 1;

					do{
						if(board[n][m] == true) conflicts[j] = true;

						n++;
						m++;
					}while(n < size && m < size);
				}
				//Check upper right
				if(i > 0 && j < size - 1){
					int n = i - 1;
					int m = j + 1;

					do{
						if(board[n][m] == true) conflicts[j] = true;

						n--;
						m++;
					}while(n > 0 && m < size);
				}
				//Check lower left
				if(i < size - 1 && j > 0){
					int n = i + 1;
					int m = j - 1;

					do{
						if(board[n][m] == true) conflicts[j] = true;

						n++;
						m--;
					}while(n < size && m >= 0);
				}
			}
		}
	}
}

void calculateConflicts(vector<vector<bool>>& board, vector<int>& numConflicts, int& column, int& size)
{
	int j = column;
	for(int i = 0; i < size; i++){
		//Check left
		if(j > 0){
			for(int k = 0; k < j; k++){
				if(board[i][k] == true)	numConflicts[i]++;
			}
		}
		//Check right
		if(j < size - 1){
			for(int k = j + 1; k < size; k++){
				if(board[i][k] == true)	numConflicts[i]++;
			}
		}
		//Check upper left
		if(j > 0 && i > 0){
			int n = i - 1;
			int m = j - 1;
			do{
				if(board[n][m] == true)	numConflicts[i]++;
				n--;
				m--;
			}while(n >= 0 && m >= 0);
		}
		//Check lower right
		if(j < size - 1 && i < size - 1){
			int n = i + 1;
			int m = j + 1;
			do{
				if(board[n][m] == true)	numConflicts[i]++;

				n++;
				m++;
			}while(n < size && m < size);
		}
		//Check upper right
		if(i > 0 && j < size - 1){
			int n = i - 1;
			int m = j + 1;
			do{
				if(board[n][m] == true) numConflicts[i]++;

				n--;
				m++;
			}while(n >= 0 && m < size);
		}
		//Check lower left
		if(i < size - 1 && j > 0){
			int n = i + 1;
			int m = j - 1;
			do{
				if(board[n][m] == true)	numConflicts[i]++;

				n++;
				m--;
			}while(n < size && m >= 0);
		}
	}
}

void displayBoard(vector<vector<bool>>& board, int& size)
{
	cout << endl;

	for(int i = 0; i < size; i++){
		for(int j = 0; j < size; j++){
			if(board[i][j] == true) cout << "X";
			else cout << "O";
		}
		cout << endl;
	}
	cout << endl;
}


int main(){

	//Size input
	string inputSize;
	cout << "Enter a size: ";
	getline(cin, inputSize);
	int size = stoi(inputSize);

	//Step input
	string inputSteps;
	cout << "Enter the maximum number of steps: ";
	getline(cin, inputSteps);
	int maxSteps = stoi(inputSteps);

	//Create board
	vector<vector<bool>> board;
	board.resize(size);
	for(int i = 0; i < size; i++){
		board[i].resize(size);
	}

	string row;
	for(int i = 0; i < size; i++){
		cout << "Enter a row: ";
		getline(cin, row);
		for(int j = 0; j < size; j++){
			if(string(1, row[j]) == "X") board[i][j] = true;
		}
	}


	cout << endl << endl << "Initial Board" << endl << endl;
	displayBoard(board, size);

	/////////////////
	//Begin solving//
	/////////////////

	int currStep = 0;
	bool notSolved = true;
	vector<bool> conflictColumns;
	conflictColumns.resize(size);
	vector<int> numConflicts;
	numConflicts.resize(size);


	
	cout << "Starting to solve..." << endl;






	while(currStep < maxSteps && notSolved){

		if(checkBoard(board, size)){
			notSolved = false;
		}else{
			notSolved = true;
			checkConflicts(board, conflictColumns, size);

			//Randomly select a conflicted column
			int column = rand() % size;
			while(conflictColumns[column] != true){
				column = (column + 1) % size;
			}

			//Find row with least number of conflicts
			fill(numConflicts.begin(), numConflicts.end(), 0); //Reset to 0
			calculateConflicts(board, numConflicts, column, size);

			int minConflicts = size; //Initialized to max value
			int newRow = 0;
			for(int i = 0; i < size; i++){
				if(numConflicts[i] < minConflicts){
					newRow = i;
					minConflicts = numConflicts[i];
				}
			}

			//Move queen to that row
			int currentRow = 0;
			for(int i = 0; i < size; i++){
				if(board[i][column] == true) currentRow = i;
			}

			if(board[currentRow][column] == false){
				cout << "Error in swap!" << endl;
			}else{
				board[currentRow][column] = false;
				board[newRow][column] = true;
			}

			//Display board
			cout << endl << "Current step: " << currStep << endl;
			displayBoard(board, size);

			currStep++;

			//Reset numConflicts
			for(int i = 0; i < size; i++){
				numConflicts[i] = 0;
			}
		}
	}


	cout << "Done" << endl;



	return 0;
}
